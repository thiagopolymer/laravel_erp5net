<?php

namespace App\Http\Controllers\Api;
use Illuminate\Support\Facades\DB;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Cadastro\Contato;

class ClienteController extends Controller
{
    public function classificacao($id = 0)
    {
        $data = DB::table('classificacoes')->find($id);

        return response()->json($data);
    }

    public function representante($id = 0)
    {
        $data = DB::table('representantes')->find($id);


        return response()->json($data);
    }

    public function banco($id = 0)
    {
        $data = DB::table('bancos')->find($id);


        return response()->json($data);
    }

    public function pessoa($id = 0)
    {
        $data = DB::table('pessoas')->find($id);


        return response()->json($data);
    }

    public function cidade($id = 0)
    {
        $data = DB::table('cidades')->find($id);

        return response()->json($data);
    }

    public function cidade_nome($nome = "")
    {
        $data = DB::table('cidades')->where("nome",trim($nome))->first();

        return response()->json($data);
    }

    public function transportadora($id = "")
    {
        $data = DB::table('transportadoras')->find($id);

        return response()->json($data);
    }

    public function bacen_banco($id = "")
    {
        $data = DB::table('bacen_bancos')->find($id);

        return response()->json($data);
    }

    public function insert_contato(Request $request)
    {
        $dados = $request->all();

        $tbContato = new Contato();
        $tbContato->insert([
            [
                'pessoa_id' => $dados['pessoa_id'],
                'tipo_id' => $dados['tipo_id_c'],
                'nome' => $dados['nome_c'],
                'fone' => $dados['fone_c'],
                'ramal' => $dados['ramal_c'],
                'celular' => $dados['celular_c'],
                'time_id' => $dados['time_id_c'],
                'hobby' => $dados['hobby_c'],
                'facebook' => $dados['facebook_c'],
                'instagran' => $dados['instagran_c'],
                'twitter' => $dados['twitter_c'],
                'linkedin' => $dados['linkedin_c'],
                'youtube' => $dados['youtube_c'],
                // 'nascimento' => $dados['nascimento_c'],
            ]
        ]);
        return response()->json($dados);
    }

    public function cliente_list()
    {
        $data = DB::table('clintes')->find($id);

        return response()->json($data);
    }

}
