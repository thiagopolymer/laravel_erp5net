<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Cadastro\Representante;
use App\Model\Cadastro\Pessoa;

class RepresentanteController extends Controller
{
    private $tbRepresentantes;
    private $tbPessoas;

    public function __construct()
    {

        $this->tbRepresentantes = new Representante();
        $this->tbPessoas = new Pessoa();
        // $this->middleware('auth');
    }

    // Metódos Padrões
    public function index()
    {
        $tituloPagina = "Representantes";

        $rsRepresentantes = $this->tbRepresentantes->paginate(20);

        return view('Cadastro.Pessoa.Representante.index',compact("tituloPagina","rsRepresentantes"));
    }

    public function edit($id = "0", $pessoa = "")
    {
        if($id == "0")
        {
            $tituloPagina = "Representante (novo cadastro)";
            $rsRepresentante = $this->tbRepresentantes;
            $rsRepresentante->pessoa_id = intval($pessoa);
        }
        else
        {
            $tituloPagina = "Representante (alterar cadastro)";
            $rsRepresentante = $this->tbRepresentantes->find($id);
        }
        return view('Cadastro.Pessoa.Representante.edit',compact("tituloPagina","rsRepresentante"));
    }


    public function insert(Request $request)
    {
        $rsRepresentante = $this->tbRepresentantes;

        $request = $request->all();
        $request['comissao'] = str_replace(",",".",$request['comissao']);

        $rsRepresentante->create($request);

        return redirect(route('pessoas.representantes'));
    }

    public function Update(Request $request)
    {
        $rsRepresentante = $this->tbRepresentantes->find($request->id);
        $request = $request->all();
        $request['comissao'] = str_replace(',','.',$request['comissao']);

        $rsRepresentante->fill($request);
        $rsRepresentante->update();

        return redirect(route('pessoas.representantes'));
    }

    public function delete()
    {
        return "Delete";
    }
}
