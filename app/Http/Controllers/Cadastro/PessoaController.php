<?php

namespace App\Http\Controllers\Cadastro;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Cadastro\Pessoa;

class PessoaController extends Controller
{

    private $tbPessoas;

    public function __construct()
    {

        $this->tbPessoas = new Pessoa();
        // $this->middleware('auth');
    }

    // Metódos Padrões
    public function index()
    {
        $tituloPagina = "Pessoas";

        $rsPessoas = $this->tbPessoas->orderby('id')->paginate(20);

        return view('Cadastro.Pessoa.index',compact("tituloPagina","rsPessoas"));
    }

    public function edit($id = "0")
    {
        if($id == "0")
        {
            $tituloPagina = "Pessoas (novo cadastro)";
            $rsPessoa = $this->tbPessoas;
        }
        else
        {
            $tituloPagina = "Pessoas (alterar cadastro)";
            $rsPessoa = $this->tbPessoas->find($id);
        }
        return view('Cadastro.Pessoa.edit',compact("tituloPagina","rsPessoa"));
    }


    public function insert(Request $request)
    {
        $rsPessoa = $this->tbPessoas;

        $validate = [
            'cnpj' => "required|unique:pessoas",
            'razaosocial' => 'required|max:120',
            'cep' => 'required',
        ];

        $msg_validate = [
            'cnpj.required' => "CNPJ - Campo de preenchimento obrigataório",
            'cnpj.unique' => "CNPJ já cadastrado",
            'razaosocial.required' => "Razão Social - Campo de preenchimento obrigataório",
            'razaosocial.max' => "Razão Social - Quantidade máxima de caracteres é de 120",
            'cep.required' => "CEP - Campo de preenchimento obrigataório",
        ];

        $request->validate($validate,$msg_validate);

        $regInsertPessoa = $rsPessoa->create($request->all());
        switch(intval($request->btn_acesso_id))
        {
            case 1:
                DB::table('clientes')->insert([
                    [
                        'id' => $regInsertPessoa->id,
                        'pessoa_id' => $regInsertPessoa->id,
                        'created_at' => new \DateTime,
                        'user_id' => auth()->user()->id,
                    ],
                ]);
                return redirect(route('pessoas.clientes.edit',['id' => $regInsertPessoa->id]));
                break;
            case 2:
                return redirect(route('pessoas.fornecedores.edit',['id' => 0, 'pessoa' => $regInsertPessoa]));
                break;
            case 3:
                return redirect(route('pessoas.transportadoras.edit',['id' => 0, 'pessoa' => $regInsertPessoa]));
                break;
            case 4:
                return redirect(route('pessoas.representantes.edit',['id' => 0, 'pessoa' => $regInsertPessoa]));
                break;
            default:
                return redirect(route('pessoas'));
                break;
        }

    }

    public function Update(Request $request)
    {
        $validate = [
            'cnpj' => "required|unique:pessoas",
            'razaosocial' => 'required|max:120',
            'cep' => 'required',
        ];

        $msg_validate = [
            'cnpj.required' => "CNPJ - Campo de preenchimento obrigataório",
            'cnpj.unique' => "CNPJ já cadastrado",
            'razaosocial.required' => "Razão Social - Campo de preenchimento obrigataório",
            'razaosocial.max' => "Razão Social - Quantidade máxima de caracteres é de 120",
            'cep.required' => "CEP - Campo de preenchimento obrigataório",
        ];

        $rsPessoa = $this->tbPessoas->find($request->id);
        $request = $request->all();

        $rsPessoa->fill($request);
        $rsPessoa->update();

        return redirect(route('pessoas'));
    }

    public function delete()
    {
        return "Delete";
    }

    public function list($search = "")
    {
         if(trim($search) != "")
         {
            $cnpjLimpo = str_replace('-','',$search);
            $cnpjLimpo = str_replace('.','',$cnpjLimpo);
            $cnpjLimpo = str_replace('/','',$cnpjLimpo);

             $rsPessoas = $this->tbPessoas
             ->orWhere("razaosocial","like","%$search%")
             ->orWhere("id","like","%$search%")
             ->orWhere(DB::raw("replace(replace(replace(cnpj,'.',''),'-',''),'/','')"),"like","%$cnpjLimpo%")
             ->limit(20)
             ->get();
         }
         else
         {
             $rsPessoas = $this->tbPessoas->limit(20)->get();
         }

         return view('Cadastro.Pessoa.list',compact("rsPessoas"));
    }
}
