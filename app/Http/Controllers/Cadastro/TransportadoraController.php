<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Cadastro\Transportadora;
use App\Model\Cadastro\Pessoa;

class TransportadoraController extends Controller
{
    private $rsTransportadoras;
    private $tbPessoas;

    public function __construct()
    {

        $this->tbTransportadoras = new Transportadora();
        $this->tbPessoas = new Pessoa();
        // $this->middleware('auth');
    }

    // Metódos Padrões
    public function index()
    {
        $tituloPagina = "Transportadoras";

        $rsTransportadoras = $this->tbTransportadoras->paginate(20);

        return view('Cadastro.Pessoa.Transportadora.index',compact("tituloPagina","rsTransportadoras"));
    }

    public function edit($id = "0", $pessoa = "")
    {
        if($id == "0")
        {
            $tituloPagina = "Transportadora (novo cadastro)";
            $rsTransportadora = $this->tbTransportadoras;
            $rsTransportadora->pessoa_id = intval($pessoa);
        }
        else
        {
            $tituloPagina = "Transportadora (alterar cadastro)";
            $rsTransportadora = $this->tbTransportadoras->find($id);
        }
        return view('Cadastro.Pessoa.Transportadora.edit',compact("tituloPagina","rsTransportadora"));
    }


    public function insert(Request $request)
    {
        $rsTransportadora = $this->tbTransportadoras;

        $request = $request->all();
        $request['valor'] = str_replace(",",".",$request['valor']);

        $rsTransportadora->create($request);

        return redirect(route('pessoas.transportadoras'));
    }

    public function Update(Request $request)
    {
        $rsTransportadora = $this->tbTransportadoras->find($request->id);
        if(intval($request['coleta'] == 0))
        {
            $request = $request->all() + ["taxa" => 0];
            $request['valor'] = 0;
        }
        else
        {
            $request = $request->all();
        }

        $request['valor'] = str_replace(',','.',$request['valor']);

        $rsTransportadora->fill($request);
        $rsTransportadora->update();

        return redirect(route('pessoas.transportadoras'));
    }

    public function delete()
    {
        return "Delete";
    }
}
