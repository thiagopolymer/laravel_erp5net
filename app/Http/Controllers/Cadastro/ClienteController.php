<?php

namespace App\Http\Controllers\Cadastro;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Model\Cadastro\Cliente;
use App\Model\Cadastro\PeriodoAtividade;
use App\Model\Cadastro\Potencial;

class ClienteController extends Controller
{
    private $tbClientes;
    private $tbPotenciais;

    public function __construct()
    {
        $this->tbClientes = new Cliente();
        $this->tbPotenciais = new Potencial();
        // $this->middleware('auth');
    }

   // Metódos Padrões
   public function index()
   {
       $tituloPagina = "Clientes";

       $regTotal = $this->tbClientes->get()->count();

       return view('Cadastro.Pessoa.Cliente.index',compact("tituloPagina","regTotal"));
   }

   public function edit($id = "0", $pessoa = 0)
   {
        $rsPotenciais = $this->tbPotenciais->get();
        $tituloPagina = "Cliente (alterar cadastro)";
        $rsCliente = $this->tbClientes->where("pessoa_id",$id)->first();

        return view('Cadastro.Pessoa.Cliente.edit',compact("tituloPagina","rsCliente","rsPotenciais"));
   }


   public function insert(Request $request)
   {
        $rsCliente = $this->tbClientes;

        $validate = [
            'pessoa_id' => "required|unique:clientes",
            'classificacao_id' => 'required',
        ];

        $msg_validate = [
            'pessoa_id.required' => "Escolher pessoa cadastrada - Campo de preenchimento obrigataório",
            'classificacao_id.required' => "Classificação - Campo de preenchimento obrigataório",
        ];
        $request->validate($validate,$msg_validate);

        $aberturaDia = substr($request->abertura,0,2);
        $aberturaMes = substr($request->abertura,3,2);
        $aberturaAno = substr($request->abertura,6,4);
        $request = $request->all();
        $request['abertura'] = "$aberturaAno-$aberturaMes-$aberturaDia";

        $rsCliente->create($request);

        return redirect(route('pessoas.clientes'));
   }

   public function Update(Request $request)
   {

    // $rsCliente = $this->tbClientes->find($request->id);
    $rsCliente = $this->tbClientes->find($request->id);
    $aberturaDia = substr($request->abertura,0,2);
    $aberturaMes = substr($request->abertura,3,2);
    $aberturaAno = substr($request->abertura,6,4);
    $request = $request->all();
    $request['abertura'] = "$aberturaAno-$aberturaMes-$aberturaDia";

    $rsCliente->fill($request);
    $rsCliente->update();

    return redirect(route('pessoas.clientes'));
   }

   public function delete()
   {
       return "Delete";
   }

   public function list($seq = 0, $search = "", $qtde = 20)
   {
        $seqTotal = 0;
        if(intval($seq) > 0){
            $seqTotal = ($seq * $qtde);
        }

        if(trim($search) != "")
        {
            $cnpjLimpo = str_replace('-','',$search);
            $cnpjLimpo = str_replace('.','',$cnpjLimpo);
            $cnpjLimpo = str_replace('/','',$cnpjLimpo);
            $rsClientes = $this->tbClientes
            ->leftJoin("pessoas",'pessoas.id','clientes.pessoa_id')
            ->orWhere("razaosocial","like","%$search%")
            ->orWhere("pessoa_id","like","%$search%")
            ->orWhere(DB::raw("replace(replace(replace(cnpj,'.',''),'-',''),'/','')"),"like","%$cnpjLimpo%")
            ->skip($seqTotal)
            ->limit($qtde)
            ->get();
        }
        else
        {
            $rsClientes = $this->tbClientes->skip($seqTotal)->limit($qtde)->get();
        }

        $regTotal = $this->tbClientes->get()->count();

        return view('Cadastro.Pessoa.Cliente.list',compact("rsClientes", "regTotal"));
   }


}
