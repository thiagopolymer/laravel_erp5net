<?php

namespace App\Http\Controllers\Comercial;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Storage;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Comercial\Sac;
use App\Model\Cadastro\Cliente;
use App\Model\Comercial\Ocorrencia;
use App\Model\Comercial\SacTemporizador;
use App\Model\Comercial\SacFile;

class SacController extends Controller
{
    private $tbSacs;
    private $tbClientes;
    private $tbOcorrencias;
    private $tbSacFiles;
    private $search;

    public function __construct()
    {
        $this->tbSacs = new Sac();
        $this->tbClientes = new Cliente();
        $this->tbOcorrencias = new Ocorrencia();
        $this->tbSacFiles = new SacFile();
        $this->middleware('auth');
    }

    public function index()
    {
        $tituloPagina = "SAC";

        return view('Comercial.Sac.index',compact("tituloPagina"));
    }

    public function edit($id = "0")
    {
        $tituloPagina = "SAC";
        $rsOcorrencias = $this->tbOcorrencias->orderBy("descricao")->get();
        $rsCliente = $this->tbClientes->where("pessoa_id",$id)->first();

        return view('Comercial.Sac.edit',compact("tituloPagina", "rsCliente", "rsOcorrencias"));
    }

    public function insert(Request $request)
    {
        $dados = $request->all();
        if(isset($dados['proximo_contato']))
        {
            $dataProximoContato = $dados['proximo_contato'];
        }
        else
        {
            $dataProximoContato = new \DateTime;
        }

        $reclamacao = 0;

        $files = $request->file('file');

        if(isset($dados['reclamacao']) )
        {
            $reclamacao = 1;
        }

        $tbSac = new Sac();
        $campos = [
            "pessoa_id"       => $dados['ocorrencia_pessoa_id'],
            "proximo_contato" => $dataProximoContato,
            "contato"         => $dados['contato'],
            "pedido"          => $dados['pedido'],
            "ocorrencia_id"   => $dados['ocorrencia_id'],
            "user_sac_id"     => Auth::user()->id,
            "obs_ocorrencia"  => $dados['obs'],
            "created_at"      => new \DateTime(),
            "reclamacao_id"   => $reclamacao,
            "user_id"         => Auth::user()->id
        ];

        $insertSac = $tbSac->create($campos);

        $seqFile = 0;
        $caminhoPasta = "//var/www/html/erp5net/storage/app/public/gravacoes/" . Auth()->user()->id;
        $caminhoSalvar = "//var/www/html/erp5net/storage/app/public/sac/prints/";
        $directory = dir($caminhoPasta);
        $seqFile = 0;
        while($arquivo = $directory->read())
        {
            if(trim($arquivo) != ".." && trim($arquivo) != ".")
            {
                $dataArquivo = substr(trim($arquivo),0,10);

                if($dataArquivo == date('Y-m-d'))
                {
                    $seqFile++;
                    $nameFile = 1 ."SAC" . $seqFile . ".png";
                    $insertSacFile = $this->tbSacFiles;
                    $insertFile = $insertSacFile->insert([
                        [
                            'nome'          => $nameFile,
                            "sac_id"        => $insertSac->id,
                            'created_at'    => new \DateTime(),
                            'user_id'       => Auth::user()->id,
                        ]
                    ]);
                    $originalName = $caminhoPasta . Auth()->user()->id . "/$arquivo";
                    $renameName = $caminhoSalvar . $nameFile;

                    shell_exec("mv $originalName $renameName" );
                }

            }
        }
        $directory -> close();


        if($files)
        {
            foreach($files as $file)
            {
                $extensaoFile = '.' . $file->getClientOriginalExtension(); //substr($file->getClientOriginalName(),-3);

                $seqFile++;
                $nameFile = $insertSac->id ."SAC" . $seqFile . $extensaoFile;
                $file->storeAs('public/sac/prints',$nameFile);
                $insertSacFile = $this->tbSacFiles;
                $insertFile = $insertSacFile->insert([
                    [
                        'nome'          => $nameFile,
                        "sac_id"        => $insertSac->id,
                        'created_at'    => new \DateTime(),
                        'user_id'       => Auth::user()->id,
                    ]
                ]);
            }
        }
        $tbSacTemporizador = new SacTemporizador();
        $tbSacTemporizador->insert([
            [
                'pessoa_id' => $dados['ocorrencia_pessoa_id'],
                'sac_id' => $insertSac->id->id,
                'tempo' => $dados['tempo'],
                'user_id' => Auth::user()->id,
                'created_at' => new \DateTime(),
            ]
        ]);

        return redirect(route('sacs'));
    }


    public function update()
    {
        return "Update";
    }

    public function delete()
    {
        return "Delete";
    }

    public function list_clientes($seq = 0, $search = "", $qtde = 20)
    {

         $seqTotal = 0;
         if(intval($seq) > 0){
             $seqTotal = ($seq * $qtde);
         }
         if(trim($search) != "")
         {
            $this->search = str_replace('-','',$search);
            $this->search = str_replace('.','',$this->search);
            $this->search = str_replace('/','',$this->search);
            $rsClientes = $this->tbClientes
                ->select(
                        "clientes.pessoa_id",
                        DB::raw('max(vw_sacs_nao_concluidos.proximo_contato) as proximo_contato'),
                        "pessoas.razaosocial",
                        "pessoas.cnpj",
                        "clientes.potencial_id",
                        "clientes.periodo_atividade_id",
                        "periodo_atividades.cor_hexa as periodo_cor_hexa",
                        "periodo_atividades.descricao as periodo_descricao",
                        "potenciais.cor_hexa as potencial_cor_hexa",
                        "potenciais.descricao as potencial_descricao"
                    )
                ->leftJoin("vw_sacs_nao_concluidos",'clientes.id','vw_sacs_nao_concluidos.pessoa_id')
                ->leftJoin("pessoas",'pessoas.id','clientes.pessoa_id')
                ->leftJoin("potenciais",'potenciais.id','clientes.potencial_id')
                ->leftJoin("periodo_atividades",'periodo_atividades.id','clientes.periodo_atividade_id')
                ->where(function ($query) {
                        $query->where("pessoas.razaosocial", "like", "%$this->search%")
                            ->orWhere("vw_sacs_nao_concluidos.pessoa_id", "like", "%$this->search%")
                            ->orWhere(DB::raw("replace(replace(replace(pessoas.cnpj,'.',''),'-',''),'/','')"),"like", "%$this->search%");
                        })
                ->groupBy(
                    "clientes.pessoa_id",
                    "pessoas.razaosocial",
                    "pessoas.cnpj",
                    "clientes.potencial_id",
                    "clientes.periodo_atividade_id",
                    "periodo_atividades.cor_hexa",
                    "periodo_atividades.descricao",
                    "potenciais.cor_hexa",
                    "potenciais.descricao"
                )
                ->orderBy(DB::raw('max(vw_sacs_nao_concluidos.proximo_contato)'))
                ->limit(20)
                ->get();
         }
         else
         {
            $user_id = Auth::user()->id;
            $rsClientes = $this->tbClientes
                ->select(
                        "clientes.pessoa_id",
                        DB::raw('max(vw_sacs_nao_concluidos.proximo_contato) as proximo_contato'),
                        "pessoas.razaosocial",
                        "pessoas.cnpj",
                        "clientes.potencial_id",
                        "clientes.periodo_atividade_id",
                        "periodo_atividades.cor_hexa as periodo_cor_hexa",
                        "periodo_atividades.descricao as periodo_descricao",
                        "potenciais.cor_hexa as potencial_cor_hexa",
                        "potenciais.descricao as potencial_descricao"
                    )
                ->leftJoin("vw_sacs_nao_concluidos",'clientes.id','vw_sacs_nao_concluidos.pessoa_id')
                ->leftJoin("pessoas",'pessoas.id','clientes.pessoa_id')
                ->leftJoin("potenciais",'potenciais.id','clientes.potencial_id')
                ->leftJoin("periodo_atividades",'periodo_atividades.id','clientes.periodo_atividade_id')
                ->where([
                        ['vw_sacs_nao_concluidos.user_sac_id',$user_id],
                    ])
                ->groupBy(
                    "clientes.pessoa_id",
                    "pessoas.razaosocial",
                    "pessoas.cnpj",
                    "clientes.potencial_id",
                    "clientes.periodo_atividade_id",
                    "periodo_atividades.cor_hexa",
                    "periodo_atividades.descricao",
                    "potenciais.cor_hexa",
                    "potenciais.descricao"
                )
                ->orderBy(DB::raw('max(vw_sacs_nao_concluidos.proximo_contato)'))
                ->limit(20)
                ->get();
         }

         $regTotal = $this->tbClientes->get()->count();

         return view('Comercial.Sac.list_clientes',compact("rsClientes", "regTotal"));
    }

    public function list($pessoa_id)
    {
        $rsSacs = $this->tbSacs->where('pessoa_id',$pessoa_id)->orderBy("id", "desc")->get();
        return view('Comercial.Sac.list',compact("rsSacs"));
    }

    public function concluido($id)
    {
        $rsSac = $this->tbSacs->find($id);
        $rsSac->concluido = 1;
        $rsSac->user_id = Auth::user()->id;
        $rsSac->update();

        return response()->json($rsSac);
    }

    public function details($id)
    {
        $rsSac = $this->tbSacs->find($id);
        $rsSacFiles = $this->tbSacFiles->where("sac_id",$id)->get();
        return view('Comercial.Sac.detail',compact("rsSac", "rsSacFiles"));
    }

    public function sac_form()
    {
        $rsOcorrencias = $this->tbOcorrencias->orderBy("descricao")->get();
        return view('Comercial.Sac.form_ocorrencia',compact("rsOcorrencias"));
    }

    public function temp_insert($pessoa_id = 0, $temp = "00:00")
    {
        $data = array($pessoa_id, $temp);
        $ultimoSac = $this->tbSacs
            ->where('pessoa_id',$pessoa_id)
            ->orderBy("id", "desc")
            ->first();

        $tbSacTemporizador = new SacTemporizador();
        $tbSacTemporizador->insert([
            [
                'pessoa_id' => $pessoa_id,
                'sac_id' => $ultimoSac->id,
                'tempo' => $temp,
                'user_id' => Auth::user()->id,
                'created_at' => new \DateTime(),
            ]
        ]);
        return response()->json($data);
    }


    public function atualiza_periodo($pessoa_id = 0)
    {
        if($pessoa_id == 0)
        {
            $pessoas = $this->tbClientes->get();
        }
        else
        {
            $pessoas = $this->tbClientes->where("pessoa_id", $pessoa_id)->get();
        }

        foreach($pessoas as $regPessoa)
        {
            $sacs = $this->tbSacs;
            $result = $sacs->where([["pessoa_id",$regPessoa->pessoa_id],["ocorrencia_id",11]])->orderBy("id", "desc")->first();
            if(!$result)
            {
                $result = $sacs->where([["pessoa_id",$regPessoa->pessoa_id],["ocorrencia_id",12]])->orderBy("id", "desc")->first();
                if(!$result)
                {
                    $result = $sacs->where([["pessoa_id",$regPessoa->pessoa_id]])->orderBy("id", "desc")->limit(1)->first();
                    if(!$result)
                    {
                        $condicao = 0;
                    }
                    else
                    {
                        $condicao = 3;
                    }
                }
                else
                {
                    $condicao = 2;
                }
            }else
            {
                $condicao = 1;
            }
            // 0 = não existe ocorrência / 1 = Tem orrencia 11 / 2 = Não Tem a ocorrência 11 mas tem a 12 / 3 - Não tem ocorrencia 11 e nem 12

            if($condicao > 0)
            {
                $dtRegistro = date_create($result['proximo_contato']);
                $dtAtual = date_create();
                $subtrair = date_diff($dtRegistro, $dtAtual);
                $totalDias = intval(date_interval_format($subtrair, '%a'));
            }



            switch($condicao)
            {
                case 1:
                    if($totalDias > 270)
                    {
                        $codigoPeriodoAtividadeId = 9;
                    }
                    elseif($totalDias > 180 && $totalDias <= 270)
                    {
                        $codigoPeriodoAtividadeId = 10;
                    }
                    elseif($totalDias > 90 && $totalDias <= 180)
                    {
                        $codigoPeriodoAtividadeId = 11;
                    }
                    elseif($totalDias <= 90 )
                    {
                        $codigoPeriodoAtividadeId = 12;
                    }
                break;

                case 2:
                    if($totalDias > 270)
                    {
                        $codigoPeriodoAtividadeId = 5;
                    }
                    elseif($totalDias > 180 && $totalDias <= 270)
                    {
                        $codigoPeriodoAtividadeId = 6;
                    }
                    elseif($totalDias > 90 && $totalDias <= 180)
                    {
                        $codigoPeriodoAtividadeId = 7;
                    }
                    elseif($totalDias <= 90 )
                    {
                        $codigoPeriodoAtividadeId = 8;
                    }
                break;

                case 3:
                    if($totalDias > 270)
                    {
                        $codigoPeriodoAtividadeId = 1;
                    }
                    elseif($totalDias > 180 && $totalDias <= 270)
                    {
                        $codigoPeriodoAtividadeId = 2;
                    }
                    elseif($totalDias > 90 && $totalDias <= 180)
                    {
                        $codigoPeriodoAtividadeId = 3;
                    }
                    elseif($totalDias <= 90 )
                    {
                        $codigoPeriodoAtividadeId = 4;
                    }
                break;
                default:
                    $codigoPeriodoAtividadeId = 0;
                break;
            }
            // Atualiza registros do Cliente para Regras de CRM por venda
            if($condicao > 0)
            {
                DB::table('clientes')
                    ->where('pessoa_id', $regPessoa->pessoa_id)
                    ->update(['periodo_atividade_id' => $codigoPeriodoAtividadeId]);
            }
        }
        return '<script type="text/javascript">window.close();</script>';
    }
    public function verifica_proximo_contato($potencial = 0, $periodo_atividade = 0, $contato)
    {
        $dataProximoContato = $contato;
        $dados = array();
        $qtdeAtendimentoDiaTotal = 0;
        switch($potencial)
        {
            case 1:
                if($periodo_atividade >= 1 && $periodo_atividade <= 4)
                {
                    $qtdeAtendimentoDiaTotal = 1;
                }
                else if($periodo_atividade >= 5 && $periodo_atividade <= 8)
                {
                    $qtdeAtendimentoDiaTotal = 2;
                }
                else if($periodo_atividade >= 9 && $periodo_atividade <= 12)
                {
                    $qtdeAtendimentoDiaTotal = 3;
                }
                break;

            case 2:
                if($periodo_atividade >= 1 && $periodo_atividade <= 4)
                {
                    $qtdeAtendimentoDiaTotal = 2;
                }
                else if($periodo_atividade >= 5 && $periodo_atividade <= 8)
                {
                    $qtdeAtendimentoDiaTotal = 3;
                }
                else if($periodo_atividade >= 9 && $periodo_atividade <= 12)
                {
                    $qtdeAtendimentoDiaTotal = 5;
                }
                break;
            case 3:
                if($periodo_atividade >= 1 && $periodo_atividade <= 4)
                {
                    $qtdeAtendimentoDiaTotal = 3;
                }
                else if($periodo_atividade >= 5 && $periodo_atividade <= 8)
                {
                    $qtdeAtendimentoDiaTotal = 5;
                }
                else if($periodo_atividade >= 9 && $periodo_atividade <= 12)
                {
                    $qtdeAtendimentoDiaTotal = 8;
                }
                break;
            case 4:
                if($periodo_atividade >= 1 && $periodo_atividade <= 4)
                {
                    $qtdeAtendimentoDiaTotal = 3;
                }
                else if($periodo_atividade >= 5 && $periodo_atividade <= 8)
                {
                    $qtdeAtendimentoDiaTotal = 5;
                }
                else if($periodo_atividade >= 9 && $periodo_atividade <= 12)
                {
                    $qtdeAtendimentoDiaTotal = 8;
                }
                break;
            case 5:
                if($periodo_atividade >= 1 && $periodo_atividade <= 4)
                {
                    $qtdeAtendimentoDiaTotal = 3;
                }
                else if($periodo_atividade >= 5 && $periodo_atividade <= 8)
                {
                    $qtdeAtendimentoDiaTotal = 5;
                }
                else if($periodo_atividade >= 9 && $periodo_atividade <= 12)
                {
                    $qtdeAtendimentoDiaTotal = 8;
                }
                break;
            default:
                $qtdeAtendimentoDiaTotal = 0;
                break;
        }
        $rsSacAgendamentos = $this->tbSacs->where('sacs.proximo_contato',$dataProximoContato)->orderBY('proximo_contato','desc')->get();

        $rsSacAgendamentoTipo = $this->tbSacs
            ->select('sacs.pessoa_id')
            ->where([
                ['sacs.proximo_contato',$dataProximoContato],
                ['clientes.potencial_id',$potencial],
                ['clientes.periodo_atividade_id',$periodo_atividade]
            ])
            ->leftJoin("clientes",'clientes.pessoa_id','sacs.pessoa_id')
            ->groupBy('sacs.pessoa_id')
            ->get();
        if($rsSacAgendamentos->count() > 45)
        {
            $rsSacAgendamentos = $this->tbSacs
                ->select(DB::raw('count(sacs.id) as qtde'), "sacs.proximo_contato")
                ->leftJoin("clientes",'clientes.pessoa_id','sacs.pessoa_id')
                ->where([["sacs.proximo_contato",'>',$dataProximoContato]])
                ->groupBy(["sacs.proximo_contato"])
                ->havingRaw('count(sacs.id) < ?', [45])
                ->orderBy("sacs.proximo_contato",'asc')
                ->first();

            $rsSacAgendamentoTipo = $this->tbSacs
                ->select('sacs.pessoa_id')
                ->where([
                    ['sacs.proximo_contato', '>', $dataProximoContato],
                    ['clientes.potencial_id',$potencial],
                    ['clientes.periodo_atividade_id',$periodo_atividade]
                ])
                ->leftJoin("clientes",'clientes.pessoa_id','sacs.pessoa_id')
                ->groupBy('sacs.pessoa_id')
                ->get();

            if($rsSacAgendamentoTipo->count() >= $qtdeAtendimentoDiaTotal)
            {
                $rsSacAgendamentoTipo = $this->tbSacs
                ->select(DB::raw('count(sacs.id) as qtde'),'sacs.proximo_contato')
                ->where([
                    ['sacs.proximo_contato', '>', $dataProximoContato],
                    ['clientes.potencial_id',$potencial],
                    ['clientes.periodo_atividade_id',$periodo_atividade]
                ])
                ->leftJoin("clientes",'clientes.pessoa_id','sacs.pessoa_id')
                ->havingRaw('count(sacs.id) < ?', [$qtdeAtendimentoDiaTotal])
                ->groupBy('sacs.proximo_contato')
                ->orderBy('sacs.proximo_contato','asc')
                ->first();

                $dataProximoContato = $rsSacAgendamentoTipo->proximo_contato;
                $dados = ['proximo_contato' => $dataProximoContato, 'tipo' => 1, 'qtdeDia' => $qtdeAtendimentoDiaTotal];
            }
            else
            {
                $dataProximoContato = $rsSacAgendamentos->proximo_contato;
                $dados = ['proximo_contato' => $dataProximoContato, 'tipo' => 0, 'qtdeDia' => $qtdeAtendimentoDiaTotal];
            }
        }
        else if($rsSacAgendamentos->count() > 0 && $rsSacAgendamentos->count() < 45)
        {
            if($rsSacAgendamentoTipo->count() >= $qtdeAtendimentoDiaTotal)
            {
                $rsSacAgendamentoTipo = $this->tbSacs
                ->select(DB::raw('count(sacs.id) as qtde'),'sacs.proximo_contato')
                ->where([
                    ['sacs.proximo_contato', '>', $dataProximoContato],
                    ['clientes.potencial_id',$potencial],
                    ['clientes.periodo_atividade_id',$periodo_atividade]
                ])
                ->leftJoin("clientes",'clientes.pessoa_id','sacs.pessoa_id')
                ->havingRaw('count(sacs.id) < ?', [$qtdeAtendimentoDiaTotal])
                ->groupBy('sacs.proximo_contato')
                ->orderBy('sacs.proximo_contato','asc')
                ->first();

                $dataProximoContato = $rsSacAgendamentoTipo->proximo_contato;
                $dados = ['proximo_contato' => $dataProximoContato, 'tipo' => 1, 'qtdeDia' => $qtdeAtendimentoDiaTotal];
            }
            else
            {
                $dataProximoContato = $rsSacAgendamentos[0]["proximo_contato"];
                $dados = ['proximo_contato' => $dataProximoContato, 'tipo' => 0, 'qtdeDia' => $qtdeAtendimentoDiaTotal];
            }
        }
        else{}

        return response()->json($dados);
    }

    public function relatorio_filtros()
    {
        $tituloPagina = "SAC - Relatório";
        return view('Comercial.Sac.relatorio_filtros',compact("tituloPagina"));
    }

    public function select_users()
    {
        $rsUsers = DB::table('users')->get();
        return view('Comercial.Sac.ListasSelects.users',compact("rsUsers"));
    }

    public function select_estados()
    {
        $rsEstados = DB::table('estados')->get();
        return view('Comercial.Sac.ListasSelects.estados',compact("rsEstados"));
    }

    public function select_classificacoes()
    {
        $rsClassificacoes = DB::table('classificacoes')->get();
        return view('Comercial.Sac.ListasSelects.classificacoes',compact("rsClassificacoes"));
    }

    public function select_potenciais()
    {
        $rsPotenciais = DB::table('potenciais')->get();
        return view('Comercial.Sac.ListasSelects.potenciais',compact("rsPotenciais"));
    }

    public function select_periodo_atividades()
    {
        $rsPeriodoAtividades = DB::table('periodo_atividades')->get();
        return view('Comercial.Sac.ListasSelects.periodo_atividades',compact("rsPeriodoAtividades"));
    }

    public function select_cidades()
    {
        return view('Comercial.Sac.ListasSelects.cidades');
    }

    public function list_cidades(Request $request)
    {
        $search = $request->query('search');
        $rsCidades = DB::table('cidades')
            ->orWhere('id','like',"%$search%")
            ->orWhere('cod_ibge','like',"%$search%")
            ->orWhere('nome','like',"%$search%")
        ->paginate(10);

        return view('Comercial.Sac.ListasSelects.list_cidades',compact("rsCidades"));
    }

    public function select_representantes()
    {
        return view('Comercial.Sac.ListasSelects.representantes');
    }

    public function list_representantes(Request $request)
    {
        $search = $request->query('search');
        $rsRepresentantes = DB::table('representantes as rep')
            ->select('rep.id', 'rep.pessoa_id', 'pes.razaosocial', 'pes.cnpj')
            ->leftJoin('pessoas as pes','pes.id','rep.pessoa_id')
            ->orWhere('rep.pessoa_id','like',"%$search%")
            ->orWhere('pes.razaosocial','like',"%$search%")
            ->orWhere('pes.cnpj','like',"%$search%")
            ->paginate(10);

        return view('Comercial.Sac.ListasSelects.list_representantes',compact("rsRepresentantes"));
    }

    public function select_transportadoras()
    {
        return view('Comercial.Sac.ListasSelects.transportadoras');
    }

    public function list_transportadoras(Request $request)
    {
        $search = $request->query('search');
        $rsTransportadoras = DB::table('transportadoras as tra')
            ->select('tra.id', 'tra.pessoa_id', 'pes.razaosocial', 'pes.cnpj')
            ->leftJoin('pessoas as pes','pes.id','tra.pessoa_id')
            ->orWhere('tra.pessoa_id','like',"%$search%")
            ->orWhere('pes.razaosocial','like',"%$search%")
            ->orWhere('pes.cnpj','like',"%$search%")
            ->paginate(10);

        return view('Comercial.Sac.ListasSelects.list_transportadoras',compact("rsTransportadoras"));
    }

    public function select_clientes()
    {
        return view('Comercial.Sac.ListasSelects.clientes');
    }

    public function list_clientes_selects(Request $request)
    {
        $search = $request->query('search');
        $rsClientes = DB::table('clientes as cli')
            ->select('cli.id', 'cli.pessoa_id', 'pes.razaosocial', 'pes.cnpj')
            ->leftJoin('pessoas as pes','pes.id','cli.pessoa_id')
            ->orWhere('cli.pessoa_id','like',"%$search%")
            ->orWhere('pes.razaosocial','like',"%$search%")
            ->orWhere('pes.cnpj','like',"%$search%")
            ->paginate(10);

        return view('Comercial.Sac.ListasSelects.list_clientes',compact("rsClientes"));
    }

    public function relatorio_print(Request $request)
    {
        $rsClientes = DB::table('clientes as cli')
            ->leftJoin('pessoas as pes', 'pes.id', 'cli.pessoa_id');
        $rsTransportadoras = DB::table('transportadoras as tra')
            ->leftJoin('pessoas as pes', 'pes.id', 'tra.pessoa_id');
        $rsRepresentantes = DB::table('representantes as rep')
            ->leftJoin('pessoas as pes', 'pes.id', 'rep.pessoa_id');

        if(trim($request->dt_inclusao_de[0]) == "" && trim($request->dt_inclusao_ate[0]) == "" && trim($request->proximo_contato_de[0]) != "" && trim($request->proximo_contato_de[0]) != "")
        {
            $rsSacsPessoasId = DB::table('sacs')
            ->select("pessoa_id")
            ->where("pessoa_id", ">",0)
            ->whereBetween('proximo_contato',[$request->proximo_contato_de,$request->proximo_contato_ate])
            ->groupBy("pessoa_id")
            ->get();

            $idPessoasSacs = array();

            foreach($rsSacsPessoasId as $reg)
            {
                $idPessoasSacs[] = $reg->pessoa_id;
            }

            $rsClientes = $rsClientes->whereIn("pessoa_id", $idPessoasSacs);
            $rsTransportadoras = $rsTransportadoras->whereIn("pessoa_id", $idPessoasSacs);
            $rsRepresentantes = $rsRepresentantes->whereIn("pessoa_id", $idPessoasSacs);
        }
        if(isset($request['clientes_id']) && trim($request->clientes_id[0]) != "")
        {
            $idInCliente = explode(',' ,$request->clientes_id);
            switch(count($idInCliente))
            {
                case 1:
                    $idInCliente = array($request->clientes_id);
                    break;
            }
            $rsClientes = $rsClientes->whereIn("cli.pessoa_id", $idInCliente);
        }

        if(isset($request['dt_inclusao_de']) && trim($request->dt_inclusao_de[0]) != "" && isset($request['dt_inclusao_ate']) && trim($request->dt_inclusao_ate[0]) != "")
        {
            $rsClientes = $rsClientes->whereBetween("cli.created_at", [$request->dt_inclusao_de, $request->dt_inclusao_ate]);
            $rsTransportadoras = $rsTransportadoras->whereBetween("tra.created_at", [$request->dt_inclusao_de, $request->dt_inclusao_ate]);
            $rsRepresentantes = $rsRepresentantes->whereBetween("rep.created_at", [$request->dt_inclusao_de, $request->dt_inclusao_ate]);
        }
        elseif(isset($request['dt_inclusao_de']) && trim($request->dt_inclusao_de[0]) != "")
        {
            $rsClientes = $rsClientes->where("cli.created_at", '>=',$request->dt_inclusao_de);
            $rsTransportadoras = $v->where("tra.created_at", '>=',$request->dt_inclusao_de);
            $rsRepresentantes = $rsRepresentantes->where("rep.created_at", '>=',$request->dt_inclusao_de);
        }
        elseif(isset($request['dt_inclusao_de']) && trim($request->dt_inclusao_de[0]) != "")
        {
            $rsClientes = $rsClientes->where("cli.created_at", '<=',$request->dt_inclusao_ate);
            $rsTransportadoras = $rsTransportadoras->where("tra.created_at", '<=',$request->dt_inclusao_ate);
            $idInRepresentantes = $idInRepresentantes->where("rep.created_at", '<=',$request->dt_inclusao_ate);
        }

        if(isset($request['transportadora_id']) && trim($request->transportadora_id[0]) != "")
        {
            $idInTransportadora = explode(',' ,$request->transportadora_id);
            switch(count($idInTransportadora))
            {
                case 1:
                    $idInTransportadora = array($request->transportadora_id);
                    break;
            }
            $rsTransportadoras = $rsTransportadoras->whereIn("tra.id", $idInTransportadora);
        }

        if(isset($request['representante_id']) && trim($request->representante_id[0]) != "")
        {
            $idInRepresentantes = explode(',' ,$request->representante_id);
            switch(count($idInRepresentantes))
            {
                case 1:
                    $idInRepresentantes = array($request->representante_id);
                    break;
            }
            $rsRepresentantes = $rsRepresentantes->whereIn("rep.id", $idInRepresentantes);
        }

        if(isset($request['user_id']) && trim($request->user_id[0]) != "")
        {
            $idInUser = explode(',' ,$request->user_id);
            if(count($idInUser) == 1);
            {
                $idInUser = array($request->user_id);
            }
            $rsClientes = $rsClientes
                ->whereIn("cli.user_id", $idInUser);
            $rsTransportadoras = $rsTransportadoras
                ->whereIn("tra.user_id", $idInUser);
            $rsRepresentantes = $rsRepresentantes
                ->whereIn("rep.user_id", $idInUser);
        }

        if(isset($request['periodo_atividade_id']) && trim($request->periodo_atividade_id[0]) != "")
        {
            $idInPeriodoAtividades = explode(',' ,$request->periodo_atividade_id);
            if(count($idInPeriodoAtividades) == 1);
            {
                $idInPeriodoAtividades = array($request->periodo_atividade_id);
            }
            $rsClientes = $rsClientes
                ->whereIn("cli.periodo_atividade_id", $idInPeriodoAtividades);
        }

        if(isset($request['potencial_id']) && trim($request->potencial_id[0]) != "")
        {
            $idInPotencial = explode(',' ,$request->potencial_id);
            if(count($idInPotencial) == 1);
            {
                $idInPotencial = array($request->potencial_id);
            }
            $rsClientes = $rsClientes
                ->whereIn("cli.potencial_id", $idInPotencial);
        }

        if(isset($request['classificacao_id']) && trim($request->classificacao_id[0]) != "")
        {
            $idInClassificacoes = explode(',' ,$request->classificacao_id);
            if(count($idInClassificacoes) == 1);
            {
                $idInClassificacoes = array($request->classificacao_id);
            }
            $rsClientes = $rsClientes
                ->whereIn("cli.classificacao_id", $idInClassificacoes);
        }

        if(isset($request['razaosocial']) && trim($request->razaosocial) != "")
        {
            $rsClientes = $rsClientes
                ->where("pes.razaosocial", 'like',"%$request->razaosocial%");
            $rsTransportadoras = $rsTransportadoras
                ->where("pes.razaosocial", 'like',"%$request->razaosocial%");
            $rsRepresentantes = $rsRepresentantes
                ->where("pes.razaosocial", 'like',"%$request->razaosocial%");
        }

        if(isset($request['cnpj']) && trim($request->cnpj) != "")
        {
            $rsClientes = $rsClientes
                ->where("pes.cnpj", 'like',"%$request->cnpj%");
            $rsTransportadoras = $rsTransportadoras
                ->where("pes.cnpj", 'like',"%$request->cnpj%");
            $rsRepresentantes = $rsRepresentantes
                ->where("pes.cnpj", 'like',"%$request->cnpj%");
        }

        if(isset($request['nomefantasia']) && trim($request->nomefantasia) != "")
        {
            $rsClientes = $rsClientes
                ->where("pes.nomefantasia", 'like',"%$request->nomefantasia%");
            $rsTransportadoras = $rsTransportadoras
                ->where("pes.nomefantasia", 'like',"%$request->nomefantasia%");
            $rsRepresentantes = $rsRepresentantes
                ->where("pes.nomefantasia", 'like',"%$request->nomefantasia%");
        }

        if(isset($request['cep']) && trim($request->cep) != "")
        {
            $rsClientes = $rsClientes
                ->where("pes.cep", 'like',"%$request->cep%");
            $rsTransportadoras = $rsTransportadoras
                ->where("pes.cep", 'like',"%$request->cep%");
            $rsRepresentantes = $rsRepresentantes
                ->where("pes.cep", 'like',"%$request->cep%");
        }

        if(isset($request['cidade_id']) && trim($request->cidade_id[0]) != "")
        {
            $idInUser = explode(',' ,$request->cidade_id);
            if(count($idInCidades) == 1);
            {
                $idInCidades = array($request->cidade_id);
            }
            $rsClientes = $rsClientes
                ->whereIn("pes.cidade_id", $idInCidades);
            $rsTransportadoras = $rsTransportadoras
                ->whereIn("pes.cidade_id", $idInCidades);
            $rsRepresentantes = $rsRepresentantes
                ->whereIn("pes.cidade_id", $idInCidades);
        }

        $rsClientes = $rsClientes->get();

        $rsTransportadoras = $rsTransportadoras->get();

        $rsRepresentantes = $rsRepresentantes->get();

        $rsSacs = DB::table('sacs')
            ->select("pessoa_id",DB::raw('
                    sum(if(ocorrencia_id = 1,1,0)) as ocorrencia_1,
                    sum(if(ocorrencia_id = 2,1,0)) as ocorrencia_2,
                    sum(if(ocorrencia_id = 8,1,0)) as ocorrencia_8,
                    sum(if(ocorrencia_id = 9,1,0)) as ocorrencia_9,
                    sum(if(ocorrencia_id = 11,1,0)) as ocorrencia_11,
                    sum(if(ocorrencia_id = 12,1,0)) as ocorrencia_12,
                    sum(if(ocorrencia_id = 14,1,0)) as ocorrencia_14,
                    sum(if(ocorrencia_id = 19,1,0)) as ocorrencia_19,
                    sum(if(ocorrencia_id = 20,1,0)) as ocorrencia_20,
                    sum(if(ocorrencia_id = 23,1,0)) as ocorrencia_23,
                    sum(if(ocorrencia_id = 24,1,0)) as ocorrencia_24,
                    sum(if(ocorrencia_id = 38,1,0)) as ocorrencia_38,
                    sum(if(ocorrencia_id = 39,1,0)) as ocorrencia_39,
                    sum(if(ocorrencia_id = 10,1,0)) as ocorrencia_40,
                    sum(if(ocorrencia_id = 41,1,0)) as ocorrencia_41,
                    sum(if(ocorrencia_id = 42,1,0)) as ocorrencia_42,
                    sum(if(coalesce(ocorrencia_id) = 0,1,0)) as sem_ocorrencia
                '))
            ->where("pessoa_id", ">",0)
            ->whereBetween('proximo_contato',[$request->proximo_contato_de,$request->proximo_contato_ate])
            ->groupBy("pessoa_id");

            if(isset($request['proximo_contato_de']) && trim($request->proximo_contato_de[0]) != "" && isset($request['proximo_contato_ate']) && trim($request->proximo_contato_ate[0]) != "")
            {
                $rsSacs = $rsSacs->whereBetween("proximo_contato", [$request->proximo_contato_de, $request->proximo_contato_ate]);
            }
            elseif(isset($request['proximo_contato_de']) && trim($request->proximo_contato_de[0]) != "")
            {
                $rsSacs = $rsSacs->where("proximo_contato", '>=',$request->proximo_contato_de);
            }
            elseif(isset($request['proximo_contato_ate']) && trim($request->proximo_contato_ate[0]) != "")
            {
                $rsSacs = $rsSacs->where("proximo_contato", '<=',$request->dt_inclusao_ate);
            }
            $rsSacs = $rsSacs->get();

            return view('Comercial.relatorio',compact("rsClientes", "rsTransportadoras", "rsRepresentantes", "rsSacs"));
    }

    public function select_ocorrencias()
    {
        $rsOcorrencias = DB::table('ocorrencias')->get();
        return view('Comercial.Sac.ListasSelects.ocorrencias',compact("rsOcorrencias"));
    }

    public function relatorio_filtros_segundo()
    {
        $tituloPagina = "SAC - Relatório por hora de antedimento";
        return view('Comercial.Sac.relatorio_filtros_segundo',compact("tituloPagina"));
    }

    public function relatorio_print_segundo(Request $request)
    {
        $rsClientes = DB::table('clientes as cli')
            ->leftJoin('pessoas as pes', 'pes.id', 'cli.pessoa_id');
        $rsRepresentantes = DB::table('representantes as rep')
            ->leftJoin('pessoas as pes', 'pes.id', 'rep.pessoa_id');
        $rsSacTemporizadores = DB::table('sac_temporizadores as tem')
            ->select(
                "tem.pessoa_id",
                "sac.ocorrencia_id",
                "oco.descricao",
                DB::raw('sum(cast(substr(tem.tempo,1,2) as unsigned integer)) as minutos,
                sum(cast(substr(tem.tempo,4,5) as unsigned integer)) as segundos')
            )
            ->leftJoin("sacs as sac", "sac.id", "tem.sac_id")
            ->leftJoin("ocorrencias as oco", "oco.id", "sac.ocorrencia_id")
            ->leftJoin("pessoas as pes", "pes.id", "tem.pessoa_id")
            ->leftJoin("representantes as rep", "rep.pessoa_id", "tem.pessoa_id")
            ->leftJoin("clientes as cli", "cli.pessoa_id", "tem.pessoa_id")
            ->leftJoin("cidades as cid", "cid.id", "pes.cidade_id")
            ->leftJoin("estados as est", "est.id", "cid.estado_id")
            ->groupBy("tem.pessoa_id", "sac.ocorrencia_id", "oco.descricao")

            ->orderBy("sac.pessoa_id");

        if(isset($request['dt_inclusao_de']) && trim($request->dt_inclusao_de[0]) != "" && isset($request['dt_inclusao_ate']) && trim($request->dt_inclusao_ate[0]) != "")
        {
            $rsSacTemporizadores = $rsSacTemporizadores->whereBetween("tem.created_at", [$request->dt_inclusao_de, $request->dt_inclusao_ate]);
        }
        elseif(isset($request['dt_inclusao_de']) && trim($request->dt_inclusao_de[0]) != "")
        {
            $rsSacTemporizadores = $rsSacTemporizadores->where("tem.created_at", '>=',$request->dt_inclusao_de);
        }
        elseif(isset($request['dt_inclusao_de']) && trim($request->dt_inclusao_de[0]) != "")
        {
            $rsSacTemporizadores = $rsSacTemporizadores->where("tem.created_at", '<=',$request->dt_inclusao_ate);
        }

        if(isset($request['proximo_contato_de']) && trim($request->proximo_contato_de[0]) != "" && isset($request['proximo_contato_ate']) && trim($request->proximo_contato_ate[0]) != "")
        {
            $rsSacTemporizadores = $rsSacTemporizadores->whereBetween("sac.proximo_contato", [$request->proximo_contato_de, $request->proximo_contato_ate]);
        }
        elseif(isset($request['proximo_contato_de']) && trim($request->proximo_contato_de[0]) != "")
        {
            $rsSacTemporizadores = $rsSacTemporizadores->where("sac.proximo_contato", '>=',$request->proximo_contato_de);
        }
        elseif(isset($request['proximo_contato_ate']) && trim($request->proximo_contato_ate[0]) != "")
        {
            $rsSacTemporizadores = $rsSacTemporizadores->where("sac.proximo_contato", '<=',$request->dt_inclusao_ate);
        }


        if(isset($request['clientes_id']) && trim($request->clientes_id[0]) != "")
        {
            $idInCliente = explode(',' ,$request->clientes_id);
            switch(count($idInCliente))
            {
                case 1:
                    $idInCliente = array($request->clientes_id);
                    break;
            }
            $rsSacTemporizadores = $rsSacTemporizadores->whereIn("tem.pessoa_id", $idInCliente);
        }

        if(isset($request['ocorrencia_id']) && trim($request->ocorrencia_id[0]) != "")
        {
            $idInOcorrencia = explode(',' ,$request->ocorrencia_id);
            switch(count($idInOcorrencia))
            {
                case 1:
                    $idInOcorrencia = array($request->ocorrencia_id);
                    break;
            }
            $rsSacTemporizadores = $rsSacTemporizadores->whereIn("sac.ocorrencia_id", $idInOcorrencia);
        }

        if(isset($request['cidade_id']) && trim($request->cidade_id[0]) != "")
        {
            $idInCidades = explode(',' ,$request->cidade_id);
            switch(count($idInCidades))
            {
                case 1:
                    $idInCidades = array($request->cidade_id);
                    break;
            }
            $rsSacTemporizadores = $rsSacTemporizadores->whereIn("pes.cidade_id", $idInCidades);
        }

        if(isset($request['representante_id']) && trim($request->representante_id[0]) != "")
        {
            $idInRepresentantes = explode(',' ,$request->representante_id);
            switch(count($idInRepresentantes))
            {
                case 1:
                    $idInRepresentantes = array($request->representante_id);
                    break;
            }
            $rsSacTemporizadores = $rsSacTemporizadores->whereIn("cli.representante_id", $idInRepresentantes);
        }

        if(isset($request['estado_id']) && trim($request->estado_id[0]) != "")
        {
            $idInEstados = explode(',' ,$request->estado_id);
            switch(count($idInEstados))
            {
                case 1:
                    $idInEstados = array($request->estado_id);
                    break;
            }
            $rsSacTemporizadores = $rsSacTemporizadores->whereIn("cid.estado_id", $idInEstados);
        }

        if(isset($request['user_id']) && trim($request->user_id[0]) != "")
        {
            $idInUsers = explode(',' ,$request->user_id);
            switch(count($idInUsers))
            {
                case 1:
                    $idInUsers = array($request->user_id);
                    break;
            }
            $rsSacTemporizadores = $rsSacTemporizadores->whereIn("tem.user_id", $idInUsers);
        }

        $rsSacTemporizadores = $rsSacTemporizadores->get();

        $idDuplicidade = 0;
        $idInPessoas = array();

        foreach($rsSacTemporizadores as $reg)
        {

            if($idDuplicidade != $reg->pessoa_id)
            {
                $idInPessoas[] = $reg->pessoa_id;
            }

            $idDuplicidade = $reg->pessoa_id;
        }

        $rsClientes = $rsClientes->whereIn('cli.pessoa_id',$idInPessoas)->get();
        $rsRepresentantes = $rsRepresentantes->whereIn('rep.pessoa_id',$idInPessoas)->get();

        return view('Comercial.relatorio_segundo',compact("rsSacTemporizadores","rsClientes","rsRepresentantes"));
    }

    public function teste()
    {

        return "Para teste";
    }

}
