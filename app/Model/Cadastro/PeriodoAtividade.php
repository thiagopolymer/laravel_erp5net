<?php

namespace App\Model\Cadastro;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;

class PeriodoAtividade extends Model
{
    protected $table = "periodo_atividades";

    protected $fillable = [
        'descricao'
    ];


}
