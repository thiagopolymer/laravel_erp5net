<?php

namespace App\Model\Cadastro;

use Illuminate\Database\Eloquent\Model;
use App\Model\Cadastro\Pessoa;

class Transportadora extends Model
{
    protected $table = "transportadoras";

    protected $fillable = [
        "pessoa_id",
        "coleta",
        "taxa",
        "valor",
        "obs"
    ];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id');
    }

}
