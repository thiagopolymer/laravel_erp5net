<?php

namespace App\Model\Cadastro;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Contato extends Model
{
    protected $table = "contatos";

    protected $fillable = [
        'tipo_id',
        'nome',
        'fone',
        'ramal',
        'celular',
        'time_id',
        'email1',
        'email2',
        'hobby',
        'facebook',
        'instagran',
        'twitter',
        'linkedin',
        'youtube',
        'nascimento'
    ];
}
