<?php

namespace App\Model\Cadastro;

use Illuminate\Database\Eloquent\Model;

class Potencial extends Model
{
    protected $table = "potenciais";

    protected $fillable = [
        'descricao',
    ];
}
