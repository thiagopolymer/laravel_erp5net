<?php

namespace App\Model\Cadastro;

use Illuminate\Database\Eloquent\Model;

use App\Model\Outros\Cidade;
use App\Model\Outros\Pais;
use App\Model\Outros\Transportadora;
use App\Model\Outros\Representante;
use App\Model\Outros\Banco;

class Pessoa extends Model
{
    protected $table = "pessoas";

    protected $fillable = [
        'pessoa_fisica',
        'cnpj',
        'inscestadual',
        'razaosocial',
        'nomefantasia',
        'cep',
        'endereco',
        'numero',
        'bairro',
        'complemento',
        'cidade_id',
        'fone_receita',
        'ramal_receita',
        'fone_fiscal',
        'ramal_fiscal',
        'email_receita',
        'email_fiscal',
        'fone1',
        'ramal1',
        'email1',
        'fone2',
        'ramal2',
        'email2',
    ];

    public function cidade()
    {
        return $this->belongsTo(Cidade::class, 'cidade_id');
    }
}
