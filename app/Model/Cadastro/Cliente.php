<?php

namespace App\Model\Cadastro;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Query\Builder;
use App\Model\Cadastro\Cliente;
use App\Model\Cadastro\Potencial;
use App\Model\Cadastro\PeriodoAtividade;

class Cliente extends Model
{
    protected $table = "clientes";

    protected $fillable = [
        'pessoa_id',
        'classificacao_id',
        'representante_id',
        'transportadora_id',
        'potencial_id',
        'periodo_atividade_id',
        'indicador_ie_id',
        'preferencial',
        'suframa',
        'serasa_negativado',
        'facebook',
        'instagran',
        'twiter',
        'youtube',
        'linkedin',
        'cnaeprimario',
        'cnaesecundario1',
        'cnaesecundario2',
        'cnaesecundario3',
        'cnaesecundario4',
        'cnaesecundario5',
        'situacao',
        'porte',

    ];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id');
    }

    public function potencial()
    {
        return $this->belongsTo(Potencial::class, 'potencial_id');
    }

    public function periodo_atividade()
    {
        return $this->belongsTo(PeriodoAtividade::class, 'potencial_id');
    }

    // public function cidade()
    // {
    //     return $this->belongsTo(Cidade::class, 'cidade_id');
    // }

    // public function pais()
    // {
    //     return $this->belongsTo(Pais::class, 'pais_id');
    // }

    // public function representante()
    // {
    //     return $this->belongsTo(Representante::class, 'representante_id');
    // }

    // public function transportadora()
    // {
    //     return $this->belongsTo(Transportadora::class, 'transportadora_id');
    // }

    // public function ent_cidade()
    // {
    //     return $this->belongsTo(Cidade::class, 'ent_cidade_id');
    // }

    // public function cob_cidade()
    // {
    //     return $this->belongsTo(Cidade::class, 'cob_cidade_id');
    // }
}
