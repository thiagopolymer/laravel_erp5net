<?php

namespace App\Model\Cadastro;

use Illuminate\Database\Eloquent\Model;
use App\Model\Cadastro\Pessoa;

class Representante extends Model
{
    protected $table = "representantes";

    protected $fillable = [
        "pessoa_id",
        "comissao",
        "bacen_banco_id",
        "agencia",
        "conta",
        "obs"
    ];

    public function pessoa()
    {
        return $this->belongsTo(Pessoa::class, 'pessoa_id');
    }

}
