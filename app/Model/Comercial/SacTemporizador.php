<?php

namespace App\Model\Comercial;

use Illuminate\Database\Eloquent\Model;

class SacTemporizador extends Model
{
    protected $table = "sac_temporizadores";

    protected $fillable = [
        'sac_id',
        "tempo",
        "user_id",
    ];
}
