<?php

namespace App\Model\Comercial;

use Illuminate\Database\Eloquent\Model;

use App\Model\Comercial\Ocorrencia;

class Sac extends Model
{
    protected $table = "sacs";

    protected $fillable = [
        'pessoa_id',
        "proximo_contato",
        "ocorrencia_id",
        "pedido",
        "user_sac_id",
        "obs_ocorrencia",
        'created_at',
        'concluido',
        'reclamacao',
        "contato",
        'user_id',
    ];

    public function ocorrencia()
    {
        return $this->belongsTo(Ocorrencia::class, 'ocorrencia_id');
    }

}
