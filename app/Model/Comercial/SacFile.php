<?php

namespace App\Model\Comercial;

use Illuminate\Database\Eloquent\Model;

class SacFile extends Model
{
    protected $table = "sac_files";

    protected $fillable = [
        'nome',
        "sac_id",
        "user_id",
    ];

}
