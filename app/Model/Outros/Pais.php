<?php

namespace App\Model\Outros;

use Illuminate\Database\Eloquent\Model;

class Pais extends Model
{
    protected $table = "paises";
}
