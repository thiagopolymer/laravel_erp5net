<?php

namespace App\Model\Outros;
use App\Model\Outros\Estado;

use Illuminate\Database\Eloquent\Model;

class Cidade extends Model
{
    protected $table = "cidades";

    public function estado()
    {
        return $this->belongsTo(Estado::class, 'estado_id');
    }

}
