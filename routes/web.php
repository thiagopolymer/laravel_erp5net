<?php
    use Illuminate\Support\Facades\Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $tituloPagina = "Bem Vindo";

    return view('welcome',compact("tituloPagina"));
})->name("home")->middleware('auth');
Route::any('users/logout', function (){

    Auth::logout();

    return redirect(route('home'));

})->name("user.logout");

Auth::routes();

// Route::get('/', 'HomeController@index')->name('home');

Route::prefix('cadastro/')->group(function () {
    Route::prefix('pessoas')->group(function () {
        Route::get('/', 'Cadastro\PessoaController@index')->name('pessoas');
        Route::get('editar/{id?}', 'Cadastro\PessoaController@edit')->name('pessoas.edit');
        Route::post('insert', 'Cadastro\PessoaController@insert')->name('pessoas.insert');
        Route::post('update', 'Cadastro\PessoaController@update')->name('pessoas.update');
        Route::get('delete/{id?}', 'Cadastro\PessoaController@delete')->name('pessoas.delete');
        Route::get('list/{search?}', 'Cadastro\PessoaController@list')->name('pessoas.list');

        // Clientes -> Ínicio
        Route::prefix('clientes')->group(function () {
            Route::get('/', 'Cadastro\ClienteController@index')->name('pessoas.clientes');
            Route::get('editar/{id?}/{pessoa?}', 'Cadastro\ClienteController@edit')->name('pessoas.clientes.edit');
            Route::post('insert', 'Cadastro\ClienteController@insert')->name('pessoas.clientes.insert');
            Route::post('update', 'Cadastro\ClienteController@update')->name('pessoas.clientes.update');
            Route::get('delete/{id?}', 'Cadastro\ClienteController@delete')->name('pessoas.clientes.delete');
            Route::get('lista/{seq?}/{search?}', 'Cadastro\ClienteController@list')->name('pessoas.clientes.list');
        });
        // Clientes -> Fim

        // Fornecedores -> Ínicio
        Route::prefix('fornecedores')->group(function () {
            Route::get('/', 'Cadastro\FornecedorController@index')->name('pessoas.fornecedores');
            Route::get('editar/{id?}/{pessoa?}', 'Cadastro\FornecedorController@edit')->name('pessoas.fornecedores.edit');
            Route::post('insert', 'Cadastro\FornecedorController@insert')->name('pessoas.fornecedores.insert');
            Route::post('update', 'Cadastro\FornecedorController@update')->name('pessoas.fornecedores.update');
            Route::get('delete/{id?}', 'Cadastro\FornecedorController@delete')->name('pessoas.fornecedores.delete');
        });
        // Fornecedores -> Fim

        // Transportadoras -> Ínicio
        Route::prefix('transportadoras')->group(function () {
            Route::get('/', 'Cadastro\TransportadoraController@index')->name('pessoas.transportadoras');
            Route::get('editar/{id?}/{pessoa?}', 'Cadastro\TransportadoraController@edit')->name('pessoas.transportadoras.edit');
            Route::post('insert', 'Cadastro\TransportadoraController@insert')->name('pessoas.transportadoras.insert');
            Route::post('update', 'Cadastro\TransportadoraController@update')->name('pessoas.transportadoras.update');
            Route::get('delete/{id?}', 'Cadastro\TransportadoraController@delete')->name('pessoas.transportadoras.delete');
        });
        // Transportadoras -> Fim

        // Representantes -> Ínicio
        Route::prefix('representantes')->group(function () {
            Route::get('/', 'Cadastro\RepresentanteController@index')->name('pessoas.representantes');
            Route::get('editar/{id?}/{pessoa?}', 'Cadastro\RepresentanteController@edit')->name('pessoas.representantes.edit');
            Route::post('insert', 'Cadastro\RepresentanteController@insert')->name('pessoas.representantes.insert');
            Route::post('update', 'Cadastro\RepresentanteController@update')->name('pessoas.representantes.update');
            Route::get('delete/{id?}', 'Cadastro\RepresentanteController@delete')->name('pessoas.representantes.delete');
        });
        // Representantes -> Fim

    });

    Route::prefix('json')->group(function () {
        Route::get('/pessoa/{id?}', 'Api\ClienteController@pessoa')->name('api.pessoa');
        Route::get('/classificacao/{id?}', 'Api\ClienteController@classificacao')->name('api.classificacao');
        Route::get('/transportadora/{id?}', 'Api\ClienteController@transportadora')->name('api.transportadora');
        Route::get('/representante/{id?}', 'Api\ClienteController@representante')->name('api.representante');
        Route::get('/banco/{id?}', 'Api\ClienteController@banco')->name('api.banco');
        Route::get('/bacen_banco/{id?}', 'Api\ClienteController@bacen_banco')->name('api.bacen_banco');
        Route::get('/cidade/{id?}', 'Api\ClienteController@cidade')->name('api.cidade');
        Route::get('/cidade/nome/{nome?}', 'Api\ClienteController@cidade_nome')->name('api.cidade.nome');
        Route::post('/contatos/insert', 'Api\ClienteController@insert_contato')->name('api.insert_contato');
        Route::post('/clientes/lista', 'Api\ClienteController@cliente_list')->name('api.clientes.list');
    });

});

Route::prefix('comercial/')->group(function () {
    Route::prefix('sacs')->group(function () {
        Route::get('/', 'Comercial\SacController@index')->name('sacs');
        Route::get('editar/{id?}', 'Comercial\SacController@edit')->name('sacs.edit');
        Route::post('insert', 'Comercial\SacController@insert')->name('sacs.insert');
        Route::post('update', 'Comercial\SacController@update')->name('sacs.update');
        Route::get('delete/{id?}', 'Comercial\SacController@delete')->name('sacs.delete');
        Route::get('lista/clientes/{seq?}/{search?}', 'Comercial\SacController@list_clientes')->name('sacs.clientes.list');
        Route::get('lista/{seq?}/{search?}', 'Comercial\SacController@list')->name('sacs.list');
        Route::get('concluido/{id?}', 'Comercial\SacController@concluido')->name('sacs.concluido');
        Route::get('detatalhes/{id?}', 'Comercial\SacController@details')->name('sacs.details');
        Route::get('formulario', 'Comercial\SacController@sac_form')->name('sacs.form');
        Route::get('temporizador/insert/{pessoa_id?}/{temp?}', 'Comercial\SacController@temp_insert')->name('sacs.temp.insert');
        Route::get('atualiza/periodo/{pessoa_id?}', 'Comercial\SacController@atualiza_periodo')->name('sacs.atualiza_periodo');
        Route::get('verifica/proximo_contato/{potencial?}/{periodo_atividade?}/{contato?}', 'Comercial\SacController@verifica_proximo_contato')->name('sacs.date.next');
        Route::get('relatorio', 'Comercial\SacController@relatorio_filtros')->name('sacs.rel');
        Route::get('select/usuarios', 'Comercial\SacController@select_users')->name('sacs.selects.users');
        Route::get('select/estados', 'Comercial\SacController@select_estados')->name('sacs.selects.estados');
        Route::get('select/classificacoes', 'Comercial\SacController@select_classificacoes')->name('sacs.selects.classificacoes');
        Route::get('select/potenciais', 'Comercial\SacController@select_potenciais')->name('sacs.selects.potenciais');
        Route::get('select/periodo/atividades', 'Comercial\SacController@select_periodo_atividades')->name('sacs.selects.periodo_atividades');
        Route::get('select/cidades', 'Comercial\SacController@select_cidades')->name('sacs.selects.cidades');
        Route::get('select/cidades/list', 'Comercial\SacController@list_cidades')->name('sacs.selects.cidades.list');
        Route::get('select/representantes', 'Comercial\SacController@select_representantes')->name('sacs.selects.representantes');
        Route::get('select/representantes/list', 'Comercial\SacController@list_representantes')->name('sacs.selects.representantes.list');
        Route::get('select/transportadoras', 'Comercial\SacController@select_transportadoras')->name('sacs.selects.transportadoras');
        Route::get('select/transportadoras/list', 'Comercial\SacController@list_transportadoras')->name('sacs.selects.transportadoras.list');
        Route::get('select/clientes', 'Comercial\SacController@select_clientes')->name('sacs.selects.clientes');
        Route::get('select/clientes/list', 'Comercial\SacController@list_clientes_selects')->name('sacs.selects.clientes.list');
        Route::post('relatorio/imprimir', 'Comercial\SacController@relatorio_print')->name('sacs.relatorios.print');
        Route::get('select/ocorrencias', 'Comercial\SacController@select_ocorrencias')->name('sacs.selects.ocorrencias');
        Route::get('relatorio/segundo', 'Comercial\SacController@relatorio_filtros_segundo')->name('sacs.rel.segundo');
        Route::post('relatorio/segundo/imprimir', 'Comercial\SacController@relatorio_print_segundo')->name('sacs.relatorios.print.segundo');

    });
});
Route::get('teste', 'Comercial\SacController@teste');
