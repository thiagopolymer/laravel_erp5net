<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContatosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('contatos', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pessoa_id')->nullable()->default(0);
            $table->integer('tipo_id')->nullable()->default(0);
            $table->string('nome', 80)->nullable()->default('');
            $table->string('fone', 20)->nullable()->default('');
            $table->string('ramal', 10)->nullable()->default('');
			$table->string('celular', 20)->nullable()->default('');
			$table->string('email1', 100)->nullable()->default('');
			$table->string('email2', 100)->nullable()->default('');
			$table->string('hobby', 120)->nullable()->default('');
			$table->string('facebook', 120)->nullable()->default('');
			$table->string('instagran', 100)->nullable()->default('');
			$table->string('twitter', 100)->nullable()->default('');
			$table->string('linkedin', 100)->nullable()->default('');
			$table->string('youtube', 100)->nullable()->default('');
			$table->date('nascimento', 100)->nullable();
			$table->string('time_id', 60)->nullable()->default('');
            $table->integer('user_id')->default(0)->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('contatos');
    }
}
