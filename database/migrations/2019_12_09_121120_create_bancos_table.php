<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBancosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bancos', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->string('descricao', 50);
			$table->string('agencia', 10);
			$table->string('numero', 10);
			$table->decimal('limite', 10)->default(0.00);
			$table->string('gerente', 50)->default(' ');
			$table->string('fone1', 20)->nullable();
			$table->string('fone2', 20)->nullable();
            $table->integer('bacen_banco_id')->default(0);
            $table->integer('user_id')->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bancos');
    }
}
