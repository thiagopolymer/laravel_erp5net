<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransportadorasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transportadoras', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->integer('pessoa_id')->default(0)->nullable();
            $table->decimal('coleta',1,0)->default(0)->nullable();
            $table->decimal('taxa',1,0)->default(0)->nullable();
            $table->decimal('valor',10,2)->default(0)->nullable();
            $table->longText("obs")->nullable()->default(0);
            $table->integer("user_id")->nullable()->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transportadoras');
    }
}
