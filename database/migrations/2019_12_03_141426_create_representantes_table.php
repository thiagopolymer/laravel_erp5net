<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateRepresentantesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('representantes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("pessoa_id")->nullable()->default(0);
            $table->decimal("comissao",5,2)->nullable()->default(0);
            $table->integer("bacen_banco_id")->nullable()->default(0);
            $table->decimal("agencia",5,0)->nullable()->default(0);
            $table->decimal("conta",11,0)->nullable()->default(0);
            $table->longText("obs")->nullable()->default(0);
            $table->integer("user_id")->nullable()->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('representantes');
    }
}
