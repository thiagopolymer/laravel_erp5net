<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSacTemporizadoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sac_temporizadores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer("pessoa_id")->nullable()->default(0);
            $table->integer("sac_id")->nullable()->default(0);
            $table->string("tempo",8)->nullable()->default('00:00');
            $table->integer("user_id")->nullable()->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sac_temporizadores');
    }
}
