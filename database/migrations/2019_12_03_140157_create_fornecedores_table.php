<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateFornecedoresTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fornecedores', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pessoa_id')->nullable()->default(0);
            $table->decimal('comissao',5,2)->nullable()->default(0);
            $table->integer('banco_id')->nullable()->default(0);
            $table->string('agencia',15)->nullable()->default('');
            $table->string('conta_corrente',15)->nullable()->default('');
            $table->longText('obs')->nullable()->default('');
            $table->integer('user_id')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('fornecedores');
    }
}
