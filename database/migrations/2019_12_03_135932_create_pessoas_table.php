<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePessoasTable extends Migration
{
    public function up()
    {
        Schema::create('pessoas', function (Blueprint $table) {
            $table->bigIncrements('id');
			$table->decimal('pessoa_fisica',1,0)->default(0)->nullable();
			$table->string('cnpj', 20);
			$table->string('inscestadual', 14)->nullable();
			$table->string('razaosocial', 80)->nullable();
            $table->string('nomefantasia', 80)->default('')->nullable();
			$table->string('cep', 9)->nullable();
            $table->string('endereco', 120)->default('')->nullable();
			$table->string('numero', 60)->default('')->nullable();
			$table->string('bairro', 80)->default('')->nullable();
			$table->string('complemento', 60)->default('')->nullable();
			$table->integer('cidade_id')->default(0)->nullable();
			$table->string('fone_receita',120)->default('')->nullable();
			$table->string('ramal_receita',20)->default('')->nullable();
            $table->string('fone_fiscal',120)->default('')->nullable();
			$table->string('ramal_fiscal',20)->default('')->nullable();
			$table->string('fone1',120)->default('')->nullable();
			$table->string('fone2',120)->default('')->nullable();
			$table->string('ramal1',20)->default('')->nullable();
			$table->string('ramal2',20)->default('')->nullable();
			$table->string('email_receita',200)->default('')->nullable();
			$table->string('email_fiscal',200)->default('')->nullable();
			$table->string('email1',200)->default('')->nullable();
			$table->string('email2',200)->default('')->nullable();
            $table->integer('user_id')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('pessoas');
    }
}
