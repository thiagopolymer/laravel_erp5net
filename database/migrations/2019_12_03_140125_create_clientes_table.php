<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateClientesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('clientes', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pessoa_id');
            $table->integer('classificacao_id')->default(0)->nullable();
            $table->integer('representante_id')->default(0)->nullable();
            $table->integer('transportadora_id')->default(0)->nullable();
            $table->integer('potencial_id')->default(0)->nullable();
            $table->integer('periodo_atividade_id')->default(0)->nullable();
            $table->integer('indicador_ie_id')->default(0)->nullable();
            $table->decimal('preferencial',1,0)->default(0)->nullable();
            $table->string('suframa',15)->default('')->nullable();
            $table->decimal('serasa_negativado',1,0)->default(0)->nullable();
            $table->string('facebook',120)->default('')->nullable();
            $table->string('instagran',120)->default('')->nullable();
            $table->string('twiter',120)->default('')->nullable();
            $table->string('youtube',120)->default('')->nullable();
            $table->string('linkedin',120)->default('')->nullable();
            $table->string('cnaeprimario',10)->default('')->nullable();
            $table->string('cnaesecundario1',10)->default('')->nullable();
            $table->string('cnaesecundario2',10)->default('')->nullable();
            $table->string('cnaesecundario3',10)->default('')->nullable();
            $table->string('cnaesecundario4',10)->default('')->nullable();
            $table->string('cnaesecundario5',10)->default('')->nullable();
            $table->string('situacao',20)->default('')->nullable();
            $table->string('porte',60)->default('')->nullable();
            $table->date('abertura')->nullable();
            $table->integer('user_id')->default(0)->nullable();
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('clientes');
    }
}
