<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSacsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sacs', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('pessoa_id')->nullable()->default(0);
			$table->integer('ocorrencia_id')->nullable()->default(0);
			$table->integer('marketing_tipo_id')->nullable()->default(0);
			$table->integer('marketing_campanha_id')->nullable()->default(0);
			$table->integer('representante_id')->nullable()->default(0);
			$table->integer('user_sac_id')->nullable()->default(0);
			$table->date('proximo_contato')->nullable();
			$table->string('concluido', 1)->nullable()->default('0');
			$table->text('obs_ocorrencia', 65535)->nullable();
			$table->text('obs_mkttipo', 65535)->nullable();
			$table->string('contato', 50)->nullable();
			$table->decimal('pedido', 20,0)->nullable()->default(0);
            $table->integer('reclamacao_id')->nullable()->default(0);
            $table->integer("user_id")->nullable()->default(0);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sacs');
    }
}
