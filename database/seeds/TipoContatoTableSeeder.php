<?php

use Illuminate\Database\Seeder;

class TipoContatoTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('tipo_contatos')->truncate();
        DB::table('tipo_contatos')->insert([
            ['descricao' => 'Dono',],
            ['descricao' => 'Qualidade',],
            ['descricao' => 'Engenharia',],
            ['descricao' => 'PCP',],
            ['descricao' => 'Comprador',],
            ['descricao' => 'Vendedor',],
        ]);

    }
}
