<?php

use Illuminate\Database\Seeder;

class PotenciaisTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('potenciais')->truncate();

        DB::table('potenciais')->insert([
            [
                'descricao' => 'R$ 0,00 - R$ 500,00',
                'cor_hexa' => '#ff0000',
            ],
            [
                'descricao' => 'R$ 501,00 - R$ 2.000,00',
                'cor_hexa' => '#ffa500',
            ],
            [
                'descricao' => 'R$ 2.001,00 - R$ 5.000,00',
                'cor_hexa' => '#ffff00',
            ],
            [
                'descricao' => 'R$ 5.001,00 - R$ 10.000,00',
                'cor_hexa' => '#008000',
            ],
            [
                'descricao' => 'Maior que R$ 10.000,00',
                'cor_hexa' => '#006400',
            ]
        ]);
    }
}
