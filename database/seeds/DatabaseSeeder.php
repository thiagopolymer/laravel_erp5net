<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $this->call(ClassificacoesTableSeeder::class);
        $this->call(PeriodoAtividadesTableSeeder::class);
        $this->call(PotenciaisTableSeeder::class);
        $this->call(TipoContatoTableSeeder::class);
        $this->call(TimesTableSeeder::class);
    }
}
