<?php

use Illuminate\Database\Seeder;

class ClassificacoesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('classificacoes')->insert([
            [
                'descricao' => "INDUSTRIAS",
            ],
            [
                'descricao' => "EPI REVEDENDOR",
            ],
            [
                'descricao' => "EPI CONSUMIDOR FINAL",
            ],
            [
                'descricao' => "FORNECEDOR",
            ],
            [
                'descricao' => "EPI",
            ]
        ]);
    }
}
