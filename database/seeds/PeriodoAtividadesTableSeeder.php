<?php

use Illuminate\Database\Seeder;

class PeriodoAtividadesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('periodo_atividades')->truncate();
        DB::table('periodo_atividades')->insert([
            [
                'descricao' => 'Nunca teve 11 ou 12 com mais de 270 dias',
                'cor_hexa' => '#8b0000'
            ],
            [
                'descricao' => 'Nunca teve 11 ou 12 com mais de 180 e menos de 270 dias',
                'cor_hexa' => '#ff0000'
            ],
            [
                'descricao' => 'Nunca teve 11 ou 12 com mais de 90 e menos de 180 dias',
                'cor_hexa' => '#ff8c00'
            ],
            [
                'descricao' => 'Nunca teve 11 ou 12 com menos de 90 dias',
                'cor_hexa' => '#ffa500'
            ],
            [
                'descricao' => 'Nunca teve 11, mas teve 12 com mais de 270 dias',
                'cor_hexa' => '#483d8b'
            ],
            [
                'descricao' => 'Nunca teve 11, mas teve 12 com mais de 180 e menos de 270 dias',
                'cor_hexa' => '#00008b'
            ],
            [
                'descricao' => 'Nunca teve 11, mas teve 12 com mais de 90 e menos de 180 dias',
                'cor_hexa' => '#0000ff'
            ],
            [
                'descricao' => 'Nunca teve 11, mas teve 12 com menos de 90 dias',
                'cor_hexa' => '#00bfff'
            ],
            [
                'descricao' => 'Teve 11 com mais de 270 dias',
                'cor_hexa' => '#ffff00'
            ],
            [
                'descricao' => 'Teve 11 com mais de 180 e menos de 270 dias',
                'cor_hexa' => '#adff2f'
            ],
            [
                'descricao' => 'Teve 11 com mais de 90 e menos de 180 dias',
                'cor_hexa' => '#008000'
            ],
            [
                'descricao' => 'Teve 11 com menos de 90 dias',
                'cor_hexa' => '#006400'
            ],
        ]);

    }
}
