<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

    <head>

      @include('layouts.head')

    </head>
    <body id="page-top">
        <div class="row justify-content-center">
            <div class="col-xl-10 col-lg-12 col-md-9">
              <div class="card o-hidden border-0 shadow-lg my-5">
                <div class="card-body p-0">
                  <!-- Nested Row within Card Body -->
                  <div class="row">
                    <div class="col-lg-3"></div>

                    <div class="col-lg-6">
                      <div class="p-5">
                        <div class="text-center">
                          <h1 class="h4 mb-4 text-info">erp5net</h1>
                        </div>
                        {!! Form::open([route('login')]) !!}
                            <div class="form-group">
                                {!! Form::email('email',null,["class" => "form-control form-control-user", "placeholder" => "Entrar com endereço de e-mail"]) !!}
                                @error('email')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            <div class="form-group">
                                {!! Form::password('password',["class" => "form-control form-control-user", "placeholder" => "Senha"]) !!}
                                @error('password')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                @enderror
                            </div>
                            {!! Form::submit("Entrar",["class" => "btn btn-outline-primary btn-user btn-block"]) !!}
                        {!! Form::close() !!}
                      </div>
                    </div>
                  </div>
                </div>
              </div>

            </div>

          </div>

        </div>

        @include('layouts.js')
    </body>

</html>
