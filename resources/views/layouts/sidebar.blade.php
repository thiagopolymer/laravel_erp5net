<!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-info sidebar sidebar-dark accordion" id="accordionSidebar">

            <!-- Sidebar - Brand -->
            <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
              <div class="sidebar-brand-icon rotate-n-15">
                <i class="fas fa-sitemap"></i>
              </div>
              <div class="sidebar-brand-text mx-3">erp5net</div>
            </a>

            <!-- Divider -->
            <hr class="sidebar-divider my-0">

            <!-- Nav Item - Pages Collapse Menu -->
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseUsuario" aria-expanded="true" aria-controls="collapseUsuario">
                    <i class="fas fa-user"></i>
                    <span>{{ Auth::user()->name }}</span>
                </a>
                <div id="collapseUsuario" class="collapse" aria-labelledby="usuarioTwo" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                    <h6 class="collapse-header">Usuário</h6>
                    <a class="collapse-item" href="#">Alterar Senha</a>
                    <a class="collapse-item" href="{{ route("user.logout") }}">Logout</a>
                    </div>
                </div>
            </li>
            <li class="nav-item">
                    <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse1" aria-expanded="true" aria-controls="collapse1">
                        <i class="fas fa fa-tasks"></i>
                        <span>Cadastros</span>
                    </a>
                    <div id="collapse1" class="collapse" aria-labelledby="11" data-parent="#accordionSidebar">
                    <div class="bg-white py-2 collapse-inner rounded">
                        <a class="collapse-item" href="{{ route("pessoas") }}">Pessoas</a>
                        <a class="collapse-item" href="{{ route("pessoas.clientes") }}">Clientes</a>
                        <a class="collapse-item" href="{{ route("pessoas.representantes") }}">Representantes</a>
                        <a class="collapse-item" href="{{ route("pessoas.transportadoras") }}">Transportadoras</a>
                        {{-- <a class="collapse-item" href="{{ route("pessoas.fornecedores") }}">Fornecedores</a> --}}
                    </div>
                </div>
            </li>
            <li class="nav-item">
                <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapse2" aria-expanded="false" aria-controls="collapse2">
                    <i class="fa fa-money-check-alt"></i>
                    <span>Comercial</span>
                </a>
                <div id="collapse2" class="collapse" aria-labelledby="22" data-parent="#accordionSidebar" style="">
                    <div class="bg-white py-2 collapse-inner rounded">
                        {{-- <a class="collapse-item" href="#">Campanhas</a>
                        <a class="collapse-item" href="#">SAC</a> --}}
                        <a class="collapse-item" href="{{ route("sacs") }}">SAC</a>
                        <a class="collapse-item" href="{{ route("sacs.rel") }}">Relatório SAC</a>
                        <a class="collapse-item" href="{{ route("sacs.rel.segundo") }}">Relatório por atendimento</a>
                    </div>
                </div>
            </li>


            <!-- Divider -->
            <hr class="sidebar-divider d-none d-md-block">

            <!-- Sidebar Toggler (Sidebar) -->
            <div class="text-center d-none d-md-inline">
              <button class="rounded-circle border-0" id="sidebarToggle"></button>
            </div>

          </ul>
          <!-- End of Sidebar -->
