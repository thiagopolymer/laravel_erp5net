
<script src="{{ asset('js/sweetalert.js') }}"></script>
<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>

<script src="{{ asset('js/jquery.mask.js') }}"></script>

<script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

<script src="{{ asset('vendor/jquery-easing/jquery.easing.min.js') }}"></script>

<script src="{{ asset('js/sb-admin-2.min.js') }}"></script>
<script src="{{ asset('js/custom-functions.js') }}"></script>

<script>
    $('input').keypress(function(e) {
        var code = null;
        code = (e.keyCode ? e.keyCode : e.which);
        return (code === 13) ? false : true;
    });
    $('input[type=text]').keydown(function(e) {
        // Obter o próximo índice do elemento de entrada de texto
        var next_idx = $('input[type=text]').index(this) + 1;

        // Obter o número de elemento de entrada de texto em um documento html
        var tot_idx = $('body').find('input[type=text]').length;

        // Entra na tecla no código ASCII
        if (e.keyCode === 13) {
            if (tot_idx === next_idx)
                // Vá para o primeiro elemento de texto
                $('input[type=text]:eq(0)').focus();
            else
                // Vá para o elemento de entrada de texto seguinte
                $('input[type=text]:eq(' + next_idx + ')').focus();
        }
    });
</script>
