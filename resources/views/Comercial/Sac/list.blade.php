


@foreach($rsSacs as $reg)
    <tr class="text-dark small">
        <td class="text-center">{{ $reg->id }}</td>
        <td class="text-center">{{ date("d/m/Y H:m:i",strtotime($reg->created_at)) }}</td>
        <td class="text-center">{{ date("d/m/Y",strtotime($reg->proximo_contato)) }}</td>
        {{-- Ocorrência - Ínicio --}}
        @if(isset($reg->ocorrencia->descricao))
            <td class="text-center" title="{{ $reg->ocorrencia->descricao }}">{{ $reg->ocorrencia_id }}</td>
        @else
            <td class="text-center"></td>
        @endif
        {{-- Ocorrência - Fim --}}
        <td class="text-center">@if($reg->reclamacao_id == 1)<i class="far fa-check-square"></i>@endif</td>
        <td class="text-center">@if($reg->concluido == 1)<i class="far fa-check-square"></i>@endif</td>
        <td class="text-center ">
            @if($reg->concluido == 0)
                <button title="Concluir Contato" class="btn btn-white btn-sm text-dark btnConluir" data-id="{{ $reg->id }}"><i class="far fa-check-circle"></i></button>
            @endif

            <button title="Visualizar" class="btn btn-white btn-sm btnVisualizar" data-id="{{ $reg->id }}"><i class="fa fa-eye text-dark"></i></button>

        </td>
    </tr>
@endforeach


<script>

    $('.btnConluir').click(function(){
        Swal.fire({
            title: 'Tem certeza que deseja concluir?',
            //   text: "Verifique se existe um próximo contato.",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Sim'
        }).then((result) => {
            if (result.value) {
                url = "{!! route('sacs.concluido') !!}" + '/' + $(this).data('id');
                console.log(url);

                $.ajax({
                    type: "GET",
                    dataType: 'json',
                    url: url,
                    success: function(outros) {
                        url = "{!! route('sacs.list') !!}" + '/' + $("#pessoa_id").val();
                        $("#list").load(url);
                    }
                })
                Swal.fire(
                'Concluído!',
                )
            }
        })

    })

    $('.btnVisualizar').click(function(){

        url = "{!! route('sacs.details') !!}" + '/' + $(this).data('id');
        $("#contentModal").load(url);


        $('#modalDetails').modal('show')

    })

</script>
