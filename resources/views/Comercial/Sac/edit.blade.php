@extends('layouts.main')

@section('content')
	<!-- DataTales cidade -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            {{-- Informações  de cadastro de Pessoa --}}
            <div class="row">
                {{-- Classificação -> Inicio --}}
                <div class="form-group col-md-12 text-right">
                    <h1 id="clock">00:00</h1>
                </div>
                {!! Form::hidden("potencial_id",$rsCliente->potencial_id,['id' => 'potencial_id']) !!}
                {!! Form::hidden("periodo_atividade_id",$rsCliente->periodo_atividade_id,['id' => 'periodo_atividade_id']) !!}
                <div class="form-group col-md-2 m-0">
                    {!! Form::label('pessoa_id', 'ID', ["class" => ' m-0']) !!}
                    {!! Form::text("pessoa_id",$rsCliente->pessoa_id,["class" => "form-control form-control-sm text-uppercase", "readonly"]) !!}
                </div>
                <div class="form-group col-md-3 m-0">
                    {!! Form::label('cnpj', 'CNPJ', ["class" => ' m-0']) !!}
                    {!! Form::text("cnpj",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                </div>
                <div class="form-group col-md-7 m-0">
                    {!! Form::label('razaosocial', 'Razão Social', ["class" => ' m-0']) !!}
                    {!! Form::text("razaosocial",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                </div>
            </div>
            <div id="accordion" class="mb-3 mt-3">
                <div class="card">
                    <div class="card-header text-center m-0 pt-0 p-0 rounded" id="headingOne">
                        <h5 class="mb-0 p-0">
                            <button type="button" class="btn btn-block btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                Mais Informações
                            </button>
                        </h5>
                    </div>

                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordion">
                        <div class="card-body">
                            {{-- Cadastro de Endereço -> Inicio --}}
                            <div class="row border-top-anger">
                                <div class="form-group col-md-2">
                                    {!! Form::label('cep', 'CEP',['class' => "small"]) !!}
                                    {!! Form::text("cep",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('endereco', 'Endereço',['class' => "small"]) !!}
                                    {!! Form::text("endereco",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('numero', 'Número',['class' => "small"]) !!}
                                    {!! Form::text("numero",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('complemento', 'Complemento',['class' => "small"]) !!}
                                    {!! Form::text("complemento",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-3">
                                    {!! Form::label('bairro', 'Bairro',['class' => "small"]) !!}
                                    {!! Form::text("bairro",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-6">
                                    {!! Form::label('cidade', 'Cidade',['class' => "small"]) !!}
                                    @if($rsCliente->pessoa->cidade_id > 0)
                                        {!! Form::text("cidade",$rsCliente->pessoa->cidade->nome,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    @else
                                        {!! Form::text("cidade",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    @endif
                                </div>
                                <div class="form-group col-md-3">
                                    {!! Form::label('estado', 'Estado',['class' => "small"], ["class" => ' m-0']) !!}
                                    @if($rsCliente->pessoa->cidade_id > 0)
                                        {!! Form::text("estado",substr($rsCliente->pessoa->cidade->descricao,-2),["class" => "form-control form-control-sm", 'readonly']) !!}
                                    @else
                                        {!! Form::text("estado",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    @endif
                                </div>
                            </div>
                            {{-- Cadastro de Endereço -> Fim --}}

                            {{-- Cadastro de Contato -> Inicio --}}
                            <div class="row">
                                <div class="form-group col-md-2">
                                    {!! Form::label('fone_receita', 'Telefone (Receita)',['class' => "small"], ["class" => ' m-0']) !!}
                                    {!! Form::text("fone_receita",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('ramal_receita', 'Ramal (Receita)',['class' => "small"], ["class" => ' m-0']) !!}
                                    {!! Form::text("ramal_receita",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-8">
                                    {!! Form::label('email_receita', 'E-mail (Receita)',['class' => "small"], ["class" => ' m-0']) !!}
                                    {!! Form::text("email_receita",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('fone_fiscal', 'Telefone (Fiscal)',['class' => "small"], ["class" => ' m-0']) !!}
                                    {!! Form::text("fone_fiscal",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('ramal_fiscal', 'Ramal (Fiscal)',['class' => "small"]) !!}
                                    {!! Form::text("ramal_fiscal",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-8">
                                    {!! Form::label('email_fiscal', 'E-mail (Fiscal)',['class' => "small"]) !!}
                                    {!! Form::text("email_fiscal",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('fone1', 'Telefone 1',['class' => "small"], ["class" => ' m-0']) !!}
                                    {!! Form::text("fone1",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('ramal1', 'Ramal 1',['class' => "small"]) !!}
                                    {!! Form::text("ramal1",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-8">
                                    {!! Form::label('email1', 'E-mail 1',['class' => "small"]) !!}
                                    {!! Form::text("email1",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('fone2', 'Telefone 2',['class' => "small"], ["class" => ' m-0']) !!}
                                    {!! Form::text("fone02",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-2">
                                    {!! Form::label('ramal2', 'Ramal 2',['class' => "small"]) !!}
                                    {!! Form::text("ramal2",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                                <div class="form-group col-md-8">
                                    {!! Form::label('email2', 'E-mail 2',['class' => "small"]) !!}
                                    {!! Form::text("email2",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                </div>
                            </div>
                            {{-- Cadastro de Contato -> Fim--}}
                        </div>
                    </div>
                </div>
                {{-- Informações de cadastro de Pessoal -> Fim --}}

                {{-- Listagem de cadastro SAC -> Inicio --}}
                <div class="row mt-3">
                    <div class="col-md-12">
                        <div class="table-responsive">
                            <table class="table table-striped table-sm table-bordered " id="" width="100%" cellspacing="0">
                                <thead>
                                    <tr class="text-dark bg-secondary small">
                                        <th class="text-center">ID</th>
                                        <th class="text-center">Data</th>
                                        <th class="text-center">Prox. Contato</th>
                                        <th>Ocorrência</th>
                                        <th>Reclamação</th>
                                        <th>Concluído</th>
                                        <th><button type="button" class="btn btn-dark btn-sm btn-block small" id="incluirOcorrencia" >Novo</button></th>
                                    </tr>
                                </thead>
                                <tbody id="list">
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            {{-- Modal Para incluir dados de um chamado do Sac. --}}
            <div class="modal fade bd-example-modal-lg" id="modalSacNovo" tabindex="-1" role="dialog" aria-labelledby="modalSacNovoLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="modalSacNovoLabel">SAC - Incluir atendimento</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="formOcorrencia">
                        </div>
                    </div>
                </div>
            </div>
                {{-- Listagem de cadastro SAC -> Fim --}}
        </div>
        {{-- Modal Para incluir dados de um chamado do Sac. --}}

        {{-- Modal de detalhamento de SAC Registrado -> Inicio --}}
        <div class="modal fade bd-example-modal-lg" id="modalDetails" tabindex="-1" role="dialog" aria-labelledby="modalDetailsLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
              <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="modalDetailsLabel">SAC - Informções de SAC</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body" id="contentModal">

                    </div>
                </div>
            </div>
        </div>
        {{-- Modal de detalhamento de SAC Registrado -> Fim --}}
    </div>

    {{-- Modais para registros --}}

    {{-- Listagem de Banco - Ínicio --}}
    <div class="modal fade" id="modalSacNovo" tabindex="-1" role="dialog" aria-labelledby="modalSacNovo" aria-hidden="true">
        @include('Cadastro.Pessoa.ListagemModal.bacen_banco')
    </div>

    {{-- Listagem de Banco - Fim --}}
@endsection
@section('execjs')
    <script>
        $(document).ready(function(){
            // Trava modal - Inicio //
            $('.modal').modal({backdrop: 'static', show: false});
            // Trava modal - Fim //

            $('#pessoa_id').blur(function(){
                if($(this).val() != "")
                {
                    url = "{!! route('api.pessoa') !!}/" + $("#pessoa_id").val();
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#cnpj").val(dados.cnpj)
                                $("#razaosocial").val(dados.razaosocial)
                                $("#cep").val(dados.cep)
                                $("#endereco").val(dados.endereco)
                                $("#bairro").val(dados.bairro)
                                $("#numero").val(dados.numero)
                                $("#fone_receita").val(dados.fone_receita)
                                $("#ramal_receita").val(dados.ramal_receita)
                                $("#fone_fiscal").val(dados.fone_fiscal)
                                $("#ramal_fiscal").val(dados.ramal_fiscal)
                                $("#email_receita").val(dados.email_receita)
                                $("#email_fiscal").val(dados.email_fiscal)
                                $("#fone1").val(dados.fone1)
                                $("#ramal1").val(dados.ramal1)
                                $("#email1").val(dados.email1)
                                $("#fone2").val(dados.fone2)
                                $("#ramal2").val(dados.ramal2)
                                $("#email2").val(dados.email2)
                                $("#comissao").focus();
                            }
                        },
                    })
                }
            })

            $('#pessoa_id').blur();

            url = "{!! route('sacs.list') !!}" + '/' + $("#pessoa_id").val();
            $("#list").load(url);



            $('#incluirOcorrencia').click(function(){
                url = "{!! route('sacs.form') !!}";
                $("#formOcorrencia").load(url);
                $('#modalSacNovo').modal('show')

            })

            // $("#btnIncluir").click(function(){
            //     url = "{!! route('sacs.insert') !!}";
            //     form = $("form").serialize();

            //     $.ajax({
            //         url: url,
            //         type: "POST",
            //         data:form,
            //         contentType: "application/x-www-form-urlencoded;charset=UTF-8",
            //         success: function(dados) {
            //             url = "{!! route('sacs.list') !!}" + '/' + $("#pessoa_id").val();
            //             $("#list").load(url);
            //             $('#modalSacNovo').modal('hide');
            //             urlAtual = window.location.href;
            //             $(window.document.location).attr('href',urlAtual);
            //         }
            //     })
            // });

        // Contador de Tempo - Ínicio //
        contador = 0;
        continuarTempo = 0;
        setInterval(function () {
            // if(continuarTempo == 0)
            // {
                contador ++;
                tempo  = ("0" + Math.trunc(contador / 60)).slice(-2) + ":" + ("0" + Math.trunc(contador % 60)).slice(-2);
                $("#clock").text(tempo);
                }
            // }
            , 1000);
            // Fim //
            // jQuery(window).focus(function(){
            //     continuarTempo = 0;
            // })

            // $(window).blur(function(){
            //     continuarTempo = 1;
            // })
            // $(window).unload(function() {
            //     window.open("https://www.google.com.br", "_blank");
            // });

            tempInsertOk = 0;
            // window.onbeforeunload = function () {
            //     url = "{!! route('sacs.temp.insert') !!}" + '/' + $("#pessoa_id").val() + '/' + $("#clock").text();
            //     $.get(url);
            // };

            $(window).focus(function(){
                url = "{!! route('sacs.list') !!}" + '/' + $("#pessoa_id").val();
                $("#list").load(url);
            })

        })


    </script>
@endsection
