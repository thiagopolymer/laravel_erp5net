<input type="text" id="search" class="form-control form-control-sm mb-2" placeholder="Digite para buscar">
<input type="hidden" id="selecionados">
<div class="table-responsive" id="list_clientes">
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
{{-- <button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button> --}}

{{-- <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script> --}}

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

<script>
    $(document).ready(function(){
        url = "{!! route('sacs.selects.clientes.list') !!}";
        $('#list_clientes').load(url)
    })

    $('#search').keyup(function(){
        search = $(this).val();

        url = "{!! route('sacs.selects.clientes.list') . '?search=' !!}" + search.replace(' ','_');
        $('#list_clientes').load(url)
    })

    $("#btn_select").click(function(){
        inIdClientes = $("#selecionados").val();
        if(inIdClientes.substr(-1) == ",")
        {
            inIdClientes = inIdClientes.substr(0,inIdClientes.length-1);
        }
        $("#clientes_id").val(inIdClientes);
        $('#modal').modal('hide')
    });

</script>
