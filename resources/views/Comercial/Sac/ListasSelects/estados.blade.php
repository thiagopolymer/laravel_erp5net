<div class="table-responsive">
    <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
        <thead>
            <tr class="text-dark bg-secondary small">
                <th class="text-center">
                    {{-- {!! Form::checkbox("estado_select_all",1,false) !!} --}}
                </th>
                <th class="text-center">ID</th>
                <th>Sigla</th>
                <th>Descrição</th>
                <th class="text-center">IBGE</th>
            </tr>
            {{-- <tr>
                <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
            </tr> --}}
        </thead>
        <tbody>
            @foreach ($rsEstados as $reg)
                <tr class="small estado_tr_select" data-valid="estado_check_select_{{ $reg->id }}">
                    <td class="text-center">
                        {!! Form::checkbox("check_estado_id[]",$reg->id,false,["id" => "estado_check_select_" . $reg->id]) !!}
                    </td>
                    <td class="text-center">{{ $reg->id }}</td>
                    <td class="text-center">{{ $reg->sigla }}</td>
                    <td>{{ $reg->descricao }}</td>
                    <td class="text-center">{{ $reg->cod_ibge }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
<script>
    $(document).ready(function(){
        $(".estado_tr_select").click(function(){

            nameChecked = "#" + $(this).data("valid");
            if($(nameChecked).prop('checked')) {
                $(nameChecked).prop('checked',false);
            } else {
                $(nameChecked).prop('checked',true);
            }
        });

        $("#btn_select").click(function(){
            inIdestados = "";
            $('input[type=checkbox]').each(function(){
                if($(this).prop("checked"))
                {
                    inIdestados = inIdestados + $(this).val() + ',';
                }
            })
            if(inIdestados.substr(-1) == ",")
            {
                inIdestados = inIdestados.substr(0,inIdestados.length-1);
            }
            $("#estado_id").val(inIdestados);
            $('#modal').modal('hide')
        });

    })
</script>
