<div class="table-responsive">
    <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
        <thead>
            <tr class="text-dark bg-secondary small">
                <th class="text-center">
                    {{-- {!! Form::checkbox("classificacao_select_all",1,false) !!} --}}
                </th>
                <th class="text-center">ID</th>
                <th>Descrição</th>
            </tr>
            {{-- <tr>
                <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
            </tr> --}}
        </thead>
        <tbody>
            @foreach ($rsClassificacoes as $reg)
                <tr class="small classificacao_tr_select" data-valid="classificacao_check_select_{{ $reg->id }}">
                    <td class="text-center">
                        {!! Form::checkbox("check_clissificacoes_id[]",$reg->id,false,["id" => "classificacao_check_select_" . $reg->id]) !!}
                    </td>
                    <td class="text-center">{{ $reg->id }}</td>
                    <td>{{ $reg->descricao }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
<script>
    $(document).ready(function(){
        $(".classificacao_tr_select").click(function(){

            nameChecked = "#" + $(this).data("valid");
            if($(nameChecked).prop('checked')) {
                $(nameChecked).prop('checked',false);
            } else {
                $(nameChecked).prop('checked',true);
            }
        });

        $("#btn_select").click(function(){
            inIdClassificacoes = "";
            $('input[type=checkbox]').each(function(){
                if($(this).prop("checked"))
                {
                    inIdClassificacoes = inIdClassificacoes + $(this).val() + ',';
                }
            })
            if(inIdClassificacoes.substr(-1) == ",")
            {
                inIdClassificacoes = inIdClassificacoes.substr(0,inIdClassificacoes.length-1);
            }
            $("#classificacao_id").val(inIdClassificacoes);
            $('#modal').modal('hide')
        });

    })
</script>
