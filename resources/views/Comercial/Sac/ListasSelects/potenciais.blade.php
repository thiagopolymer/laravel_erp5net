<div class="table-responsive">
    <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
        <thead>
            <tr class="text-dark bg-secondary small">
                <th class="text-center">
                    {{-- {!! Form::checkbox("potenciais_select_all",1,false) !!} --}}
                </th>
                <th class="text-center">ID</th>
                <th>name</th>
                <th>Cor Hexadecimal</th>
            </tr>
            {{-- <tr>
                <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
            </tr> --}}
        </thead>
        <tbody>
            @foreach ($rsPotenciais as $reg)
                <tr class="small potenciais_tr_select" data-valid="potenciais_check_select_{{ $reg->id }}">
                    <td class="text-center">
                        {!! Form::checkbox("check_potenciais_id[]",$reg->id,false,["id" => "potenciais_check_select_" . $reg->id]) !!}
                    </td>
                    <td class="text-center">{{ $reg->id }}</td>
                    <td>{{ $reg->descricao }}</td>
                    <td class="text-dark" style="background-color: {{ $reg->cor_hexa }};">{{ $reg->cor_hexa }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
<script>
    $(document).ready(function(){
        $(".potenciais_tr_select").click(function(){

            nameChecked = "#" + $(this).data("valid");
            if($(nameChecked).prop('checked')) {
                $(nameChecked).prop('checked',false);
            } else {
                $(nameChecked).prop('checked',true);
            }
        });

        $("#btn_select").click(function(){
            inIdPotenciais = "";
            $('input[type=checkbox]').each(function(){
                if($(this).prop("checked"))
                {
                    inIdPotenciais = inIdPotenciais + $(this).val() + ',';
                }
            })
            if(inIdPotenciais.substr(-1) == ",")
            {
                inIdPotenciais = inIdPotenciais.substr(0,inIdPotenciais.length-1);
            }
            $("#potencial_id").val(inIdPotenciais);
            $('#modal').modal('hide')
        });

    })
</script>
