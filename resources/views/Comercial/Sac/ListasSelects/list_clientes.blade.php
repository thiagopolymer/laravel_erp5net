<table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
    <thead>
        <tr class="text-dark bg-secondary small">
            <th class="text-center">
                {{-- {!! Form::checkbox("clientes_select_all",1,false) !!} --}}
            </th>
            <th class="text-center">ID</th>
            <th class="text-center">Razão Social</th>
            <th>CNPJ</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rsClientes as $reg)
            <tr class="small clientes_tr_select" data-valid="clientes_check_select_{{ $reg->id }}">
                <td class="text-center">
                    {!! Form::checkbox("check_clientes_id[]",$reg->id,false,["id" => "clientes_check_select_" . $reg->id]) !!}
                </td>
                <td class="text-center">{{ $reg->pessoa_id }}</td>
                <td>{{ $reg->razaosocial }}</td>
                <td class="text-center">{{ $reg->cnpj }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
{{ $rsClientes->links() }}
<script>
    $(".clientes_tr_select").click(function(){
        nameChecked = "#" + $(this).data("valid");

        selecionados = $("#selecionados").val();

        selecionadoTruFalse = $(nameChecked).val() + ',';

        if($(nameChecked).prop('checked')) {
            $(nameChecked).prop('checked',false);
            selecionados = selecionados.replace(selecionadoTruFalse,'')
        } else {
            $(nameChecked).prop('checked',true);
            selecionados = selecionados + selecionadoTruFalse
        }

        $('#selecionados').val(selecionados);
    });

    $('.page-link').click(function(){
        search = $("#search").val();
        url = $(this).attr('href') + '&search=' + search.replace(' ','_');
        $("#list_clientes").load(url);
        return false;
    })
</script>
