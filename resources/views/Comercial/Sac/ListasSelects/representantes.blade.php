<input type="text" id="search" class="form-control form-control-sm mb-2" placeholder="Digite para buscar">
<input type="hidden" id="selecionados">
<div class="table-responsive" id="list_representantes">
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
{{-- <button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button> --}}

{{-- <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script> --}}

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

<script>
    $(document).ready(function(){
        url = "{!! route('sacs.selects.representantes.list') !!}";
        $('#list_representantes').load(url)
    })

    $('#search').keyup(function(){
        search = $(this).val();

        url = "{!! route('sacs.selects.representantes.list') . '?search=' !!}" + search.replace(' ','_');
        $('#list_representantes').load(url)
    })

    $("#btn_select").click(function(){
        inIdRepresentantes = $("#selecionados").val();
        if(inIdRepresentantes.substr(-1) == ",")
        {
            inIdRepresentantes = inIdRepresentantes.substr(0,inIdRepresentantes.length-1);
        }
        $("#representante_id").val(inIdRepresentantes);
        $('#modal').modal('hide')
    });

</script>
