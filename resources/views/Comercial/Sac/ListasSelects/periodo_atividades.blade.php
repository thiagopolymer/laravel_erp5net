<div class="table-responsive">
    <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
        <thead>
            <tr class="text-dark bg-secondary small">
                <th class="text-center">
                    {{-- {!! Form::checkbox("periodo_atividade_select_all",1,false) !!} --}}
                </th>
                <th class="text-center">ID</th>
                <th>name</th>
                <th>Cor Hexadecimal</th>
            </tr>
            {{-- <tr>
                <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
            </tr> --}}
        </thead>
        <tbody>
            @foreach ($rsPeriodoAtividades as $reg)
                <tr class="small periodo_atividade_tr_select" data-valid="periodo_atividade_check_select_{{ $reg->id }}">
                    <td class="text-center">
                        {!! Form::checkbox("check_periodo_atividade_id[]",$reg->id,false,["id" => "periodo_atividade_check_select_" . $reg->id]) !!}
                    </td>
                    <td class="text-center">{{ $reg->id }}</td>
                    <td>{{ $reg->descricao }}</td>
                    <td class="text-dark" style="background-color: {{ $reg->cor_hexa }};">{{ $reg->cor_hexa }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
<script>
    $(document).ready(function(){
        $(".periodo_atividade_tr_select").click(function(){

            nameChecked = "#" + $(this).data("valid");
            if($(nameChecked).prop('checked')) {
                $(nameChecked).prop('checked',false);
            } else {
                $(nameChecked).prop('checked',true);
            }
        });

        $("#btn_select").click(function(){
            inIdPeriodoAtividades = "";
            $('input[type=checkbox]').each(function(){
                if($(this).prop("checked"))
                {
                    inIdPeriodoAtividades = inIdPeriodoAtividades + $(this).val() + ',';
                }
            })
            if(inIdPeriodoAtividades.substr(-1) == ",")
            {
                inIdPeriodoAtividades = inIdPeriodoAtividades.substr(0,inIdPeriodoAtividades.length-1);
            }
            $("#periodo_atividade_id").val(inIdPeriodoAtividades);
            $('#modal').modal('hide')
        });

    })
</script>
