<input type="text" id="search" class="form-control form-control-sm mb-2" placeholder="Digite para buscar">
<input type="hidden" id="selecionados">
<div class="table-responsive" id="list_transportadoras">
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
{{-- <button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button> --}}

{{-- <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script> --}}

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

<script>
    $(document).ready(function(){
        url = "{!! route('sacs.selects.transportadoras.list') !!}";
        $('#list_transportadoras').load(url)
    })

    $('#search').keyup(function(){
        search = $(this).val();

        url = "{!! route('sacs.selects.transportadoras.list') . '?search=' !!}" + search.replace(' ','_');
        $('#list_transportadoras').load(url)
    })

    $("#btn_select").click(function(){
        inIdTransportadoras = $("#selecionados").val();
        if(inIdTransportadoras.substr(-1) == ",")
        {
            inIdTransportadoras = inIdTransportadoras.substr(0,inIdTransportadoras.length-1);
        }
        $("#transportadora_id").val(inIdTransportadoras);
        $('#modal').modal('hide')
    });

</script>
