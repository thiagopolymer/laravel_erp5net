<input type="text" id="search" class="form-control form-control-sm mb-2" placeholder="Digite para buscar">
<input type="hidden" id="selecionados">
<div class="table-responsive" id="list_cidades">
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
{{-- <button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button> --}}

{{-- <script src="{{ asset('vendor/datatables/jquery.dataTables.min.js') }}"></script>
<script src="{{ asset('vendor/datatables/dataTables.bootstrap4.min.js') }}"></script> --}}

<!-- Page level custom scripts -->
<script src="{{ asset('js/demo/datatables-demo.js') }}"></script>

<script>
    $(document).ready(function(){
        url = "{!! route('sacs.selects.cidades.list') !!}";
        $('#list_cidades').load(url)
    })

    $('#search').keyup(function(){
        search = $(this).val();

        url = "{!! route('sacs.selects.cidades.list') . '?search=' !!}" + search.replace(' ','_');
        $('#list_cidades').load(url)
    })

    $("#btn_select").click(function(){
        inIdCidades = $("#selecionados").val();
        if(inIdCidades.substr(-1) == ",")
        {
            inIdCidades = inIdCidades.substr(0,inIdCidades.length-1);
        }
        $("#cidade_id").val(inIdCidades);
        $('#modal').modal('hide')
    });

</script>
