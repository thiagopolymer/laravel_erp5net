<div class="table-responsive">
    <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
        <thead>
            <tr class="text-dark bg-secondary small">
                <th class="text-center">
                    {{-- {!! Form::checkbox("ocorrencias_select_all",1,false) !!} --}}
                </th>
                <th class="text-center">ID</th>
                <th>Sigla</th>
            </tr>
            {{-- <tr>
                <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
            </tr> --}}
        </thead>
        <tbody>
            @foreach ($rsOcorrencias as $reg)
                <tr class="small ocorrencias_tr_select" data-valid="ocorrencias_check_select_{{ $reg->id }}">
                    <td class="text-center">
                        {!! Form::checkbox("check_ocorrencias_id[]",$reg->id,false,["id" => "ocorrencias_check_select_" . $reg->id]) !!}
                    </td>
                    <td class="text-center">{{ $reg->id }}</td>
                    <td class="text-center">{{ $reg->descricao }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
<script>
    $(document).ready(function(){
        $(".ocorrencias_tr_select").click(function(){

            nameChecked = "#" + $(this).data("valid");
            if($(nameChecked).prop('checked')) {
                $(nameChecked).prop('checked',false);
            } else {
                $(nameChecked).prop('checked',true);
            }
        });

        $("#btn_select").click(function(){
            inIdOcorrencias = "";
            $('input[type=checkbox]').each(function(){
                if($(this).prop("checked"))
                {
                    inIdOcorrencias = inIdOcorrencias + $(this).val() + ',';
                }
            })
            if(inIdOcorrencias.substr(-1) == ",")
            {
                inIdOcorrencias = inIdOcorrencias.substr(0,inIdOcorrencias.length-1);
            }
            $("#ocorrencia_id").val(inIdOcorrencias);
            $('#modal').modal('hide')
        });

    })
</script>
