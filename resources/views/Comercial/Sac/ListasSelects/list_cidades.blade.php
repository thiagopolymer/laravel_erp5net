<table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
    <thead>
        <tr class="text-dark bg-secondary small">
            <th class="text-center">
                {{-- {!! Form::checkbox("cidades_select_all",1,false) !!} --}}
            </th>
            <th class="text-center">ID</th>
            <th class="text-center">IBGE</th>
            <th>Nome</th>
        </tr>
        {{-- <tr>
            <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
        </tr> --}}
    </thead>
    <tbody>
        @foreach ($rsCidades as $reg)
            <tr class="small cidades_tr_select" data-valid="cidades_check_select_{{ $reg->id }}">
                <td class="text-center">
                    {!! Form::checkbox("check_cidades_id[]",$reg->id,false,["id" => "cidades_check_select_" . $reg->id]) !!}
                </td>
                <td class="text-center">{{ $reg->id }}</td>
                <td class="text-center">{{ $reg->cod_ibge }}</td>
                <td>{{ $reg->nome }}</td>
            </tr>
        @endforeach
    </tbody>
</table>
{{ $rsCidades->links() }}
<script>
    $(".cidades_tr_select").click(function(){
        nameChecked = "#" + $(this).data("valid");

        selecionados = $("#selecionados").val();

        selecionadoTruFalse = $(nameChecked).val() + ',';

        if($(nameChecked).prop('checked')) {
            $(nameChecked).prop('checked',false);
            selecionados = selecionados.replace(selecionadoTruFalse,'')
        } else {
            $(nameChecked).prop('checked',true);
            selecionados = selecionados + selecionadoTruFalse
        }

        $('#selecionados').val(selecionados);
    });
    $('.page-link').click(function(){
        search = $("#search").val();
        url = $(this).attr('href') + '&search=' + search.replace(' ','_');
        $("#list_cidades").load(url);
        return false;
    })

</script>
