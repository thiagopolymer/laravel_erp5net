<div class="table-responsive">
    <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
        <thead>
            <tr class="text-dark bg-secondary small">
                <th class="text-center">
                    {{-- {!! Form::checkbox("user_select_all",1,false) !!} --}}
                </th>
                <th class="text-center">ID</th>
                <th>name</th>
                <th>E-mail</th>
            </tr>
            {{-- <tr>
                <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
            </tr> --}}
        </thead>
        <tbody>
            @foreach ($rsUsers as $reg)
                <tr class="small user_tr_select" data-valid="user_check_select_{{ $reg->id }}">
                    <td class="text-center">
                        {!! Form::checkbox("check_user_id[]",$reg->id,false,["id" => "user_check_select_" . $reg->id]) !!}
                    </td>
                    <td class="text-center">{{ $reg->id }}</td>
                    <td>{{ $reg->name }}</td>
                    <td>{{ $reg->email }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>
<button class="btn btn-sm btn-block btn-outline-info" id="btn_select">Selecionar</button>
<script>
    $(document).ready(function(){
        $(".user_tr_select").click(function(){

            nameChecked = "#" + $(this).data("valid");
            if($(nameChecked).prop('checked')) {
                $(nameChecked).prop('checked',false);
            } else {
                $(nameChecked).prop('checked',true);
            }
        });

        $("#btn_select").click(function(){
            inIdUsers = "";
            $('input[type=checkbox]').each(function(){
                if($(this).prop("checked"))
                {
                    inIdUsers = inIdUsers + $(this).val() + ',';
                }
            })
            if(inIdUsers.substr(-1) == ",")
            {
                inIdUsers = inIdUsers.substr(0,inIdUsers.length-1);
            }
            $("#user_id").val(inIdUsers);
            $('#modal').modal('hide')
        });

    })
</script>
