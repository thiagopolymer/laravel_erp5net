{!! Form::open( ['route' => 'sacs.insert']) !!}
    {{-- Cadastro de Endereço -> Inicio --}}
    <div class="row border-top-anger">
        <div class="form-group col-md-3">
            {!! Form::label('proximo_contato', 'Próx. Contato',['class' => "small"]) !!}
            {!! Form::text("proximo_contato",date('d/m/Y',strtotime($rsSac->proximo_contato)),["class" => "form-control form-control-sm", 'readonly']) !!}
        </div>
        <div class="form-group col-md-5">
            {!! Form::label('contato', 'Contato',['class' => "small"]) !!}
            {!! Form::text("contato",$rsSac->contato,["class" => "form-control form-control-sm", 'readonly']) !!}
        </div>
        <div class="form-group col-md-4">
            {!! Form::label('pedido', 'Pedido',['class' => "small"]) !!}
            {!! Form::text("pedido",$rsSac->pedido,["class" => "form-control form-control-sm", 'readonly']) !!}
        </div>
        <div class="form-group col-md-2">
            {!! Form::label('ocorrencia_id', 'Ocorrencia',['class' => "small"]) !!}
            {!! Form::text("ocorrencia_id",$rsSac->ocorrencia_id,["class" => "form-control form-control-sm", 'readonly']) !!}
        </div>
        <div class="form-group col-md-6">
            {!! Form::label('ocorrencia_descricao', ' ',['class' => "small"]) !!}
            {!! Form::text("ocorrencia_descricao",$rsSac->ocorrencia->descricao,["class" => "form-control form-control-sm", 'readonly']) !!}
        </div>
        <div class="form-group col-md-4">
            {!! Form::label('created_at', 'Dt. e Hr. do Atendimento',['class' => "small"]) !!}
            {!! Form::text("created_at",date('d/m/Y h:m:i',strtotime($rsSac->created_at)),["class" => "form-control form-control-sm", 'readonly']) !!}
        </div>
        <div class="form-group col-md-12">
            {!! Form::label('obs_ocorrencia', 'Observação',['class' => "small"]) !!}
            {!! Form::textarea("obs_ocorrencia",$rsSac->obs_ocorrencia,["class" => "form-control form-control-sm text-uppercase", "rows" => 2, 'readonly']) !!}
        </div>
    </div>
    <div class="row border-top-anger">
        @foreach ($rsSacFiles as $reg)
        <div class="form-group col-md-4">
            @php($urlImagem = url('') . '/storage/sac/prints/' . $reg->nome)
                <div class="card">
                    <div class="card-body">
                        <a href="{{ str_replace('../','',$urlImagem) }}" target="_blank">
                            <img class="img-fluid" src="{{ $urlImagem }}" alt="Print">
                        </a>
                    </div>
                </div>
        </div>
        @endforeach
    </div>
{!! Form::close() !!}
{{-- Cadastro de Endereço -> Fim --}}
