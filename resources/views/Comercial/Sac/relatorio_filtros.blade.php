@extends('layouts.main')

@section('content')
    {!! Form::open( ['route' => 'sacs.relatorios.print',"enctype" => "multipart/form-data", 'target' => "_blank"]) !!}
        {!! Form::hidden('ocorrencia_pessoa_id',null,["id" => "ocorrencia_pessoa_id"]) !!}
        {!! Form::hidden('potencial_pessoa_id',null,["id" => "potencial_pessoa_id"]) !!}
        {!! Form::hidden('periodo_atividade_pessoa_id',null,["id" => "periodo_atividade_pessoa_id"]) !!}

        <div class="form-group row mt-0 mb-0">
            <div class="col-sm-3">
                {!! Form::label('dt_inclusao_de', 'Dt. Inclusão de:', ["class" => ' col-form-label m-0']) !!}
                {!! Form::date('dt_inclusao_de', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::label('dt_inclusao_ate', 'Até:', ["class" => ' col-form-label m-0']) !!}
                {!! Form::date('dt_inclusao_ate', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::label('proximo_contato_de', 'Próx. Contato de:', ["class" => ' col-form-label m-0']) !!}
                {!! Form::date('proximo_contato_de', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::label('proximo_contato_ate', 'Até:', ["class" => ' col-form-label m-0']) !!}
                {!! Form::date('proximo_contato_ate', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('user_id', 'Usuário', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('user_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="user_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('periodo_atividade_id', 'Periodo de Atividade', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('periodo_atividade_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="periodo_atividade_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('classificacao_id', 'Classificação', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('classificacao_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="classificacao_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('potencial_id', 'Potencial', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('potencial_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="potencial_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('ocorrencia_id', 'Ocorrência', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('ocorrencia_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="ocorrencia_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('cnpj', 'CNPJ/CPF', ["class" => ' col-form-label m-0']) !!}
                {!! Form::text('cnpj', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('razaosocial', 'Razão Social', ["class" => ' col-form-label m-0']) !!}
                {!! Form::text('razaosocial', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-md-6">
                {!! Form::label('nomefantasia', 'Nome Fantasia', ["class" => ' col-form-label m-0']) !!}
                {!! Form::text('nomefantasia', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('cep', 'CEP', ["class" => ' col-form-label m-0']) !!}
                {!! Form::text('cep', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-md-3">
                {!! Form::label('cidade_id', 'Cidades', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('cidade_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="cidade_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('estado_id', 'UF', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('estado_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="estado_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('representante_id', 'Representantes', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('representante_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="representante_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('transportadora_id', 'Transportadoras', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('transportadora_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="transportadora_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                {!! Form::label('clientes_id', 'Clientes', ["class" => ' col-form-label m-0']) !!}
                <div class="input-group mb-2 mr-sm-2 m-0">
                    {!! Form::text('clientes_id', null,["class" => "form-control form-control-sm"]) !!}
                    <div class="input-group-prepend input-group-prepend-sm">
                        <button type="button" id="clientes_id_btn" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalButton"><i class="fa fa-search"></i></button>
                    </div>
                </div>
            </div>
            <div class="col-sm-3">
                {!! Form::label('cnae', 'CNAE:', ["class" => ' col-form-label m-0']) !!}
                {!! Form::text('cnae', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::label('situacao_empresa', 'Situação Empresa', ["class" => ' col-form-label m-0']) !!}
                {!! Form::text('situacao_empresa', null,["class" => "form-control form-control-sm"]) !!}
            </div>
            <div class="col-sm-3">
                {!! Form::label('porte_empresa', 'Porte Empresa', ["class" => ' col-form-label m-0']) !!}
                {!! Form::text('porte_empresa', null,["class" => "form-control form-control-sm"]) !!}
            </div>
        </div>

        {{-- Campos de informações para cadastro de Clinete -> Fim --}}

        {{-- Botões de acesso -> Inicio --}}
        <div class="form-group row mt-3">
            <div class="col-md-12">
                <button type="submit" id="btnSubmit" class="btn btn-outline-primary btn-block">Gerar</button>
            </div>
        </div>
    {{-- Botões de acesso -> Inicio --}}
    {!! Form::close() !!}

    {{-- Modal para multselect  -> Ínicio --}}
    <div class="modal fade bd-example-modal-lg" id="modal" tabindex="-1" role="dialog" aria-labelledby="modalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalLabel"></h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body" id="list">
                </div>
            </div>
        </div>
    </div>
    {{-- Modal para multselect  -> Fim --}}

@endsection

@section('execjs')
    <script>
        // seleciona Usuários -> Ínicio
        $('#user_id_btn').click(function(){
            url = "{!! route('sacs.selects.users') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Usuários");
            $('#modal').modal('show')
        });
        // seleciona Usuários -> Fim

        // seleciona Estados -> Ínicio
        $('#estado_id_btn').click(function(){
            url = "{!! route('sacs.selects.estados') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Estados (UF)");
            $('#modal').modal('show')
        });
        // seleciona Estados -> Fim

        // seleciona Classificação -> Ínicio
        $('#classificacao_id_btn').click(function(){
            url = "{!! route('sacs.selects.classificacoes') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Classificações");
            $('#modal').modal('show')
        });
        // seleciona Estados -> Fim

        // seleciona Potenciais -> Ínicio
        $('#potencial_id_btn').click(function(){
            url = "{!! route('sacs.selects.potenciais') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Potenciais");
            $('#modal').modal('show')
        });
        // seleciona Potenciais -> Fim

        // seleciona Cidades -> Ínicio
        $('#cidade_id_btn').click(function(){
            url = "{!! route('sacs.selects.cidades') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Cidades");
            $('#modal').modal('show')
        });
        // seleciona Cidades -> Fim

        // seleciona Periodo de Atividades -> Ínicio
        $('#periodo_atividade_id_btn').click(function(){
            url = "{!! route('sacs.selects.periodo_atividades') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Período de Atividades");
            $('#modal').modal('show')
        });
        // seleciona Periodo de Atividades -> Fim

        // seleciona Representante -> Ínicio
        $('#representante_id_btn').click(function(){
            url = "{!! route('sacs.selects.representantes') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Representantes");
            $('#modal').modal('show')
        });
        // seleciona Representante -> Fim

        // seleciona Transportadoras -> Ínicio
        $('#transportadora_id_btn').click(function(){
            url = "{!! route('sacs.selects.transportadoras') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Transportadoras");
            $('#modal').modal('show')
        });
        // seleciona Representante -> Fim

        // seleciona Clientes -> Ínicio
        $('#clientes_id_btn').click(function(){
            url = "{!! route('sacs.selects.clientes') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Clientes");
            $('#modal').modal('show')
        });
        // seleciona Clientes -> Fim

        // seleciona Clientes -> Ínicio
        $('#ocorrencia_id_btn').click(function(){
            url = "{!! route('sacs.selects.ocorrencias') !!}";
            $("#list").load(url);
            $("#modalLabel").text("Ocorrências");
            $('#modal').modal('show')
        });
        // seleciona Clientes -> Fim
    </script>
@endsection
