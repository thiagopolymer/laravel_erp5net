@extends('layouts.main')

@section('content')
	<!-- DataTales Example -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col-md-12">
                    <a href="#" class="btn btn-outline-dark btn-sm" id="excel" title="Execl CSV">Excel CSV</a>
                    <a href="#" class="btn btn-outline-dark btn-sm" title="PDF">PDF</a>
                    <a href="#" class="btn btn-outline-dark btn-sm" title="Imprimir">Imprimir</a>
                    <a href="#" class="btn btn-outline-dark btn-sm" title="Colunas Visíveis">Colunas Visíveis</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-dark bg-secondary small">
                            <th class="text-center">ID</th>
                            <th>Razão Social</th>
                            <th>CNPJ</th>
                            <th class="text-center">Próximo Contato</th>
                            <th class="text-center">Potencial</th>
                            <th class="text-center" style="width:120px;">Período Atividade</th>
                            <th class="text-center" style="width:100px;">Menu</th>
                        </tr>
                        <tr>
                            <th colspan="7"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
                        </tr>
                    </thead>
                    <tbody id="list">
                    </tbody>
                </table>
            </div>
            {{-- <div class="row">
                <div class="col-md-12 text-center">
                    <div class="btn-group" role="group" aria-label="Basic example">
                        <button type="button" value="0" id="1" class="btn btn-outline-secondary btn-sm btn_pagination">Primeiro</button>
                        <button type="button" value="0" id="2" class="btn btn-outline-secondary btn-sm btn_pagination">Anterior</button>
                        <button type="button" value="1" id="3" class="btn btn-outline-secondary btn-sm btn_pagination">Próximo</button>
                        <button type="button" value="0" id="4" class="btn btn-outline-secondary btn-sm btn_pagination">Último</button>
                    </div>
                </div>
            </div> --}}
        </div>
    </div>
@endsection

@section('execjs')
    <script>

        $(document).ready(function(){
            $("#list").load("{!! route('sacs.clientes.list') !!}");

            // total = 10;

            // paginationTotal = total/20;

            // total = parseFloat(paginationTotal.toFixed(0))

            // $("#4").val(total);
            $("#search").keyup(function(){
                search = $(this).val();
                if(search == "")
                {
                    url = "{!! route('sacs.clientes.list') !!}";
                }
                else
                {
                    url = "{!! route('sacs.clientes.list') !!}" + '/' + 0 + '/' + search.replace(' ','_');
                }
                $("#list").load(url);
            })

            // $(".btn_pagination").click(function(){
            //     if($("#search").val() == "")
            //     {
            //         url = "{!! route('sacs.clientes.list') !!}" + '/' + $(this).val();
            //     }
            //     else
            //     {
            //         url = "{!! route('sacs.clientes.list') !!}" + '/' + $(this).val() + '/' + $("#search").val();
            //     }
            //     $("#list").load(url);


            //     activeButton = $(this).attr("id");

            //     switch(parseInt(activeButton))
            //     {
            //         case 1:
            //             $("#2").val(0);
            //             $("#3").val(1);
            //             break;
            //         case 2:
            //             if($("#2").val() <= 1)
            //             {
            //                 $("#2").val(0);
            //                 $("#3").val(1);
            //             }
            //             else
            //             {
            //                 $("#2").val( ($("#2").val() - 1));
            //                 $("#3").val( ($("#3").val() - 1));
            //             }
            //             break;
            //         case 3:
            //             if(parseInt($("#3").val()) > parseInt($("#4").val()))
            //             {
            //                 $("#2").val(parseInt($("#4").val()));
            //                 $("#3").val(parseInt($("#4").val()) - 1);
            //             }
            //             else
            //             {
            //                 $("#2").val(parseInt($("#2").val()) + 1);
            //                 $("#3").val(parseInt($("#3").val()) + 1);
            //             }
            //             break;
            //         case 4:
            //             $("#2").val(parseInt($("#4").val()) - 1);
            //             $("#3").val( parseInt($("#4").val()));
            //             break;
            //     }
            // })

        });
    </script>
@endsection
