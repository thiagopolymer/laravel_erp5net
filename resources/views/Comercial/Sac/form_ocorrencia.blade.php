
{!! Form::open( ['route' => 'sacs.insert',"enctype" => "multipart/form-data"]) !!}
    {!! Form::hidden('ocorrencia_pessoa_id',null,["id" => "ocorrencia_pessoa_id"]) !!}
    {!! Form::hidden('potencial_pessoa_id',null,["id" => "potencial_pessoa_id"]) !!}
    {!! Form::hidden('periodo_atividade_pessoa_id',null,["id" => "periodo_atividade_pessoa_id"]) !!}
    {!! Form::hidden('tempo',null,["id" => "tempo"]) !!}

    <div class="row">
        <div class="form-group col-md-3 m-0">
            {!! Form::label('proximo_contato', 'Próximo Contato', ["class" => ' m-0']) !!}
            {!! Form::date('proximo_contato', null,["class" => "form-control form-control-sm", "readonly"]) !!}
        </div>
        <div class="form-group col-md-5 m-0">
            {!! Form::label('contato', 'Contato', ["class" => ' m-0']) !!}
            {!! Form::text('contato', null,["class" => "form-control form-control-sm", "required"]) !!}
        </div>
        <div class="form-group col-md-4 m-0">
            {!! Form::label('pedido', 'Nr. Pedido/Orçamento', ["class" => ' m-0']) !!}
            {!! Form::text('pedido', null,["class" => "form-control form-control-sm"]) !!}
        </div>
        <div class="form-group col-md-6 m-0">
            {!! Form::label('ocorrencia_id', 'Ocorência', ["class" => ' m-0']) !!}
            <select class="form-control form-control-sm" name="ocorrencia_id" id="ocorrencia_id" required>
                <option value="">--- Selecione ---</option>
                @foreach ($rsOcorrencias as $reg)
                    <option value="{{ $reg->id }}">{{ str_pad($reg->id, 2, "0", STR_PAD_LEFT) }} - {{ $reg->descricao }}</option>
                @endforeach
            <select>
        </div>

        <div class="form-check form-check-inline">
            <div class="custom-control custom-checkbox mr-sm-3 pt-3 m-0">
                {!! Form::checkbox('reclamacao',1,"",["class" => "custom-control-input", "id" => 'reclamacao']) !!}
                {!! Form::label('reclamacao', 'Reclamação',["class" => "custom-control-label", "for" => "reclamacao"]); !!}
            </div>
        </div>
        <div class="col-md-12 mt-3">
            <div class="custom-file">
                {!! Form::label('file', 'Arquiva contato(s)', ["class" => 'custom-file-label ']) !!}
                {!! Form::file('file[]',["class" => "custom-file-input form-control-sm", "multiple"]); !!}
            </div>
        </div>
        <div class="form-group col-md-12 m-0">
            {!! Form::label('obs', 'Observação', ["class" => ' m-0']) !!}
            {!! Form::textarea("obs",null,["class" => "form-control form-control-sm text-uppercase", "rows" => 2]) !!}
        </div>
    </div>

    {{-- Campos de informações para cadastro de Clinete -> Fim --}}

    {{-- Botões de acesso -> Inicio --}}
    <div class="row mt-3">
        <div class="col-md-12">
        <button type="submit" id="btnSubmit" class="btn btn-outline-primary btn-block">Salvar</button>
        </div>
    </div>
    {{-- Botões de acesso -> Inicio --}}
{!! Form::close() !!}

<script>
    function proximoContato()
    {
        potencial_id = parseInt($('#potencial_id').val());
        periodo_atividade_id = parseInt($('#periodo_atividade_id').val());
        addDias = 0;
        switch(potencial_id)
        {
            case 1:
                if(periodo_atividade_id >= 1 && periodo_atividade_id <= 4)
                {
                    addDias = 60;
                }
                else if(periodo_atividade_id >= 5 && periodo_atividade_id <= 8)
                {
                    addDias = 30;
                }
                else if(periodo_atividade_id >= 9 && periodo_atividade_id <= 12)
                {
                    addDias = 20;
                }
                break;
            case 2:
                if(periodo_atividade_id >= 1 && periodo_atividade_id <= 4)
                {
                    addDias = 60;
                }
                else if(periodo_atividade_id >= 5 && periodo_atividade_id <= 8)
                {
                    addDias = 30;
                }
                else if(periodo_atividade_id >= 9 && periodo_atividade_id <= 12)
                {
                    addDias = 20;
                }
                break;
            case 3:
                if(periodo_atividade_id >= 1 && periodo_atividade_id <= 4)
                {
                    addDias = 30;
                }
                else if(periodo_atividade_id >= 5 && periodo_atividade_id <= 8)
                {
                    addDias = 20;
                }
                else if(periodo_atividade_id >= 9 && periodo_atividade_id <= 12)
                {
                    addDias = 7;
                }
                break;
            case 4:
                if(periodo_atividade_id >= 1 && periodo_atividade_id <= 4)
                {
                    addDias = 30;
                }
                else if(periodo_atividade_id >= 5 && periodo_atividade_id <= 8)
                {
                    addDias = 20;
                }
                else if(periodo_atividade_id >= 9 && periodo_atividade_id <= 12)
                {
                    addDias = 7;
                }
                break;
            case 5:
                if(periodo_atividade_id >= 1 && periodo_atividade_id <= 4)
                {
                    addDias = 30;
                }
                else if(periodo_atividade_id >= 5 && periodo_atividade_id <= 8)
                {
                    addDias = 20;
                }
                else if(periodo_atividade_id >= 9 && periodo_atividade_id <= 12)
                {
                    addDias = 7;
                }
                break;
            default:
                addDias = 0;
                break;
        }
        datatAtual = new Date();
        datatAtual.setDate(datatAtual.getDate() + parseInt(addDias));

        dia = datatAtual.getDate().toString();
        if(parseInt(dia.length) < 2)
        {
            dia = ['0',dia].join('');
        }
        mes = (datatAtual.getMonth() + 1).toString();
        if(parseInt(mes.length) < 2)
        {
            mes = '0' + mes;
        }
        ano = datatAtual.getFullYear().toString();
        newProximoContato = [ano, mes, dia].join('-');

        $("#proximo_contato").val(newProximoContato);

        return true;
    }

    $("form").ready(function(){
        proximoContato();
        $("#tempo").val($("#clock").text());
    })


    $("#ocorrencia_id").change(function(){
        codEnabledProximoContato = new Array(12,41,02,38,9)
        cod_ocorrencia = parseInt($(this).val());

        if($.inArray(cod_ocorrencia,codEnabledProximoContato) != -1)
        {
            $("#proximo_contato").attr("readonly",false);
        }
        else
        {
            $("#proximo_contato").attr("readonly",true);
            proximoContato();
        }
    })

    $('form').submit(function(){
        $('#ocorrencia_pessoa_id').val($("#pessoa_id").val());
        $('#potencial_pessoa_id').val($("#potencial_id").val());
        $('#periodo_atividade_pessoa_id').val($("#periodo_atividade_id").val());
        $('#modalSacNovo').modal('hide');
    });

    $("#proximo_contato").blur(function(){
        if($("#proximo_contato").val() != '')
        {
            url = "{!! route('sacs.date.next') !!}/" + $("#potencial_id").val() + '/' + $("#periodo_atividade_id").val() + '/' + $(this).val();
            $ValidaProximoContato = $.get(url,function(data){
                newDateProximoContatoSet = new Date(data.proximo_contato);
                newDateProximoContatoSet.setDate(newDateProximoContatoSet.getDate() + 1);


                dia = newDateProximoContatoSet.getDate().toString();
                if(parseInt(dia.length) < 2)
                {
                    dia = ['0',dia].join('');
                }
                mes = (newDateProximoContatoSet.getMonth() + 1).toString();
                if(parseInt(mes.length) < 2)
                {
                    mes = '0' + mes;
                }
                ano = newDateProximoContatoSet.getFullYear().toString();
                newDateProximoContato = [ano, mes, dia].join('-');
                if(data.tipo == 1)
                {
                    joinArray = [
                        'Já existe contatos agendados com potencial',
                        $("#potencial_id").val(),
                        'e período de atividade',
                        $("#periodo_atividade_id").val(),
                        '.. gostaria de uma opnião de data?'
                    ];
                    msgCompleta = joinArray.join(' ');
                    msgCompleta = msgCompleta.replace(' ..', '.');

                    // Swal.fire({
                    //     text: msgCompleta,
                    //     icon: 'warning',
                    //     showCancelButton: true,
                    //     confirmButtonColor: '#3085d6',
                    //     cancelButtonColor: '#d33',
                    //     confirmButtonText: 'Sim'
                    //     }).then((result) => {
                    //     if (result.value) {
                    //         $("#proximo_contato").val(newDateProximoContato);
                    //     }
                    // })
                }

            })

        }
    });

</script>
