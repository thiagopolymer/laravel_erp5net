@foreach($rsClientes as $reg)
    <tr class="text-dark small">
        <td class="text-center">{{ $reg->pessoa_id }}</td>
        <td>{{ $reg->razaosocial }}</td>
        <td>{{ $reg->cnpj }}</td>
        <td>{{ date("d/m/Y",strtotime($reg->proximo_contato)) }}</td>
        <td
            class="text-center"
            title="{!! $reg->potencial_descricao !!}"
            style="width:90px ;background-color: {!! $reg->potencial_cor_hexa !!};"
        ></td>
        <td
            class="text-center"
            title="{!! $reg->periodo_descricao !!}"
            style="width:90px ;background-color: {!! $reg->periodo_cor_hexa !!};"
        ></td>
        <td class="text-center ">
            <a href="{{ route('sacs.edit',['id' => $reg->pessoa_id]) }}" title="Iniciar Atendimento"><i class="fa fa-phone-volume text-dark"></i></a>
            {{-- <a href="" title="Visualizar"><i class="fa fa-eye text-dark"></i></a>
            <a href="" title="Excluir"><i class="fa fa-trash-alt text-dark"></i></a> --}}
        </td>
    </tr>
@endforeach
