
<link href="{{ asset('css/sb-admin-2.min.css') }}" rel="stylesheet" type="text/css" />
<button type="button" id='teste' class="bth btn-danger mb-3">Excel</button>

<table class="table">
    <thead class="text-dark">
        <tr>
            {{-- Pessoas -> Ínicio --}}
            <th>Id</th>
            <th>Tipo Pessoa</th>
            <th>Pessoa</th>
            <th>CNPJ/CPF</th>
            <th>Nome Fantasia</th>
            <th>Razão Social/Nome</th>
            <th>Insc. Estadual</th>
            <th>CEP</th>
            <th>Endereço</th>
            <th>Número</th>
            <th>Complemento</th>
            <th>Bairro</th>
            <th>Cidade (id)</th>
            <th>Cidade (nome)</th>
            <th>Estado</th>
            <th>fone (Receita)</th>
            <th>Ramal (Receita)</th>
            <th>E-mail (Receita)</th>
            <th>fone (Fiscal)</th>
            <th>Ramal (Fiscal)</th>
            <th>E-mail (Fiscal)</th>
            <th>fone 1</th>
            <th>Ramal 1</th>
            <th>E-mail 1</th>
            <th>fone 2</th>
            <th>Ramal 2</th>
            <th>E-mail 2</th>
            {{-- Pessoas -> Fim --}}

            {{-- Cliente -> Ínicio --}}
            <th>Classificacao (ID)</th>
            <th>Classificacao (Descrição)</th>
            <th>Representante (ID)</th>
            <th>Representante (Razão Social)</th>
            <th>Representante (CNPJ/CPF)</th>
            <th>Transportadora (ID)</th>
            <th>Transportadora (Razão Social)</th>
            <th>Transportadora (CNPJ/CPF)</th>
            <th>Potencial (ID)</th>
            <th>Potencial (Descrição)</th>
            <th>Period. Atividade (ID)</th>
            <th>Period. Atividade (Descrição)</th>
            <th>Suframa</th>
            <th>Preferência</th>
            <th>Negativado</th>
            <th>Facebook</th>
            <th>Instagran</th>
            <th>Twiter</th>
            <th>Youtube</th>
            <th>linkedIn</th>
            <th>CNAE Primário</th>
            <th>CNAE Sec. 01</th>
            <th>CNAE Sec. 02</th>
            <th>CNAE Sec. 03</th>
            <th>CNAE Sec. 04</th>
            <th>CNAE Sec. 05</th>
            <th>Situacao Empresa</th>
            <th>Porte Empresa</th>
            <th>Abertura Empresa</th>
            {{-- Cliente -> Fim --}}

            {{-- Transportadora -> Ínicio --}}
            <th>Entrega/Coleta</th>
            <th>Taxa de Coleta</th>
            <th>Valor da Coleta</th>
            <th>Obs</th>
            {{-- Transportadora -> Fim --}}

            {{-- Representante -> Ínicio --}}
            <th>comissao</th>
            <th>bacen_banco_id</th>
            <th>agencia</th>
            <th>conta</th>
            <th>Obs</th>
            {{-- Representante -> Fim --}}

            {{-- SACs -> Ínicio --}}
            <th>Ocorrência - Inativo</th>
            <th>Ocorrência - Prospecção</th>
            <th>Ocorrência - Estoque</th>
            <th>Ocorrência - Ausente</th>
            <th>Ocorrência - Pedido Fechado</th>
            <th>Ocorrência - Cotação Pendente</th>
            <th>Ocorrência - Perde Preço</th>
            <th>Ocorrência - Diversos Especificar</th>
            <th>Ocorrência - Telefone errado</th>
            <th>Ocorrência - Mudou Comprador</th>
            <th>Ocorrência - Solicitou Recontato</th>
            <th>Ocorrência - Telefone Não Atende</th>
            <th>Ocorrência - Cliente Negativado no SAC</th>
            <th>Ocorrência - Representante</th>
            <th>Ocorrência - Recontato com data específica</th>
            <th>Ocorrência - Empresa de Grupo Economico</th>
            <th>Sem Ocorrência</th>
            {{-- SACs -> Fim --}}


        </tr>
    </thead>
    <tbody class="text-dark">
        @foreach ($rsClientes as $reg)
            <tr>
                {{-- Pessoa -> Ínicio --}}
                <td>{{ $reg->pessoa_id }}</td>
                <td>Cliente</td>
                <td>@if($reg->pessoa_fisica == 0)Jurídica @else Fisíca @endif</td>
                <td>{{ $reg->cnpj }}</td>
                <td>{{ $reg->nomefantasia }}</td>
                <td>{{ $reg->razaosocial }}</td>
                <td>{{ $reg->inscestadual }}</td>
                <td>{{ $reg->cep }}</td>
                <td>{{ $reg->endereco }}</td>
                <td>{{ $reg->numero }}</td>
                <td>{{ $reg->complemento }}</td>
                <td>{{ $reg->bairro }}</td>
                <td>{{ $reg->cidade_id }}</td>
                <td></td>
                <td></td>
                <td>{{ $reg->fone_receita }}</td>
                <td>{{ $reg->ramal_receita }}</td>
                <td>{{ $reg->email_receita }}</td>
                <td>{{ $reg->fone_fiscal }}</td>
                <td>{{ $reg->ramal_fiscal }}</td>
                <td>{{ $reg->email_fiscal }}</td>
                <td>{{ $reg->fone1 }}</td>
                <td>{{ $reg->ramal1 }}</td>
                <td>{{ $reg->email1 }}</td>
                <td>{{ $reg->fone2 }}</td>
                <td>{{ $reg->ramal2 }}</td>
                <td>{{ $reg->email2 }}</td>
                {{-- Pessoa -> Fim --}}

                {{-- Cliente -> Ínicio --}}
                <td>{{ $reg->classificacao_id }}</td>
                <td></td>
                <td>{{ $reg->representante_id }}</td>
                <td></td>
                <td></td>
                <td>{{ $reg->transportadora_id }}</td>
                <td></td>
                <td></td>
                <td>{{ $reg->potencial_id }}</td>
                <td></td>
                <td>{{ $reg->periodo_atividade_id }}</td>
                <td></td>
                <td>{{ $reg->suframa }}</td>
                <td>{{ $reg->preferencial }}</td>
                <td>{{ $reg->serasa_negativado }}</td>
                <td>{{ $reg->facebook }}</td>
                <td>{{ $reg->instagran }}</td>
                <td>{{ $reg->twiter }}</td>
                <td>{{ $reg->youtube }}</td>
                <td>{{ $reg->linkedin }}</td>
                <td>{{ $reg->cnaeprimario }}</td>
                <td>{{ $reg->cnaesecundario1 }}</td>
                <td>{{ $reg->cnaesecundario2 }}</td>
                <td>{{ $reg->cnaesecundario3 }}</td>
                <td>{{ $reg->cnaesecundario4 }}</td>
                <td>{{ $reg->cnaesecundario5 }}</td>
                <td>{{ $reg->situacao }}</td>
                <td>{{ $reg->porte }}</td>
                <td>{{ $reg->abertura }}</td>
                {{-- Clientes -> Fim --}}

                {{-- Transportadora -> Ínicio --}}
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                {{-- Transportadora -> Fim --}}

                {{-- Representante -> Ínicio --}}
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                {{-- Representante -> Fim --}}

                {{-- SACs -> Ínicio --}}
                @php
                    $sacFilter = $rsSacs->where('pessoa_id',$reg->pessoa_id)->first();
                @endphp
                @if(!$sacFilter)
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @else
                    <td>{{ $sacFilter->ocorrencia_1 }}</td>
                    <td>{{ $sacFilter->ocorrencia_2 }}</td>
                    <td>{{ $sacFilter->ocorrencia_8 }}</td>
                    <td>{{ $sacFilter->ocorrencia_9 }}</td>
                    <td>{{ $sacFilter->ocorrencia_11 }}</td>
                    <td>{{ $sacFilter->ocorrencia_12 }}</td>
                    <td>{{ $sacFilter->ocorrencia_14 }}</td>
                    <td>{{ $sacFilter->ocorrencia_19 }}</td>
                    <td>{{ $sacFilter->ocorrencia_20 }}</td>
                    <td>{{ $sacFilter->ocorrencia_23 }}</td>
                    <td>{{ $sacFilter->ocorrencia_24 }}</td>
                    <td>{{ $sacFilter->ocorrencia_38 }}</td>
                    <td>{{ $sacFilter->ocorrencia_39 }}</td>
                    <td>{{ $sacFilter->ocorrencia_40 }}</td>
                    <td>{{ $sacFilter->ocorrencia_41 }}</td>
                    <td>{{ $sacFilter->ocorrencia_42 }}</td>
                    <td>{{ $sacFilter->sem_ocorrencia }}</td>
                @endif
                {{-- SACs -> Fim --}}

            </tr>
        @endforeach

        @foreach ($rsTransportadoras as $reg)
            <tr>
                {{-- Pessoa -> Ínicio --}}
                <td>{{ $reg->pessoa_id }}</td>
                <td>Transportadora</td>
                <td>@if($reg->pessoa_fisica == 0)Juírdica @else Fisíca @endif</td>
                <td>{{ $reg->cnpj }}</td>
                <td>{{ $reg->nomefantasia }}</td>
                <td>{{ $reg->razaosocial }}</td>
                <td>{{ $reg->inscestadual }}</td>
                <td>{{ $reg->cep }}</td>
                <td>{{ $reg->endereco }}</td>
                <td>{{ $reg->numero }}</td>
                <td>{{ $reg->complemento }}</td>
                <td>{{ $reg->bairro }}</td>
                <td>{{ $reg->cidade_id }}</td>
                <td></td>
                <td></td>
                <td>{{ $reg->fone_receita }}</td>
                <td>{{ $reg->ramal_receita }}</td>
                <td>{{ $reg->email_receita }}</td>
                <td>{{ $reg->fone_fiscal }}</td>
                <td>{{ $reg->ramal_fiscal }}</td>
                <td>{{ $reg->email_fiscal }}</td>
                <td>{{ $reg->fone1 }}</td>
                <td>{{ $reg->ramal1 }}</td>
                <td>{{ $reg->email1 }}</td>
                <td>{{ $reg->fone2 }}</td>
                <td>{{ $reg->ramal2 }}</td>
                <td>{{ $reg->email2 }}</td>
                {{-- Pessoa -> Fim --}}

                {{-- Cliente -> Ínicio --}}
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                {{-- Clientes -> Fim --}}

                {{-- Transportadora -> Ínicio --}}
                <td>{{ $reg->coleta }}</td>
                <td>{{ $reg->taxa }}</td>
                <td>{{ $reg->valor }}</td>
                <td>{{ $reg->obs }}</td>
                {{-- Transportadora -> Fim --}}

                {{-- Representante -> Ínicio --}}
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                {{-- Representante -> Fim --}}
                {{-- SACs -> Ínicio --}}
                @php
                    $sacFilter = $rsSacs->where('pessoa_id',$reg->pessoa_id)->first();
                @endphp
                @if(!$sacFilter)
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @else
                    <td>{{ $sacFilter->ocorrencia_1 }}</td>
                    <td>{{ $sacFilter->ocorrencia_2 }}</td>
                    <td>{{ $sacFilter->ocorrencia_8 }}</td>
                    <td>{{ $sacFilter->ocorrencia_9 }}</td>
                    <td>{{ $sacFilter->ocorrencia_11 }}</td>
                    <td>{{ $sacFilter->ocorrencia_12 }}</td>
                    <td>{{ $sacFilter->ocorrencia_14 }}</td>
                    <td>{{ $sacFilter->ocorrencia_19 }}</td>
                    <td>{{ $sacFilter->ocorrencia_20 }}</td>
                    <td>{{ $sacFilter->ocorrencia_23 }}</td>
                    <td>{{ $sacFilter->ocorrencia_24 }}</td>
                    <td>{{ $sacFilter->ocorrencia_38 }}</td>
                    <td>{{ $sacFilter->ocorrencia_39 }}</td>
                    <td>{{ $sacFilter->ocorrencia_40 }}</td>
                    <td>{{ $sacFilter->ocorrencia_41 }}</td>
                    <td>{{ $sacFilter->ocorrencia_42 }}</td>
                    <td>{{ $sacFilter->sem_ocorrencia }}</td>
                @endif
                {{-- SACs -> Fim --}}

            </tr>
        @endforeach

        @foreach ($rsRepresentantes as $reg)
            <tr>
                {{-- Pessoa -> Ínicio --}}
                <td>{{ $reg->pessoa_id }}</td>
                <td>Representante</td>
                <td>@if($reg->pessoa_fisica == 0)Jurídica @else Fisíca @endif</td>
                <td>{{ $reg->cnpj }}</td>
                <td>{{ $reg->nomefantasia }}</td>
                <td>{{ $reg->razaosocial }}</td>
                <td>{{ $reg->inscestadual }}</td>
                <td>{{ $reg->cep }}</td>
                <td>{{ $reg->endereco }}</td>
                <td>{{ $reg->numero }}</td>
                <td>{{ $reg->complemento }}</td>
                <td>{{ $reg->bairro }}</td>
                <td>{{ $reg->cidade_id }}</td>
                <td></td>
                <td></td>
                <td>{{ $reg->fone_receita }}</td>
                <td>{{ $reg->ramal_receita }}</td>
                <td>{{ $reg->email_receita }}</td>
                <td>{{ $reg->fone_fiscal }}</td>
                <td>{{ $reg->ramal_fiscal }}</td>
                <td>{{ $reg->email_fiscal }}</td>
                <td>{{ $reg->fone1 }}</td>
                <td>{{ $reg->ramal1 }}</td>
                <td>{{ $reg->email1 }}</td>
                <td>{{ $reg->fone2 }}</td>
                <td>{{ $reg->ramal2 }}</td>
                <td>{{ $reg->email2 }}</td>
                {{-- Pessoa -> Fim --}}

                {{-- Cliente -> Ínicio --}}
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                {{-- Clientes -> Fim --}}

                {{-- Transportadora -> Ínicio --}}
                <td></td>
                <td></td>
                <td></td>
                <td></td>
                {{-- Transportadora -> Fim --}}

                {{-- Representante -> Ínicio --}}
                <td>{{ $reg->comissao }}</td>
                <td>{{ $reg->bacen_banco_id }}</td>
                <td>{{ $reg->agencia }}</td>
                <td>{{ $reg->conta }}</td>
                <td>{{ $reg->obs }}</td>
                {{-- Representante -> Fim --}}
                {{-- SACs -> Ínicio --}}
                @php
                    $sacFilter = $rsSacs->where('pessoa_id',$reg->pessoa_id)->first();
                @endphp
                @if(!$sacFilter)
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                    <td></td>
                @else
                    <td>{{ $sacFilter->ocorrencia_1 }}</td>
                    <td>{{ $sacFilter->ocorrencia_2 }}</td>
                    <td>{{ $sacFilter->ocorrencia_8 }}</td>
                    <td>{{ $sacFilter->ocorrencia_9 }}</td>
                    <td>{{ $sacFilter->ocorrencia_11 }}</td>
                    <td>{{ $sacFilter->ocorrencia_12 }}</td>
                    <td>{{ $sacFilter->ocorrencia_14 }}</td>
                    <td>{{ $sacFilter->ocorrencia_19 }}</td>
                    <td>{{ $sacFilter->ocorrencia_20 }}</td>
                    <td>{{ $sacFilter->ocorrencia_23 }}</td>
                    <td>{{ $sacFilter->ocorrencia_24 }}</td>
                    <td>{{ $sacFilter->ocorrencia_38 }}</td>
                    <td>{{ $sacFilter->ocorrencia_39 }}</td>
                    <td>{{ $sacFilter->ocorrencia_40 }}</td>
                    <td>{{ $sacFilter->ocorrencia_41 }}</td>
                    <td>{{ $sacFilter->ocorrencia_42 }}</td>
                    <td>{{ $sacFilter->sem_ocorrencia }}</td>
                @endif
                {{-- SACs -> Fim --}}

            </tr>
        @endforeach

    </tbody>
</table>



<script src="{{ asset('vendor/jquery/jquery.min.js') }}"></script>


<script>
    function download_csv(csv, filename) {
        var csvFile;
        var downloadLink;

        // CSV FILE
        csvFile = new Blob([csv], {type: "text/csv"});

        // Download link
        downloadLink = document.createElement("a");

        // File name
        downloadLink.download = filename;

        // We have to create a link to the file
        downloadLink.href = window.URL.createObjectURL(csvFile);

        // Make sure that the link is not displayed
        downloadLink.style.display = "none";

        // Add the link to your DOM
        document.body.appendChild(downloadLink);

        // Lanzamos
        downloadLink.click();
    }

    function export_table_to_csv(html, filename) {
        var csv = [];
        var rows = document.querySelectorAll("table tr");

        for (var i = 0; i < rows.length; i++) {
            var row = [], cols = rows[i].querySelectorAll("td, th");

            for (var j = 0; j < cols.length; j++)
                row.push(cols[j].innerText);

            csv.push(row.join(";"));
        }

        // Download CSV
        download_csv(csv.join("\n"), filename);
    }

    $('#teste').click(function(){
        var html = document.querySelector("table").outerHTML;
        export_table_to_csv(html, "table.csv");
    })
</script>
