@extends('layouts.main')

@section('content')
	<!-- DataTales Example -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-dark bg-secondary small">
                            <th class="text-center">ID</th>
                            <th>Razão Social</th>
                            <th>CNPJ</th>
                            <th class="text-center" style="width:100px;">Menu</th>
                        </tr>
                        <tr>
                            <th colspan="6"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($rsRepresentantes as $reg)
                            <tr class="text-dark small">
                                <td class="text-center">{{ $reg->pessoa_id }}</td>
                                <td>{{ $reg->pessoa->razaosocial }}</td>
                                <td>{{ $reg->pessoa->cnpj }}</td>
                                <td class="text-center ">
                                    <a href="{{ route('pessoas.representantes.edit',['id' => $reg->id]) }}" title="Alterar"><i class="fa fa-edit text-dark"></i></a>
                                    {{-- <a href="" title="Visualizar"><i class="fa fa-eye text-dark"></i></a>
                                    <a href="" title="Excluir"><i class="fa fa-trash-alt text-dark"></i></a> --}}
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                    <tfoot>
                        <tr>
                            <th colspan="4">
                                        {{-- <small>Total de {{ $rsPessoas->count() }} registros</small> --}}
                                {{$rsRepresentantes->links('vendor.pagination.bootstrap-4')}}
                            </th>

                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
@endsection

