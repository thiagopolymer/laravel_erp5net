@extends('layouts.main')

@section('content')
	<!-- DataTales cidade -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            @if(isset($rsRepresentante->id) || $rsRepresentante->id > 0)
                {!! Form::model($rsRepresentante, ['route' => ['pessoas.representantes.update']]) !!}
                {!! Form::hidden("id",$rsRepresentante->id) !!}
            @else
                {!! Form::model($rsRepresentante, ['route' => ['pessoas.representantes.insert']]) !!}
            @endif
                {{-- Informações  de cadastro de Pessoa --}}
                <div class="row">
                    {{-- Classificação -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('pessoa_id', 'ID', ['class' => "m-0"]) !!}
                        {!! Form::text("pessoa_id",null,["class" => "form-control form-control-sm text-uppercase", "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('cnpj', 'CNPJ', ['class' => "m-0"]); !!}
                        {!! Form::text("cnpj",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                    </div>
                    <div class="form-group col-md-7 m-0">
                        {!! Form::label('razaosocial', 'Razão Social', ['class' => "m-0"]); !!}
                        {!! Form::text("razaosocial",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                    </div>
                </div>
                {{-- Informações de cadastro de Pessoal -> Fim --}}

                {{-- Campos de informações para cadastro de Representantes -> Inicio --}}
                <div class="row">
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('comissao', 'Comissão %', ['class' => "m-0"]); !!}
                        {!! Form::text("comissao",null,["class" => "form-control form-control-sm text-uppercase",]) !!}
                    </div>

                    {{-- Banco -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('bacen_banco_id', 'Banco', ['class' => "m-0"]); !!}
                        <div class="input-group mb-2 mr-sm-2">
                          {!! Form::text("bacen_banco_id",null,["class" => "form-control form-control-sm text-uppercase",]) !!}
                          <div class="input-group-prepend input-group-prepend-sm">
                            <button type="button" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalBacen_banco"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('descricao_banco', '.',['class' => "text-white m-0"]); !!}
                        {!! Form::text("descricao_banco",null,["class" => "form-control form-control-sm", "readonly"]) !!}
                    </div>
                    {{-- Banco -> Fim --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('agencia', 'Agência', ['class' => "m-0"]); !!}
                        {!! Form::text("agencia",null,["class" => "form-control form-control-sm text-uppercase",]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('conta', 'C/ Corrente', ['class' => "m-0"]); !!}
                        {!! Form::text("conta",null,["class" => "form-control form-control-sm text-uppercase",]) !!}
                    </div>
                    <div class="form-group col-md-12 m-0">
                        {!! Form::label('obs', 'Observação', ['class' => "m-0"]) !!}
                        {!! Form::textarea("obs",null,["class" => "form-control form-control-sm text-uppercase", "rows" => 2]) !!}
                    </div>
                </div>

                {{-- Campos de informações para cadastro de Clinete -> Fim --}}

                {{-- Botões de acesso -> Inicio --}}
                <div class="row mt-3">
                    <div class="col-md-6">
                        {!! Form::submit("Salvar",['class' => 'btn btn-outline-primary btn-block']) !!}
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route("pessoas.representantes") }}" title="Não salva os dados e volta para lista" class ='btn btn-outline-danger btn-block'>Cancelar</a>
                    </div>
                </div>
                {{-- Botões de acesso -> Inicio --}}

            {!! Form::close() !!}
        </div>
    </div>

    {{-- Modais para registros --}}

    {{-- Listagem de Banco - Ínicio --}}
    <div class="modal fade" id="modalBacen_banco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        @include('Cadastro.Pessoa.ListagemModal.bacen_banco')
    </div>
    {{-- Listagem de Banco - Fim --}}
@endsection
@section('execjs')
    <script>
        $(document).ready(function(){
            // Mascara de preenchimento - Inicio //
            $('#comissao').mask('00,00');
            $('#agencia').mask('0000000')
            $('#conta_corrente').mask('000000000')
            // Mascara de preenchimento - Fim //

            // Trava modal - Inicio //
            $('.modal').modal({backdrop: 'static', show: false});
            // Trava modal - Fim //

            // Seleção de banco - Inicio //
            $(".select_classificacao").dblclick(function(){
                $("#descricao_banco").val($(this).data("classificacao"));
                $("#bacen_banco_id").val($(this).data("id"));
                $('.modal').modal('hide');
            });

            $('#bacen_banco_id').blur(function(){
                if($(this).val() != "")
                {
                    url = "{!! route('api.bacen_banco') !!}/" + $(this).val();
                    console.log(url);
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#descricao_banco").val(dados.descricao)
                                $("#agencia").focus();
                            }
                            else
                            {
                                alert("Banco com esse código não existe");
                                $("#descricao_banco").val('');
                                $("#bacen_banco_id").val('');
                            }                        },
                    })
                }
            })
            // Seleção de banco - Fim //

            $('#pessoa_id').blur(function(){
                if($(this).val() != "")
                {
                    url = "{!! route('api.pessoa') !!}/" + $("#pessoa_id").val();
                    console.log(url);
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#cnpj").val(dados.cnpj)
                                $("#razaosocial").val(dados.razaosocial)
                                $("#comissao").focus();
                            }
                            else
                            {
                                $("#cnpj").val('');
                                $("#razaosocial").val('');
                            }                        },
                    })
                }
            })
            // Seleção de Pessoa - Fim //
            $('#bacen_banco_id').blur();
            $('#pessoa_id').blur();
        })
    </script>
@endsection
