@extends('layouts.main')

@section('content')
	<!-- DataTales Example -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            <div class="row mb-2">
                <div class="col-md-12 text-right">
                    <a href="{{ route('pessoas.edit') }}" class="btn btn-outline-dark" title="Novo">Novo</a>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-striped table-sm table-bordered" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-dark bg-secondary small">
                            <th class="text-center">ID</th>
                            <th>Razão Social</th>
                            <th>CNPJ</th>
                            <th class="text-center" style="width:100px;" >Menu</th>
                        </tr>
                        <tr>
                            <th colspan="4"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
                        </tr>
                    </thead>

                    <tbody id="list">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('execjs')
    <script>
        $(document).ready(function(){
            $("#list").load("{!! route('pessoas.list') !!}");

            $("#search").keyup(function(){
                if($("#search").val() == "")
                {
                    $("#list").load("{!! route('pessoas.list') !!}");
                }
                else
                {
                    url = "{!! route('pessoas.list') !!}" + '/' + $(this).val().replace(' ',"_");
                    $("#list").load(url);
                }
            })

        });
    </script>
@endsection
