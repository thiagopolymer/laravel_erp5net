@php
    use Illuminate\Support\Facades\DB;
    $rsTipoContatos = DB::table('tipo_contatos')->orderby('descricao')->get();
    $rsTimes = DB::table('times')->orderby('descricao')->get();

@endphp

<div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Incluir Contato</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="form-group col-md-3">
                        {!! Form::label('tipo_id_c', 'Tipo', ["class" => "small"]) !!}
                        <select class="form-control form-control-sm" id="tipo_id_c" name="tipo_id_c">
                            <option value="">--- Selecione ---</option>
                            @foreach ($rsTipoContatos as $reg)
                                <option value="{{ $reg->id }}">{{ $reg->descricao }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('nome_c', 'Nome', ["class" => "small"]) !!}
                        {!! Form::text("nome_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('nascimento_c', 'Dt. Nasc.', ["class" => "small"]) !!}
                        {!! Form::text("nascimento_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                </div>
                <div class="row">
                    <div class="form-group col-md-3">
                        {!! Form::label('fone_c', 'Telefone', ["class" => "small"]) !!}
                        {!! Form::text("fone_c",null,["class" => "form-control form-control-sm fone"]) !!}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('ramal_c', 'Ramal', ["class" => "small"]) !!}
                        {!! Form::text("ramal_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('celular_c', 'Celular', ["class" => "small"]) !!}
                        {!! Form::text("celular_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-3">
                        {!! Form::label('time_id_c', 'Time', ["class" => "small"]) !!}
                        <select class="form-control form-control-sm" id="time_id_c" name="time_id_c">
                            <option value="">--- Selecione ---</option>
                            @foreach ($rsTimes as $reg)
                                <option value="{{ $reg->id }}">{{ $reg->descricao }}/{{ $reg->federecao }}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('hobby_c', 'Hobby', ["class" => "small"]) !!}
                        {!! Form::text("hobby_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('facebook_c', 'Facebook', ["class" => "small"]) !!}
                        {!! Form::text("facebook_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('instagran_c', 'Instagran', ["class" => "small"]) !!}
                        {!! Form::text("instagran_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('twitter_c', 'Twiter', ["class" => "small"]) !!}
                        {!! Form::text("twitter_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('linkedin', 'Linkedin', ["class" => "small"]) !!}
                        {!! Form::text("linkedin_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6">
                        {!! Form::label('youtube_c', 'Youtube', ["class" => "small"]) !!}
                        {!! Form::text("youtube_c",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                </div>
                {{-- Botões de acesso -> Inicio --}}
                <div class="row">
                    <div class="col-md-12">
                        <button type="button" id="btn_contatos_salvar" class="btn btn-block btn-outline-primary">Salvar</button>
                    </div>
                </div>
                {{-- Botões de acesso -> Inicio --}}
            </div>
        </div>
    </div>
