@extends('layouts.main')

@section('content')
	<!-- DataTales cidade -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            @if(isset($rsTransportadora->id) || $rsTransportadora->id > 0)
                {!! Form::model($rsTransportadora, ['route' => ['pessoas.transportadoras.update']]) !!}
                {!! Form::hidden("id",$rsTransportadora->id) !!}
            @else
                {!! Form::model($rsTransportadora, ['route' => ['pessoas.transportadoras.insert']]) !!}
            @endif
            {{-- Informações  de cadastro de Pessoa --}}
                <div class="row">
                    {{-- Classificação -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('pessoa_id', 'ID', ["class" => ' m-0']) !!}
                        {!! Form::text("pessoa_id",null,["class" => "form-control form-control-sm text-uppercase", "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('cnpj', 'CNPJ', ["class" => ' m-0']) !!}
                        {!! Form::text("cnpj",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                    </div>
                    <div class="form-group col-md-7 m-0">
                        {!! Form::label('razaosocial', 'Razão Social', ["class" => ' m-0']) !!}
                        {!! Form::text("razaosocial",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                    </div>
                </div>
                <div id="accordion" class="mb-3 mt-3">
                    <div class="card">
                        <div class="card-header text-center m-0 pt-0 p-0 rounded" id="headingOne">
                            <h5 class="mb-0 p-0">
                                <button type="button" class="btn btn-block btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Clique para Mais Informações
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                {{-- Cadastro de Endereço -> Inicio --}}
                                <div class="row border-top-anger">
                                    <div class="form-group col-md-2">
                                        {!! Form::label('cep', 'CEP',['class' => "small"]) !!}
                                        {!! Form::text("cep",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('endereco', 'Endereço',['class' => "small"]) !!}
                                        {!! Form::text("endereco",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-2">
                                        {!! Form::label('numero', 'Número',['class' => "small"]) !!}
                                        {!! Form::text("numero",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-2">
                                        {!! Form::label('complemento', 'Complemento',['class' => "small"]) !!}
                                        {!! Form::text("complemento",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-3">
                                        {!! Form::label('bairro', 'Bairro',['class' => "small"]) !!}
                                        {!! Form::text("bairro",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-6">
                                        {!! Form::label('cidade', 'Cidade',['class' => "small"]) !!}
                                        @if($rsTransportadora->pessoa->cidade_id > 0)
                                            {!! Form::text("cidade",$rsTransportadora->pessoa->cidade->nome,["class" => "form-control form-control-sm", 'readonly']) !!}
                                        @else
                                            {!! Form::text("cidade",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                        @endif
                                    </div>
                                    <div class="form-group col-md-3">
                                        {!! Form::label('estado', 'Estado',['class' => "small"], ["class" => ' m-0']) !!}
                                        @if($rsTransportadora->pessoa->cidade_id > 0)
                                            {!! Form::text("estado",substr($rsTransportadora->pessoa->cidade->descricao,-2),["class" => "form-control form-control-sm", 'readonly']) !!}
                                        @else
                                            {!! Form::text("estado",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                        @endif
                                    </div>
                                </div>
                                {{-- Cadastro de Endereço -> Fim --}}

                                {{-- Cadastro de Contato -> Inicio --}}
                                <div class="row">
                                    <div class="form-group col-md-2">
                                        {!! Form::label('fone_receita', 'Telefone (Receita)',['class' => "small"], ["class" => ' m-0']) !!}
                                        {!! Form::text("fone_receita",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-2">
                                        {!! Form::label('ramal_receita', 'Ramal (Receita)',['class' => "small"], ["class" => ' m-0']) !!}
                                        {!! Form::text("ramal_receita",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-8">
                                        {!! Form::label('email_receita', 'E-mail (Receita)',['class' => "small"], ["class" => ' m-0']) !!}
                                        {!! Form::text("email_receita",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-2">
                                        {!! Form::label('fone_fiscal', 'Telefoen (Fiscal)',['class' => "small"], ["class" => ' m-0']) !!}
                                        {!! Form::text("fone_fiscal",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-2">
                                        {!! Form::label('ramal_fiscal', 'Ramal (Fiscal)',['class' => "small"]) !!}
                                        {!! Form::text("ramal_fiscal",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                    <div class="form-group col-md-8">
                                        {!! Form::label('email_fiscal', 'E-mail (Fiscal)',['class' => "small"]) !!}
                                        {!! Form::text("email_fiscal",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                                    </div>
                                </div>
                                {{-- Cadastro de Contato -> Fim--}}
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Informações de cadastro de Pessoal -> Fim --}}

                {{-- Campos de informações para cadastro de Transportadora -> Inicio --}}
                <div class="row">
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('coleta', 'Entrega/Coleta', ["class" => ' m-0']) !!}
                        {!! Form::select('coleta', ['0' => 'Entrega', '1' => 'Coleta'],null,["class" => "form-control form-control-sm", 'id' => 'coleta']) !!}
                    </div>

                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-checkbox mr-sm-5 pt-3">
                            {!! Form::checkbox('taxa',1,null,["class" => "custom-control-input", "id" => 'taxa']) !!}
                            {!! Form::label('taxa', 'Taxa de Coleta?',["class" => "custom-control-label", "for" => "taxa"], ["class" => ' m-0']) !!}
                        </div>
                    </div>

                    <div class="form-group col-md-3">
                        {!! Form::label('valor', 'Valor da Coleta', ["class" => ' m-0']) !!}
                        {!! Form::text("valor",null,["class" => "form-control form-control-sm text-uppercase",]) !!}
                    </div>
                    <div class="form-group col-md-12">
                        {!! Form::label('obs', 'Observação', ["class" => ' m-0']) !!}
                        {!! Form::textarea("obs",null,["class" => "form-control form-control-sm text-uppercase", "rows" => 2]) !!}
                    </div>
                </div>

                {{-- Campos de informações para cadastro de Clinete -> Fim --}}

                {{-- Botões de acesso -> Inicio --}}
                <div class="row">
                    <div class="col-md-6">
                        {!! Form::submit("Salvar",['class' => 'btn btn-outline-primary btn-block']) !!}
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route("pessoas.transportadoras") }}" title="Não salva os dados e volta para lista" class ='btn btn-outline-danger btn-block'>Cancelar</a>
                    </div>
                </div>
                {{-- Botões de acesso -> Inicio --}}

            {!! Form::close() !!}
        </div>
    </div>

    {{-- Modais para registros --}}

    {{-- Listagem de Banco - Ínicio --}}
    <div class="modal fade" id="modalBacen_banco" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        @include('Cadastro.Pessoa.ListagemModal.bacen_banco')
    </div>
    {{-- Listagem de Banco - Fim --}}
@endsection
@section('execjs')
    <script>
        $(document).ready(function(){
            // Mascara de preenchimento - Inicio //
            $('#valor').mask('00,00');
            // Mascara de preenchimento - Fim //

            $("#coleta").change(function(){
                if($(this).val() == 1)
                {
                    $("#taxa").prop("disabled", false);
                }
                else
                {
                    $("#taxa").prop("disabled", true);
                    $("#taxa").prop("checked", false);
                    $("#valor").prop("readonly", true);
                    $("#valor").val('');
                }
            });

            $("#taxa").change(function(){
                if($(this).prop("checked") == true)
                {
                    $("#valor").prop("readonly", false);
                }
                else
                {
                    $("#valor").prop("readonly", true);
                    $("#valor").val("");
                }
            });

            $("#coleta").change();

            $('#pessoa_id').blur(function(){
                if($(this).val() != "")
                {
                    url = "{!! route('api.pessoa') !!}/" + $("#pessoa_id").val();
                    console.log(url);
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#cnpj").val(dados.cnpj)
                                $("#razaosocial").val(dados.razaosocial)
                                $("#cep").val(dados.cep)
                                $("#endereco").val(dados.endereco)
                                $("#bairro").val(dados.bairro)
                                $("#numero").val(dados.numero)
                                $("#fone_receita").val(dados.fone_receita)
                                $("#ramal_receita").val(dados.ramal_receita)
                                $("#fone_fiscal").val(dados.fone_fiscal)
                                $("#ramal_fiscal").val(dados.ramal_fiscal)
                                $("#email_receita").val(dados.email_receita)
                                $("#email_fiscal").val(dados.email_fiscal)
                                $("#comissao").focus();
                            }
                            else
                            {
                                $("#cnpj").val('');
                                $("#razaosocial").val('');
                            }                        },
                    })
                }
            })
            // Seleção de Pessoa - Fim //
            $('#bacen_banco_id').blur();
            $('#pessoa_id').blur();
        })
    </script>
@endsection
