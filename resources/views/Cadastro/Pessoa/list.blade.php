@foreach($rsPessoas as $reg)
    <tr class="text-dark small">
        <td class="text-center">{{ $reg->id }}</td>
        <td>{{ $reg->razaosocial }}</td>
        <td>{{ $reg->cnpj }}</td>
        <td class="text-center ">
            <a href="{{ route('pessoas.edit',['id' => $reg->id]) }}" title="Alterar"><i class="fa fa-edit text-dark"></i></a>
        </td>
    </tr>
@endforeach
