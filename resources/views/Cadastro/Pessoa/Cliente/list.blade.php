@foreach($rsClientes as $reg)
    <tr class="text-dark small">
        <td class="text-center">{{ $reg->pessoa_id }}</td>
        <td>{{ $reg->pessoa->razaosocial }}</td>
        <td>{{ $reg->pessoa->cnpj }}</td>
        @if (isset($reg->potencial->descricao))
            <td class="text-center" title="{!! $reg->potencial->descricao !!}" style="width:90px ;background-color: {!! $reg->potencial->cor_hexa !!};"></td>
        @else
            <td class="text-center" title=""></td>
        @endif
        @if (isset($reg->periodo_atividade->descricao))
            <td class="text-center" title="{!! $reg->periodo_atividade->descricao !!}" style="width:90px ;background-color: {!! $reg->periodo_atividade->cor_hexa !!};"></td>
        @else
            <td class="text-center" title=""></td>
        @endif

        <td class="text-center ">
            <a href="{{ route('pessoas.clientes.edit',['id' => $reg->id]) }}" title="Alterar"><i class="fa fa-edit text-dark"></i></a>
            {{-- <a href="" title="Visualizar"><i class="fa fa-eye text-dark"></i></a>
            <a href="" title="Excluir"><i class="fa fa-trash-alt text-dark"></i></a> --}}
        </td>
    </tr>
@endforeach
