@extends('layouts.main')

@php
    use Illuminate\Support\Facades\DB;
    $rsContatos = DB::table("contatos")->where("pessoa_id", $rsCliente->pessoa_id)->get();
@endphp

@section('content')
	<!-- DataTales cidade -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            @if(isset($rsCliente->id) || $rsCliente->id > 0)
                {!! Form::model($rsCliente, ['route' => ['pessoas.clientes.update', $rsCliente->id]]) !!}
            @else
                {!! Form::model($rsCliente, ['route' => ['pessoas.clientes.insert']]) !!}
            @endif
                {{-- Informações  de cadastro de Pessoa --}}
                {!! Form::hidden('id'); !!}
                <div class="row">
                    {{-- Pessoa -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('pessoa_id', 'ID',["class" => ' m-0']) !!}
                        <div class="input-group mb-2 mr-sm-2 m-0">
                          {!! Form::text("pessoa_id",null,[
                                "class" => "form-control form-control-sm text-uppercase border border-danger",
                                "data-json" => route('api.pessoa'),
                                "readonly"
                            ]) !!}
                        </div>
                    </div>
                    {{-- Pessoa -> Fim --}}

                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('cnpj', 'CNPJ',["class" => ' m-0']) !!}
                        {!! Form::text("cnpj",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                    </div>
                    <div class="form-group col-md-7 m-0">
                        {!! Form::label('razaosocial', 'Razão Social',["class" => ' m-0']) !!}
                        {!! Form::text("razaosocial",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                    </div>
                </div>
                {{-- Informações de cadastro de Pessoal -> Fim --}}

                {{-- Campos de informações para cadastro de Cliente -> Inicio --}}
                <div class="row">
                    {{-- Classificação -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('classificacao_id', 'Classificação',["class" => ' m-0']); !!}
                        <div class="input-group mb-2 mr-sm-2 m-0">
                          {!! Form::text("classificacao_id",null,[
                                "class" => "form-control form-control-sm text-uppercase",
                                "data-json" => route('api.classificacao')
                            ]) !!}
                          <div class="input-group-prepend input-group-prepend-sm">
                            <button type="button" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalClassificacao"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('descricao_classificacao', '.',['class' => "text-white m-0"]); !!}
                        {!! Form::text("descricao_classificacao",null,["class" => "form-control form-control-sm", "readonly"]) !!}
                    </div>
                    {{-- Classificação->Fim --}}

                    {{-- Representante -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('representante_id', 'Representante',["class" => ' m-0']); !!}
                        <div class="input-group mb-2 mr-sm-2 m-0">
                          {!! Form::text("representante_id",null,[
                              "class" => "form-control form-control-sm text-uppercase",
                              "data-json" => route('api.representante')
                              ]) !!}
                          <div class="input-group-prepend input-group-prepend-sm m-0">
                            <button type="button" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalRepresentante"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('descricao_representante', '.',['class' => "text-white m-0"]); !!}
                        {!! Form::text("descricao_representante",null,["class" => "form-control form-control-sm", "readonly"]) !!}
                    </div>
                    {{-- Representante -> Fim --}}

                    {{-- Transportadora -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('transportadora_id', 'Transportadora',["class" => ' m-0']) !!}
                        <div class="input-group mb-2 mr-sm-2 m-0">
                          {!! Form::text("transportadora_id",null,[
                                "class" => "form-control form-control-sm text-uppercase",
                                "data-json" => route('api.transportadora')
                              ]) !!}
                          <div class="input-group-prepend input-group-prepend-sm m-0">
                            <button type="button" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalTransportadora"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('descricao_transportadora', '.',['class' => "text-white m-0"]); !!}
                        {!! Form::text("descricao_transportadora",null,["class" => "form-control form-control-sm", "readonly"]) !!}
                    </div>
                    {{-- Classificação -> Fim --}}
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('potencial', 'Potencial',["class" => ' m-0']) !!}
                        <select class="form-control form-control-sm" name="potencial_id" id="potencial_id">
                            <option value="">--- Selecione ---</option>
                            @foreach ($rsPotenciais as $reg)
                                @if ($rsCliente->potencial_id === $reg->id)
                                    <option value="{{ $reg->id }}" selected>{{ $reg->descricao }}</option>
                                @else
                                    <option value="{{ $reg->id }}">{{ $reg->descricao }}</option>
                                @endif
                            @endforeach
                        </select>
                    </div>

                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('suframa', 'Suframa',["class" => ' m-0']) !!}
                        {!! Form::text('suframa',null,['class' => 'form-control form-control-sm']) !!}
                    </div>
                    <div class="form-check form-check-inline">
                        <div class="custom-control custom-checkbox mr-sm-5 pt-3 m-0">
                            {!! Form::checkbox('preferencial',1,"",["class" => "custom-control-input", "id" => 'preferencial']) !!}
                            {!! Form::label('preferencial', 'Preferencial',["class" => "custom-control-label", "for" => "preferencial"]); !!}
                        </div>
                            <div class="custom-control custom-checkbox mr-sm-2 pt-3 m-0">
                            {!! Form::checkbox('serasa_negativado',1,"",["class" => "custom-control-input", "id" => 'serasa_negativado']) !!}
                            {!! Form::label('serasa_negativado', 'Negativado',["class" => "custom-control-label", "for" => "serasa_negativado"]); !!}
                        </div>
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('facebook', 'Facebook',["class" => ' m-0']) !!}
                        {!! Form::text('facebook',null,['class' => 'form-control form-control-sm']) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('instagran', 'Instagran',["class" => ' m-0']) !!}
                        {!! Form::text('instagran',null,['class' => 'form-control form-control-sm']) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('twiter', 'Twiter',["class" => ' m-0']) !!}
                        {!! Form::text('twiter',null,['class' => 'form-control form-control-sm']) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('youtube', 'Youtube',["class" => ' m-0']) !!}
                        {!! Form::text('youtube',null,['class' => 'form-control form-control-sm']) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('linkedin', 'Linkedin',["class" => ' m-0']) !!}
                        {!! Form::text('linkedin',null,['class' => 'form-control form-control-sm']) !!}
                    </div>
                </div>
                <div class="row mb-2">
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cnaeprimario', 'CNAE Pri.',["class" => ' m-0']) !!}
                        {!! Form::text('cnaeprimario',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cnaesecundario1', 'CNAE Sec. 01',["class" => ' m-0']) !!}
                        {!! Form::text('cnaesecundario1',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cnaesecundario2', 'CNAE Sec. 02',["class" => ' m-0']) !!}
                        {!! Form::text('cnaesecundario2',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cnaesecundario3', 'CNAE Sec. 03',["class" => ' m-0']) !!}
                        {!! Form::text('cnaesecundario3',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cnaesecundario4', 'CNAE Sec. 04',["class" => ' m-0']) !!}
                        {!! Form::text('cnaesecundario4',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cnaesecundario5', 'CNAE Sec. 05',["class" => ' m-0']) !!}
                        {!! Form::text('cnaesecundario5',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('situacao', 'Situação Cadastral',["class" => ' m-0']) !!}
                        {!! Form::text('situacao',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('porte', 'Porte',["class" => ' m-0']) !!}
                        {!! Form::text('porte',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                    <div class="form-group col-md-4 m-0">
                        {!! Form::label('abertura', 'Dt. Abertura',["class" => ' m-0']) !!}
                        {!! Form::text('abertura',null,['class' => 'form-control form-control-sm', "readonly"]) !!}
                    </div>
                </div>
                {{-- Campos de informações para cadastro de Cliente -> Fim --}}

                {{-- Campos de informações para cadastro de Contatos -> Ínicio --}}
                <div id="accordion" class="mb-3">
                    <div class="card">
                        <div class="card-header text-center m-0 pt-0 p-0 rounded" id="headingOne">
                            <h5 class="mb-0 p-0">
                                <button type="button" class="btn btn-block btn-secondary" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">
                                    Clique para Mais Contatos
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse" aria-labelledby="headingOne" data-parent="#accordion">
                            <div class="card-body">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="table-responsive">
                                            <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
                                                <thead>
                                                    <tr class="text-dark bg-secondary small">
                                                        <th>Tipo</th>
                                                        <th>Nome</th>
                                                        <th>Fone/Celular</th>
                                                        <th>Idade</th>
                                                        <th>
                                                            <button type="button" class="btn btn-sm btn-info" data-toggle="modal" data-target="#modalContatoNovo">Novo</button>
                                                        </th>
                                                    </tr>
                                                </thead>
                                                <tbody id="contato_info">
                                                    @foreach($rsContatos as $reg)
                                                        <tr class="small">
                                                            <td>{{ $reg->tipo_id }}</td>
                                                            <td>{{ $reg->nome }}</td>
                                                            <td>{{ $reg->fone }}</td>
                                                            <td>{{ $reg->nascimento }}</td>
                                                            <td>Menu</td>
                                                        </tr>
                                                    @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{-- Campos de informações para cadastro de Contatos -> Fim --}}

                {{-- Botões de acesso -> Inicio --}}
                <div class="row m-0">
                    <div class="col-md-6">
                        {!! Form::submit("Salvar",['class' => 'btn btn-outline-primary btn-block']) !!}
                    </div>
                    <div class="col-md-6">
                        <a href="{{ route("pessoas.clientes") }}" title="Não salva os dados e volta para lista" class ='btn btn-outline-danger btn-block'>Cancelar</a>
                    </div>
                </div>
                {{-- Botões de acesso -> Inicio --}}

            {!! Form::close() !!}
        </div>
    </div>

    {{-- Modais para registros --}}

    {{-- Listagem de Classificação - Ínicio --}}
    <div class="modal fade" id="modalClassificacao" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        @include('Cadastro.Pessoa.ListagemModal.classificacao')
    </div>
    {{-- Listagem de Classificação - Fim --}}

    {{-- Listagem de Representante - Ínicio --}}
    <div class="modal fade" id="modalRepresentante" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        @include('Cadastro.Pessoa.ListagemModal.representante')
    </div>
    {{-- Listagem de Representante - Fim --}}

    {{-- Listagem de Transportadora - Ínicio --}}
    <div class="modal fade" id="modalTransportadora" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        @include('Cadastro.Pessoa.ListagemModal.transportadora')
    </div>
    {{-- Listagem de Transportadora - Fim --}}

    {{-- Include de Contatos - Ínicio --}}
    <div class="modal fade" id="modalContatoNovo" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        @include('Cadastro.Pessoa.contato_form')
    </div>
    {{-- Include de Contatos - Fim --}}
@endsection

@section('execjs')
    <script>
        $(document).ready(function(){
            // Mascara de preenchimento - Inicio //
            $('#cnpj').mask('00.000.000/0000-00');
            $('.cep_mask').mask('00000-000')
            $('#telefone').mask('(00) 0 0000-0000')
            $('.fone').mask('(00) 0 0000-0000')
            $('#fone_c').mask('(00) 0000-0000')
            $('#celular_c').mask('(00) 0 0000-0000')
            // Mascara de preenchimento - Fim //

            // Trava modal - Inicio //
            $('.modal').modal({backdrop: 'static', show: false});
            // Trava modal - Fim //

            // Seleção de Pessoa - Inicio //
            $('#pessoa_id').blur(function(){
                if($(this).val() != "")
                {
                    url = $(this).data('json') + '/' + $(this).val();
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#cnpj").val(dados.cnpj)
                                $("#razaosocial").val(dados.razaosocial)
                                $("#classificacao_id").focus();
                                // Traz informações complementares do Cliente via WS Receita -> Ínicio
                                cnpj = $("#cnpj").val();
                                cnpj = cnpj.replace('.','');
                                cnpj = cnpj.replace('.','');
                                cnpj = cnpj.replace('/','');
                                cnpj = cnpj.replace('-','');

                                url = ("https://www.receitaws.com.br/v1/cnpj/" + cnpj);
                                $.ajax({
                                    url: url,
                                    jsonp: "callback",
                                    dataType: "jsonp",
                                    success: function(dados) {
                                        $("#abertura").val(dados.abertura);
                                        $("#cnaeprimario").val(dados.atividade_principal[0].code);
                                        $("#porte").val(dados.porte);
                                        $("#situacao").val(dados.situacao);
                                        $("#cnaesecundario1").val(dados.atividades_secundarias[0].code);
                                        $("#cnaesecundario2").val(dados.atividades_secundarias[1].code);
                                        $("#cnaesecundario3").val(dados.atividades_secundarias[2].code);
                                        $("#cnaesecundario4").val(dados.atividades_secundarias[3].code);
                                        $("#cnaesecundario5").val(dados.atividades_secundarias[4].code);
                                    }
                                });
                                // Traz informações complementares do Cliente via WS Receita -> Fim
                            }
                            else
                            {
                                alert("Código de classificacao não existe");
                                $("#cnpj").val('');
                                $("#razaosocial").val('');
                            }                        },
                    })
                }
            })
            // Seleção de Pessoa - Fim //

            // Seleção de classficacao - Inicio //
            $(".select_classificacao").dblclick(function(){
                $("#descricao_classificacao").val($(this).data("classificacao"));
                $("#classificacao_id").val($(this).data("id"));
                $('.modal').modal('hide');
            });

            $('#classificacao_id').blur(function(){
                if($(this).val() != "")
                {
                    url = $(this).data('json') + '/' + $(this).val();
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#descricao_classificacao").val(dados.descricao)
                            }
                            else
                            {
                                alert("Código de classificacao não existe");
                                $("#descricao_classificacao").val('');
                                $("#classificacao_id").val('');
                            }                        },
                    })
                }
            })
            // Seleção de classficacao - Fim //

            // Seleção de Representante - Inicio //
            $(".select_representante").dblclick(function(){
                $("#descricao_representante").val($(this).data("representante"));
                $("#representante_id").val($(this).data("id"));
                $('.modal').modal('hide');
            });

            $('#representante_id').blur(function(){
                if($(this).val() != "")
                {
                    url = $(this).data('json') + '/' + $(this).val();
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#descricao_representante").val(dados.descricao)
                            }
                            else
                            {
                                alert("Código de representante não existe");
                                $("#descricao_representante").val('');
                                $("#representante_id").val('');
                            }                        },
                    })
                }
            })
            // Seleção de classficacao - Fim //

            // Seleção de Transportadora - Inicio //
            $(".select_transportadora").dblclick(function(){
                $("#descricao_transportadora").val($(this).data("transportadora"));
                $("#transportadora_id").val($(this).data("id"));
                $('.modal').modal('hide');
            });

            $('#transportadora_id').blur(function(){
                if($(this).val() != "")
                {
                    url = $(this).data('json') + '/' + $(this).val();
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#descricao_transportadora").val(dados.razaosocial)
                            }
                            else
                            {
                                alert("Código de transportadora não existe");
                                $("#descricao_transportadora").val('');
                                $("#transportadora_id").val('');
                            }                        },
                    })
                }
            })
            // Seleção de Transportadora - Fim //

            // Carrega informações dos campos ID
            $("#pessoa_id").blur();
            $("#classificacao_id").blur();
        })

        $("#btn_contatos_salvar").click(function(){
            msgObrigatoriedade = "";
            if($("#tipo_id").val() == "")
            {
                msgObrigatoriedade = "Selecione o Tipo de Contato";
                $("#tipo_id").focus();
            }
            else if($("#nome").val() == "")
            {
                msgObrigatoriedade = "Digite o nome do Contato"
                $("#nome").focus();
            }
            else if($("#fone").val() == "" || $("#celular").val() == "")
            {
                msgObrigatoriedade = "Digite um número de Telefone ou Celular"
                $("#fone").focus();
            }

            if(msgObrigatoriedade != "")
            {
                alert(msgObrigatoriedade);
            }
            else
            {
                linhaContato = "";
                linhaContato = linhaContato + "<tr>";
                linhaContato = linhaContato + "    <td>" + $("#tipo_id_c").val() + "<input type='hidden' name='tipo_id_c' value='" + $("#tipo_id_c").val() + "'></td>";
                linhaContato = linhaContato + "    <td>" + $("#nome_c").val() + "<input type='hidden' name='nome_c' value='" + $("#nome_c").val() + "'></td>";
                linhaContato = linhaContato + "    <td>" + $("#fone_c").val() + "<input type='hidden' name='fone_c' value='" + $("#fone_c").val() + "'></td>";
                linhaContato = linhaContato + "    <td></td>";
                linhaContato = linhaContato + "    <td class='text-center'>Menu</td>";
                linhaContato = linhaContato + "</tr>";
                linhaContato = linhaContato + "<input type='hidden' name='celular_c' value='" + $("#celular_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='ramal_c' value='" + $("#ramal_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='nascimento_c' value='" + $("#nascimento_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='facebook_c' value='" + $("#facebook_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='hobby_c' value='" + $("#hobby_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='twitter_c' value='" + $("#twitter_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='linkedin_c' value='" + $("#linkedin_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='instagran_c' value='" + $("#instagran_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='youtube_c' value='" + $("#youtube_c").val() + "'>";
                linhaContato = linhaContato + "<input type='hidden' name='time_id_c' value='" + $("#time_id_c").val() + "'>";

                $("#contato_info").append(linhaContato);

                url = "{!! route('api.insert_contato') !!}";
                form = $("form").serialize();
                $.ajax({
                    url: url,
                    type: "POST",
                    data:form,
                    contentType: "application/x-www-form-urlencoded;charset=UTF-8",
                    success: function(dados) {
                        console.log(dados);
                    }
                })

                $('.modal').modal('hide');
                $("#tipo_id_c").val('');
                $("#nome_c").val('');
                $("#fone_c").val('');
                $("#celular_c").val('');
                $("#ramal_c").val('');
                $("#nascimento_c").val('');
                $("#facebook_c").val('');
                $("#hobby_c").val('');
                $("#twitter_c").val('');
                $("#linkedin_c").val('');
                $("#instagran_c").val('');
                $("#youtube_c").val('');
                $("#time_id_c").val('');

            }
        });
    </script>
@endsection
