@extends('layouts.main')

@section('content')
	<!-- DataTales Example -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            {{-- <div class="row mb-2">
                <div class="col-md-10">
                    <a href="#" class="btn btn-outline-dark btn-sm" id="excel" title="Execl CSV">Excel CSV</a>
                    <a href="#" class="btn btn-outline-dark btn-sm" title="PDF">PDF</a>
                    <a href="#" class="btn btn-outline-dark btn-sm" title="Imprimir">Imprimir</a>
                    <a href="#" class="btn btn-outline-dark btn-sm" title="Colunas Visíveis">Colunas Visíveis</a>
                </div>
                <div class="col-md-2">
                    <a href="{{ route('pessoas.edit') }}" class="btn btn-outline-dark btn-block display" title="Novo">Novo</a>
                </div>
            </div> --}}

            <div class="table-responsive">
                <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-dark bg-secondary small">
                            <th class="text-center">ID</th>
                            <th>Razão Social</th>
                            <th>CNPJ</th>
                            <th class="text-center">Potencial</th>
                            <th class="text-center" style="width:120px;">Período Atividade</th>
                            <th class="text-center" style="width:100px;">Menu</th>
                        </tr>
                        <tr>
                            <th colspan="6"><input type="text" class="form-control form-control-sm" id="search" placeholder="pesquisar"></th>
                        </tr>
                    </thead>
                    <tbody id="list">
                    </tbody>
                </table>
            </div>
        </div>
    </div>
@endsection

@section('execjs')
    <script>

        $(document).ready(function(){
            $("#list").load("{!! route('pessoas.clientes.list') !!}");

            $(".btn_pagination").click(function(){
                if($("#search").val() == "")
                {
                    url = "{!! route('pessoas.clientes.list') !!}" + '/' + $(this).val();
                }
                else
                {
                    url = "{!! route('pessoas.clientes.list') !!}" + '/' + $(this).val() + '/' + $("#search").val();
                }

                $("#list").load(url);


                activeButton = $(this).attr("id");

            })

            $("#search").keyup(function(){
                textSearch = $(this).val();

                url = "{!! route('pessoas.clientes.list') !!}" + '/' + 0 + '/' + textSearch.replace(' ','_');
                $("#list").load(url);
            })
        });
    </script>
@endsection
