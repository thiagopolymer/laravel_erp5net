@extends('layouts.main')

@section('content')
	<!-- DataTales cidade -->
	<div class="card shadow mb-3 border-secondary">
        <div class="card-body">
            @if ($errors->any())
                <div class="alert alert-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{ $error }}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            @if(isset($rsPessoa->id) || $rsPessoa->id > 0)
                {!! Form::model($rsPessoa, ['route' => ['pessoas.update', $rsPessoa->id], "autocomplete" => "off"]) !!}
                @php($readonly = "readonly")
            @else
                {!! Form::model($rsPessoa, ['route' => ['pessoas.insert'], "autocomplete" => "off"]) !!}
                @php($readonly = "")
            @endif
                {{-- Cadastro de Informações Pessoal --}}
                {!! Form::hidden("btn_acesso_id",null,["id" => "btn_acesso_id"]) !!}
                <div class="row">
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('id', 'ID',["class" => "m-0"]) !!}
                        {!! Form::text("id",null,["class" => "form-control form-control-sm", "readonly"]) !!}</small>
                    </div>

                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('pessoa_fisica', 'Pessoa',["class" => "m-0"]) !!}
                        {!! Form::select('pessoa_fisica', ['0' => 'Jurídica', '1' => 'Fisíca'],null,["class" => "form-control form-control-sm", 'id' => 'pessoa_fisica', $readonly]) !!}
                    </div>

                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('cnpj', 'CNPJ/CPF',["class" => "m-0"]) !!}
                        {!! Form::text("cnpj",null,["class" => "form-control form-control-sm border border-danger", "autofocus", $readonly]) !!}
                    </div>
                    <div class="form-group col-md-5 m-0">
                        {!! Form::label('nomefantasia', 'Nome Fantasia',["class" => "m-0"]) !!}
                        {!! Form::text("nomefantasia",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('razaosocial', 'Razão Social/Nome',["class" => "m-0"]) !!}
                        {!! Form::text("razaosocial",null,["class" => "form-control form-control-sm border border-danger", $readonly]) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('inscestadual', 'Inscrição Estadual',["class" => "m-0"]) !!}
                        {!! Form::text("inscestadual",null,["class" => "form-control form-control-sm "]) !!}
                    </div>
                </div>
                {{-- Cadastro de Informações Pessoal -> Fim --}}

                {{-- Cadastro de Endereço -> Inicio --}}
                <div class="row border-top-anger">
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cep', 'CEP',["class" => "m-0"]) !!}
                        {!! Form::text("cep",null,["class" => "form-control form-control-sm border border-danger",]) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('endereco', 'Endereço',["class" => "m-0"]) !!}
                        {!! Form::text("endereco",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('numero', 'Número',["class" => "m-0"]) !!}
                        {!! Form::text("numero",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('complemento', 'Complemento',["class" => "m-0"]) !!}
                        {!! Form::text("complemento",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('bairro', 'Bairro',["class" => "m-0"]) !!}
                        {!! Form::text("bairro",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    {{-- Cidade -> Inicio --}}
                    <div class="form-group col-md-2 m-0">
                        {!! Form::label('cidade_id', 'Cidade',["class" => "m-0"]) !!}
                        <div class="input-group mb-2 mr-sm-2">
                          {!! Form::text("cidade_id",null,[
                                "class" => "form-control form-control-sm text-uppercase",
                                "data-json" => route('api.cidade')
                            ]) !!}
                          <div class="input-group-prepend input-group-prepend-sm">
                            <button type="button" class="btn btn-sm btn-outline-secondary rounded" data-toggle="modal" data-target="#modalCidade"><i class="fa fa-search"></i></button>
                          </div>
                        </div>
                    </div>
                    <div class="form-group col-md-4 m-0">
                        @if ($rsPessoa->cidade_id > 0)
                            @php ($cidade = $rsPessoa->cidade->nome)
                        @else
                            @php($cidade = null)
                        @endif
                        {!! Form::label('descricao_cidade', '.',['class' => "text-white m-0"]) !!}
                        {!! Form::text("descricao_cidade",$cidade,["class" => "form-control form-control-sm", "readonly"]) !!}
                    </div>
                    {{-- Cidade -> Fim --}}
                    {{-- Estado -> Inicio --}}
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('descricao_estado', 'Estado',["class" => "m-0"]) !!}
                        {!! Form::text("descricao_estado",null,["class" => "form-control form-control-sm", 'readonly']) !!}
                    </div>
                    {{-- Estado -> Fim --}}
                </div>
                {{-- Cadastro de Endereço -> Fim --}}

                {{-- Cadastro de Contato -> Inicio --}}
                <div class="row">
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('fone_receita', 'Telefone (Receita)',["class" => "m-0"]) !!}
                        {!! Form::text("fone_receita",null,["class" => "form-control form-control-sm fone"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('ramal_receita', 'Ramal (Receita)',["class" => "m-0"]) !!}
                        {!! Form::text("ramal_receita",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('email_receita', 'E-mail (Receita)',["class" => "m-0"]) !!}
                        {!! Form::text("email_receita",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('fone_fiscal', 'Telefone (Fiscal)',["class" => "m-0"]) !!}
                        {!! Form::text("fone_fiscal",null,["class" => "form-control form-control-sm fone"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('ramal_fiscal', 'Ramal (Fiscal)',["class" => "m-0"]) !!}
                        {!! Form::text("ramal_fiscal",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('email_fiscal', 'E-mail (Fiscal)',["class" => "m-0"]) !!}
                        {!! Form::text("email_fiscal",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('fone1', 'Telefone 1',["class" => "m-0"]) !!}
                        {!! Form::text("fone1",null,["class" => "form-control form-control-sm fone"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('ramal1', 'Ramal 1',["class" => "m-0"]) !!}
                        {!! Form::text("ramal1",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('email1', 'E-mail 1',["class" => "m-0"]) !!}
                        {!! Form::text("email1",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('fone2', 'Telefone 2',["class" => "m-0"]) !!}
                        {!! Form::text("fone2",null,["class" => "form-control form-control-sm fone"]) !!}
                    </div>
                    <div class="form-group col-md-3 m-0">
                        {!! Form::label('ramal2', 'Ramal 2',["class" => "m-0"]) !!}
                        {!! Form::text("ramal2",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                    <div class="form-group col-md-6 m-0">
                        {!! Form::label('email2', 'E-mail 2',["class" => "m-0"]) !!}
                        {!! Form::text("email2",null,["class" => "form-control form-control-sm"]) !!}
                    </div>
                </div>
                {{-- Cadastro de Contato -> Fim--}}

                {{-- Botões de acesso -> Inicio --}}
                <div class="row">
                    @if($rsPessoa->id > 0)
                        <div class="col-md-6 mt-3">
                            <button type="submit" title="Alterar os dados da Pessoa e volta para listagem" class ='btn btn-outline-primary btn-block btn_funcoes'><small>Alterar</small></button>
                        </div>
                        <div class="col-md-6 mt-3">
                            <a href="{{ route("pessoas") }}" title="Não salva os dados e volta para lista" class ='btn btn-outline-danger btn-block'><small>Cancelar</small></a>
                        </div>
                    @else
                        {{-- <div class="col-md-1 mt-3"></div> --}}
                        <div class="col-md-3 mt-3">
                            <button type="button" data-botao="1" title="Salva os dados e abre a tela para preencher os dados do Cliente" class ='btn btn-outline-info btn-block btn_funcoes'><small>Cliente</small></button>
                        </div>
                        {{-- <div class="col-md-2 mt-3">
                            <button type="button" data-botao="2" title="Salva os dados e abre a tela para preencher os dados do Fornecedor" class ='btn btn-outline-secondary btn-block btn_funcoes'><small>Fornecedor</small></button>
                        </div> --}}
                        <div class="col-md-3 mt-3">
                            <button type="button" data-botao="3" title="Salva os dados e abre a tela para preencher os dados da Transportadora" class ='btn btn-outline-warning btn-block btn_funcoes'><small>Transportadora</small></button>
                        </div>
                        <div class="col-md-3 mt-3">
                            <button type="button" data-botao="4" title="Salva os dados e abre a tela para preencher os dados do Representante" class ='btn btn-outline-success btn-block btn_funcoes'><small>Representante</small></button>
                        </div>
                        <div class="col-md-3 mt-3">
                            <a href="{{ route("pessoas") }}" title="Não salva os dados e volta para lista" class ='btn btn-outline-danger btn-block'><small>Cancelar</small></a>
                        </div>
                        {{-- <div class="col-md-1 mt-3"></div> --}}
                    @endif
                </div>
                {{-- Botões de acesso -> Inicio --}}

            {!! Form::close() !!}
        </div>
        {{-- Modal listagem --}}
        {{-- Listagem de Cidades - Ínicio --}}
        <div class="modal fade" id="modalCidade" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            @include('Cadastro.Pessoa.ListagemModal.cidade')
        </div>
        {{-- Listagem de Cidades - Fim --}}

    </div>

@endsection

@section('execjs')
    <script>
        $(document).ready(function(){
            $('#search').keydown(function(e){
                var keyCode = null;
                keyCode = (e.keyCode ? e.keyCode : e.which);

                if(keyCode == '13'){
                    var value = $(this).val().toLowerCase();
                    $("#table_cidade tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                    $(this).focus()
                }
            });
            $('#search').keypress(function(){
                textValue = $(this).val();

                if(textValue.length >= 5){
                    var value = $(this).val().toLowerCase();
                        $("#table_cidade tr").filter(function() {
                        $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
                    });
                }
            });

            // Mascara de preenchimento - Inicio //
            $('#cnpj').mask('00.000.000/0000-00');

            $("#pessoa_fisica").change(function(){
                $('#cnpj').val('')

                if($(this).val() == 1)
                {
                    $('#cnpj').mask('000.000.000-00');
                }
                else
                {
                    $('#cnpj').mask('00.000.000/0000-00')
                }

            })

            $('#cep').mask('00000-000')
            // Mascara de preenchimento - Fim //

            // Funçoes dos Botões de acesso - Inicio //
            // 1 -> Cliente
            // 2 -> Fornecedor
            // 3 -> Transportadora
            // 4 -> Representante
            $(".btn_funcoes").click(function(){
                $("#btn_acesso_id").val($(this).data("botao"))
                $("form").submit()

            })
            // Funçoes dos Botões de acesso - Fim //

            // Traz informações das Empresa por WS Receita -> inicio
            $('#cnpj').change(function(){
                cnpj = $("#cnpj").val()
                cnpj = cnpj.replace('.','')
                cnpj = cnpj.replace('.','')
                cnpj = cnpj.replace('/','')
                cnpj = cnpj.replace('-','')
                url = ("https://www.receitaws.com.br/v1/cnpj/" + cnpj)

                if($("#pessoa_fisica").val() == 0)
                {
                    if(validarCNPJ(cnpj) == 1 && $("#id").val() == 0)
                    {
                        $.ajax({
                            url: url,
                            jsonp: "callback",
                            dataType: "jsonp",
                            success: function( dados ) {
                                $("#razaosocial").val(dados.nome)
                                $("#nomefantasia").val(dados.fantasia)
                                cep = dados.cep;
                                cep = cep.replace('.','')
                                $("#cep").val(cep)
                                $("#cep").change()
                                $("#numero").val(dados.numero)
                                $("#email_receita").val(dados.email)
                                $("#fone_receita").val(dados.telefone)
                                $("#fone_receita").blur()

                            }
                        })

                    }

                }

            })
            // Traz informações das Empresa por WS Receita -> Fim

            // Mascara de preenchimento - Inicio //
            function limpa_formulário_cep() {
                // Limpa valores do formulário de cep.
                $("#endereco").val("")
                $("#bairro").val("")
                $("#cidade").val("")
                $("#uf").val("")
                $("#ibge").val("")
                $("#numero").val("")
            }

            $("#cep").change(function(){
                cep = $('#cep').val()
                $.getJSON("https://viacep.com.br/ws/"+ cep.replace("-","") +"/json/?callback=?", function(dados) {
                if (!("erro" in dados)) {
                    //Atualiza os campos com os valores da consulta.
                    $("#endereco").val(dados.logradouro)
                    $("#bairro").val(dados.bairro)
                    cidade_desc = cidade_descricao(dados.localidade);
                    $("#numero").val('');
                    $("#numero").focus();
                    }
                })
            })


            // Seleção de Cidade - Inicio //
            $(".select_cidade").dblclick(function(){
                $("#cidade_id").val($(this).data("id"))
                $('#cidade_id').blur();
                $('.modal').modal('hide')
            })

            $('#cidade_id').blur(function(){
                if($(this).val() != "")
                {
                    url = $(this).data('json') + '/' + $(this).val()
                    $.ajax({
                        type: "GET",
                        dataType: 'json',
                        url: url,
                        success: function(dados) {
                            if(dados.id > 0)
                            {
                                $("#descricao_cidade").val(dados.nome)
                                $("#descricao_estado").val(dados.descricao.substr(-2))
                                $("#fone_receita").focus()
                            }
                            else
                            {
                                alert("Não existe cidade com esse código")
                                $("#cidade_id").val('')
                                $("#descricao_cidade").val('')
                                $("#descricao_estado").val('')
                            }                        },
                    })
                }
            })

            function cidade_descricao(desc)
            {
                url = $("#cidade_id").data('json') + '/nome/' + desc;
                console.log(url)
                $.ajax({
                    type: "GET",
                    dataType: 'json',

                    url: url,
                    success: function(dados) {
                        if(dados.id > 0)
                        {
                            $("#cidade_id").val(dados.id)
                            $("#cidade_id").blur()
                        }
                    },
                })

                return true;
            }
            // Seleção de cidade - Fim //
            $('#cidade_id').blur();
        })
    </script>
@endsection
