@php
    use Illuminate\Support\Facades\DB;
    $rsClassificacoes = DB::table('bancos')->get();
@endphp
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Classificações</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-dark bg-secondary">
                            <th class="text-center">ID</th>
                            <th>Descrição</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($rsClassificacoes as $reg)
                        <tr class="select_classificacao cursor_pointer" data-classificacao="{{ $reg->descricao }}" data-id="{{ $reg->id }}">
                                <td class="text-center">{{ $reg->id }}</td>
                                <td>{{ $reg->descricao }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
