@php
    use Illuminate\Support\Facades\DB;
    $rsTransportadoras = DB::table('transportadoras')
                                ->join('pessoas','pessoas.id', '=', 'transportadoras.pessoa_id')
                                ->select('transportadoras.*','pessoas.razaosocial')
                               ->get();
@endphp

<div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Transportadoras</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="table-responsive">
                    <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
                        <thead>
                            <tr class="text-dark bg-secondary">
                                <th class="text-center">ID</th>
                                <th>Descrição</th>
                            </tr>
                        </thead>

                        <tbody>
                            @foreach($rsTransportadoras as $reg)
                                <tr class="select_transportadora cursor_pointer" data-id="{{ $reg->id }}">
                                        <td class="text-center">{{ $reg->id }}</td>
                                        <td>{{ $reg->razaosocial }}</td>
                                    </tr>
                                @endforeach
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
