@php
    use Illuminate\Support\Facades\DB;
    $rsRepresentantes = DB::table('representantes')
                                ->join('pessoas','pessoas.id', '=', 'representantes.pessoa_id')
                                ->select('representantes.*','pessoas.razaosocial')
                               ->get();
@endphp
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Representante</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm table-bordered" id="" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-dark bg-secondary">
                            <th class="text-center">ID</th>
                            <th>Descrição</th>
                        </tr>
                    </thead>

                    <tbody>
                        @foreach($rsRepresentantes as $reg)
                        <tr class="select_representante cursor_pointer" data-representante="{{ $reg->razaosocial }}" data-id="{{ $reg->id }}">
                                <td class="text-center">{{ $reg->id }}</td>
                                <td>{{ $reg->razaosocial }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
