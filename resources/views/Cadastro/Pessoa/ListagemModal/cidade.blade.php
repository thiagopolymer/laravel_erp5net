@php
    use Illuminate\Support\Facades\DB;
    $rsCidades = DB::table('cidades')->get();
@endphp

<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-header">
            <h5 class="modal-title text-dark" id="exampleModalLabel">Cidades</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Fechar">
                <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="table-responsive">
                <table class="table table-striped table-sm table-bordered" width="100%" cellspacing="0">
                    <thead>
                        <tr class="text-dark bg-secondary">
                            <th class="text-center">ID</th>
                            <th>Descrição</th>
                        </tr>
                        <tr>
                            <th colspan="2"><input id="search" type="text" class="form-control form-control-sm" placeholder="Minímo 5 carácteres ou dê enter para Buscar"></th>
                        </tr>
                    </thead>

                    <tbody id="table_cidade">
                        @foreach($rsCidades as $reg)
                            <tr class="select_cidade cursor_pointer" data-cidade="{{ $reg->descricao }}" data-id="{{ $reg->id }}">
                                <td class="text-center">{{ $reg->id }}</td>
                                <td>{{ $reg->nome }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

