@php
    use App\Model\Outros\Cidade;
    $tbCidade = new Cidade;
    $rsCidades = $tbCidade->orderBy("descricao")->get();

@endphp
<table class="table table-striped table-hover table-sm modal_option dataTable" id="">
    <thead>
        <tr>
            <th class="text-center">ID</th>
            <th class="text-center">IBGE</th>
            <th>Cidade</th>
            <th>Estado</th>
        </tr>
        <tr>
            <th colspan="4"><input type="text" class="form-control form-control-sm" placeholder="Digite para Buscar"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rsCidades as $reg)
            <tr class="tr_option_cidade" data-cidade="{{ $reg->descricao }}" data-id="{{ $reg->id }}">
                <td class="text-center" >{{ $reg->id }}</td>
                <td class="text-center">{{ $reg->cod_ibge }}</td>
                <td>{{ $reg->descricao }}</td>
                <td>{{ $reg->estado->descricao }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

