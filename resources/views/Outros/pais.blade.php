@php
    use App\Model\Outros\Pais;
    $tbPais = new Pais;
    $rsPais = $tbPais->orderBy("descricao")->get();

@endphp
<table class="table table-striped table-hover table-sm modal_option dataTable" id="">
    <thead>
        <tr>
            <th class="text-center">ID</th>
            <th class="text-center">Código</th>
            <th>País</th>
        </tr>
        <tr>
            <th colspan="4"><input type="text" class="form-control form-control-sm" placeholder="Digite para Buscar"></th>
        </tr>
    </thead>
    <tbody>
        @foreach ($rsPais as $reg)
            <tr class="tr_option_pais" data-pais="{{ $reg->descricao }}" data-id="{{ $reg->id }}">
                <td class="text-center" >{{ $reg->id }}</td>
                <td class="text-center">{{ $reg->codigo }}</td>
                <td>{{ $reg->descricao }}</td>
            </tr>
        @endforeach
    </tbody>
</table>

